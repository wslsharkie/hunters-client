﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccountManager : MonoBehaviour {
	
	private static Canvas Canvas;
	private InputField PasswordInputField;
	private InputField RecoverPasswordField;
	private InputField RecoverCodeField;

	void Awake() {
		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		GameObject layout = Canvas.transform.Find("Layout").transform.gameObject;
		PasswordInputField = layout.transform.Find("InputPasswordField").GetComponent<InputField>();
		RecoverPasswordField = layout.transform.Find("RecoverPasswordField").GetComponent<InputField>();
		RecoverCodeField = layout.transform.Find("RecoverCodeField").GetComponent<InputField>();
	}

	// Use this for initialization
	void Start () {
		if (Application.isEditor && !GameObject.Find("GameManager")) {
			SceneLoader.LoadGame();
		}
	}
	
	public void RequestCodeClick(){
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["password"] = PasswordInputField.text;
		body["client_id"] = PlayerPrefs.GetString("client_id");
		body["client_secret"] = PlayerPrefs.GetString("client_secret");

		//Debug.Log(PasswordInputField.text);

		StartCoroutine(Postman.RequestCode(body, ShowPasswordAndCodePopup));
	}

	public void RecoverAccountClick(){
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["password"] = RecoverPasswordField.text;
		body["code"] = RecoverCodeField.text;

		//StartCoroutine(Postman.RecoverAccountClick(body, ShowRecoveredAccountPopup));
		ShowRecoveredAccountPopup();
	}

	public void ShowPasswordAndCodePopup() {
		GameObject popup = PopupManager.OpenPopup("TextPopup");
		Dictionary<string, string> data = JsonSerializer.ParseFSData(Postman.RESPONSE);

		popup.transform.Find("Layout/Text").GetComponent<Text>().text = 
			"Your Code: \n"+data["code"] +
			"\nYour Password: \n"+data["password"] +
			"\nRemember this Code/Password in order to retrieve your account!";

		//Debug.Log(Postman.RESPONSE["code"]);
		//Debug.Log(Postman.RESPONSE["password"]);
	}

	public static void BindNewClient() {
		Dictionary<string, string> data = JsonSerializer.ParseFSData(Postman.RESPONSE);
		PlayerPrefs.SetString("client_id", data["_id"]);
		PlayerPrefs.SetString("client_secret", data["secret"]);
	}

	public void ShowRecoveredAccountPopup() {
		//BindNewClient();
		GameObject popup = PopupManager.OpenPopup("RecoveredAccount");

		popup.transform.Find("Layout/Text").GetComponent<Text>().text = 
			"Your account has been recovered!\n" +
			"Remember to generate a new code for account binding!\n" +
			"The game will now reload!";

		Button reloadButton = popup.transform.Find("Layout/Close").GetComponent<Button>();
		reloadButton.onClick.AddListener(()=>GameManager.ReloadGame());

		Debug.Log(reloadButton);

		//Debug.Log(Postman.RESPONSE["code"]);
		//Debug.Log(Postman.RESPONSE["password"]);
	}
}	
