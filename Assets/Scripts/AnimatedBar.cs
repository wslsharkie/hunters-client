﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedBar : MonoBehaviour {

	public float targetFillPercentage;
	public float startFillPercentage;
	public float transitionTime = 2;
	public bool barWraparound = false;
	float elaspedTime = 0;
	public Image bar;
	bool queuedStart = false;


	// Use this for initialization
	void Start () {

		if (!bar) {
			bar = gameObject.GetComponent<Image>();
		}

	}

	void OnEnable(){
		//Debug.Log("Doing enable for animated bar");
		if (queuedStart) {
			queuedStart = false;
			StartAnimation(targetFillPercentage, transitionTime, barWraparound);
		}
	}

	public void StartAnimation(float targetFill, float time, bool wraparound = false){

		//Just in case we call this before start
		if (!bar) {
			bar = gameObject.GetComponent<Image>();
		}

		targetFillPercentage = targetFill;
		startFillPercentage = bar.fillAmount;
		transitionTime = time;
		elaspedTime = 0;
		barWraparound = wraparound;

		if (this.enabled) {
		//	Debug.Log("this is enabled!");
			StartCoroutine(Animate());
		} else {
			queuedStart = true;
		}
	}
 
	IEnumerator Animate(){

		if (barWraparound) {
			while (bar.fillAmount < 1) {
				bar.fillAmount = Mathf.Lerp(startFillPercentage, 1.0f, elaspedTime * (1  / transitionTime));
				elaspedTime += Time.deltaTime;
				yield return null;
			}

			barWraparound = false;
			bar.fillAmount = 0;
			startFillPercentage = 0;
			elaspedTime = 0;
		}

		while (bar.fillAmount != targetFillPercentage) {
			bar.fillAmount = Mathf.Lerp(startFillPercentage, targetFillPercentage, elaspedTime * (1  / transitionTime));
			elaspedTime += Time.deltaTime;
			yield return null;
		}

	}
}
