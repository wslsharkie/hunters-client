﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {

	Queue<AnimationInfo> queuedAnimations;
	bool playingAnimation = false;
	public static AnimationManager _instance;

	struct AnimationInfo{
		public Animator animator;
		public string trigger;
		public string animationName;
	}

	public static AnimationManager instance
	{
		get { return _instance; }
	} 


	// Use this for initialization
	void Start () {
		if(AnimationManager._instance == null) {
			AnimationManager._instance = this;
			queuedAnimations = new Queue<AnimationInfo>();
		} else if(AnimationManager._instance != this) {
			Destroy(gameObject);
			return;
		}
	}

	public static void AnimateBar(AnimatedBar bar, float targetFill, float time, bool wraparound = false){
		bar.StartAnimation(targetFill, time, wraparound);
	}

	//Most of the time the trigger and animation name are the same
	public void PlayAnimation(Animator animator, string trigger, bool isImmediate = true){
		PlayAnimation(animator, trigger, trigger, isImmediate);
	}
		

	public void PlayAnimation(Animator animator, string trigger, string animationName, bool isImmediate = true){

		//Immediate animations don't block and aren't blocked by other animations
		if (isImmediate) {
			animator.SetTrigger(trigger);
		} else {
			if (playingAnimation) {
				AnimationInfo info = new AnimationInfo();
				info.animator = animator;
				info.trigger = trigger;
				info.animationName = animationName;
				queuedAnimations.Enqueue(info);
			} else {

				//Add a call back to this aniamtion to tell us when it's done playing
				AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;

				bool foundAnimation = false;
				foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips) {
					if (clip.name == animationName) {
						foundAnimation = true;
						StartCoroutine(WaitForAnimationToFinish(clip.length));
					}
				}

				//If we can't add the callback, don't play the animation
				//otherwise we'll be stuck forever
				if (!foundAnimation) {
					Debug.LogError("Couldn't find animation name " + animationName + " in clips!");
				} else {

					animator.SetTrigger(trigger);
					playingAnimation = true;
				}
			}
		}
	}

	//todo: Replace this with adding a script that
	//has a done playing function that fires an event picked up by the animation manager
	IEnumerator WaitForAnimationToFinish(float time){

		yield return new WaitForSeconds(time);
		playingAnimation = false;

		if (queuedAnimations.Count > 0) {
			AnimationInfo info = queuedAnimations.Dequeue();

			PlayAnimation(info.animator, info.trigger, info.animationName, false);
		}

	}

	public void DonePlaying(){
		playingAnimation = false;
	}
		
}
