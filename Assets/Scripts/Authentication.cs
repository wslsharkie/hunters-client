﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FullSerializer;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Authentication : MonoBehaviour {

	// used as initial access for creating a new player
	private const string _GameClientName = "L3%s#V[z_";
	private const string _GameClientPassword = "87eYrHw%C+";

	private static Authentication _Instance;

	private InputField _NameInputField;
	private Button _ConfirmNameButton;
	public bool GameStarted = false;

	void Awake() {
		//Check if instance already exists
		if(Authentication._Instance == null) {
			Authentication._Instance = this;
		} else if(Authentication._Instance != this) {
			Debug.LogWarning("Should never create Authentication again!");
			Destroy(gameObject);
			return;
		}

		// Testing: Uncomment this when player is dropped from backend to reset app!
		// PlayerPrefs.DeleteAll();

		//Debug.Log("Client: "+PlayerPrefs.GetString("client_id"));
		//Debug.Log("Secret: "+PlayerPrefs.GetString("client_secret"));

		string clientId = PlayerPrefs.GetString("client_id");
		if(string.Equals(clientId, "")) {
			PostRegisterClient();
			return;
		}

		string accessToken = PlayerPrefs.GetString("access_token");
		if(string.Equals(accessToken, "")) {
			// request new token
			PostAccessToken();
		} else {
			Debug.Log("Client: "+clientId);
			Debug.Log("Token: "+accessToken);

			if (!GameStarted) {
				InitialGameLoad();
			}
			//WWW www = Postman.GetHome();
			//StartCoroutine(LoadHome(www));
		}
	}

	void InitialGameLoad() {
		StartCoroutine(Postman.GetHome(LoadHome));
	}
		
	void PostRegisterClient() {
		// begin request for client id and secret
		string url = GameConfig.SERVER_URL+"clients";

		// form
		WWWForm form = new WWWForm();
		form.AddField ("type", "new_client");
		Dictionary<string, string> headers = form.headers;
		// Add a custom header to the request.
		// In this case a basic authentication to access a password protected resource.
		headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(_GameClientName+":"+_GameClientPassword)));

		//List<string> keyList = new List<string>(headers.Keys);

		WWW www = new WWW(url, form.data, headers);
		StartCoroutine(SaveClientData(www));
	}

	void PostAccessToken(string name = "") {
		// begin request for client id and secret
		string url = GameConfig.SERVER_URL+"oauth/token";
		string clientId = PlayerPrefs.GetString("client_id");
		string clientSecret = PlayerPrefs.GetString("client_secret");

		// something went very wrong! this should never happen!
		if(clientId.Equals("") || clientSecret.Equals("")) {
			ResetClient();
			return;
		}

		// form
		WWWForm form = new WWWForm();
		form.AddField ("grant_type", "client_credentials");
		form.AddField("name", name);

		Dictionary<string, string> headers = form.headers;
		// Add a custom header to the request.
		// In this case a basic authentication to access a password protected resource.
		headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(clientId+":"+clientSecret)));

		WWW www = new WWW(url, form.data, headers);
		StartCoroutine(StoreAccessToken(www));
	}

	IEnumerator SaveClientData(WWW www)
	{
		yield return www;

		// check for errors
		if (string.IsNullOrEmpty(www.error))
		{
			Dictionary<string, fsData> response = JsonSerializer.FromJSon(www.text);
			Dictionary<string, fsData> data = response["data"].AsDictionary;

			PlayerPrefs.SetString("client_id", data["_id"].AsString);
			PlayerPrefs.SetString("client_secret", data["secret"].AsString);

			SceneManager.LoadScene("SetName");
		} else {
			NetworkErrorPopup.OpenPopup();
		}
	}

	IEnumerator StoreAccessToken(WWW www)
	{
		yield return www;

		// check for errors
		if (string.IsNullOrEmpty(www.error))
		{
			Dictionary<string, fsData> response = JsonSerializer.FromJSon(www.text);
			Dictionary<string, fsData> access_token = response["access_token"].AsDictionary;

			PlayerPrefs.SetString("access_token", access_token["value"].AsString);

			//Debug.Log("Storing Access Token:");
			//Debug.Log(PlayerPrefs.GetString("access_token"));

			StartCoroutine(Postman.GetHome(LoadHome));
		} else {
			NetworkErrorPopup.OpenPopup();
		}
	}

	void LoadHome()
	{
		GameStarted = true;
		SceneManager.LoadScene("Home");
	}

	void ResetClient()
	{
		Debug.LogWarning("RESETTING CLIENT!");
		PlayerPrefs.DeleteAll();
		Destroy(gameObject);
		SceneManager.LoadScene("LoadGame");
	}

	public void ConfirmNamePressed(string name) {
		PostAccessToken(name);
	}
}
