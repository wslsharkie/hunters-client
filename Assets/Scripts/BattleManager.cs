﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using FullSerializer;
using System;
using System.Linq;

public class BattleManager : MonoBehaviour {

	public GameObject[] monsters;
	public GameObject[] heroesGameObjects;
	public Dictionary<string, GameObject> damageAnimations;
	public string[] damageAnimationNames = {"MonsterSlashRed", "MonsterSlashGray", "MonsterSlashDown", "MonsterSlashCross",
		"MonsterCircleSmall", "MonsterSlashLine"}; //Names of prefabs
	
	public GameObject criticalDamageTxtPrefab;
	public Canvas BattleCanvas;
	public Animator monsterAnimator;
	public GameObject heroAbilityAniamtionHolder;
	public NavBarManager navBars;

	//enum BattleTypes {StageBattle, RaidBattle}; 

	//for swipe inputs
	Vector2 startPos;
	float startTime;
	float maxSwipeTime = 1;
	float minSwipeDist = 10;
	bool couldBeSwipe;
	Vector2 touchPosition; //Used to know where to play the damage animation

	List<Rect> clickableAreaWorld;
	Dictionary<string, Hero> currentHeroes;
	List<Minion> currentMinions;
	List<GameObject> damageTxtObjects; //used for pooling
	Dictionary<string, List<GameObject>> damageAnimationsPool;
	Dictionary<string, Item> lootList;
	List<Hero> heroOrder; //List of heroes in a team in the order from left to right


	BattleState state;

	int currentDisplayedMinion;
	GameObject raidDetailsPlankPrefab;
	string playerID = "";
	string battleConfigID;
	bool logOpen = false;
	bool menuOpen = false;
	bool autoPlay = false;
	bool monsterCanBeAttacked = true;
	bool monsterDoingCounterAttack = false;
	bool popupWasActive = false;
	GameObject battleLogTxt;
	GameObject counterAttackTxt;

	#region Unity Functions

	void Start () {

		//For easy of use while working in the editor
		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

		//our main canvas
		if (!BattleCanvas) {
			BattleCanvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		}

		//Hides the nav bars from this scene
		if (!navBars) {
			navBars = GameObject.FindGameObjectWithTag("NavBarManager").GetComponent<NavBarManager>();
		}

		navBars.SetBarVisibility(NavBarManager.NavBarState.BothHidden);


		clickableAreaWorld = new List<Rect>(); //Areas that we can click on to attack
		currentHeroes = new Dictionary<string, Hero>(); //heroes in the current team
		currentMinions = new List<Minion>(); //minions that we can attack
		currentDisplayedMinion = 0; //if we ever want to have more 1 minion at atime
		damageTxtObjects = new List<GameObject>(); //cache of damage text
		lootList = new Dictionary<string, Item>(); //List of rewards. Not populated until we click on the log
		battleLogTxt = BattleCanvas.transform.Find("BattleLogTxt").gameObject;
		counterAttackTxt = BattleCanvas.transform.Find("CounterAttackTxt").gameObject;
		playerID = Player.GetID();


		//FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		state = ScriptableObject.CreateInstance<BattleState>();
		state.Init(Postman.RESPONSE);

		SetupInitialDisplay(state);
		UpdateDisplayFromBattleState(state);

		//Get the battle id and minion info
		if (Postman.RESPONSE.ContainsKey("battle")) {


			/*Dictionary<string, fsData> battle = Postman.RESPONSE["battle"].AsDictionary;
			Dictionary<string, fsData> config = battle["config"].AsDictionary;
			Dictionary<string, fsData> instance = battle["instance"].AsDictionary;
			Dictionary<string, fsData> playerStats = Postman.RESPONSE["player"].AsDictionary;
			Dictionary<string, fsData> minions = instance["minions"].AsDictionary;
			Dictionary<string, fsData> players = instance["players"].AsDictionary;*/

			//Get the first player
			/*foreach (KeyValuePair<string, fsData> player in players) {
				if (playerID == player.Key) {
					Dictionary<string, fsData> playerData = player.Value.AsDictionary;
					UpdateHeroes(playerData["team"].AsDictionary);
					SetupHeroImages(playerData["team"].AsDictionary);
				}
			} */
			
			//Need battle id to attack
			/*battleConfigID = config["id"].AsInt64.ToString();
			battleID = instance["_id"].AsString; */

			//This page is used for both raids and stage battles
			//only difference is the damage tracker

			//FullSerializer.fsSerializer.LogDictionary(instance["players"].AsDictionary);

			if (state.GetIsRaidBattle()) {
				UpdateDamageTracker(state);
			} else {
				Transform damageTracker = BattleCanvas.transform.Find("RaidDamageTracker");
				damageTracker.gameObject.SetActive(false);

			}

			//loads the hero images and uses them for ability animations
			//StartCoroutine(SetupAbilityAnimations());

			//Load monster and player animations

			damageAnimations = new Dictionary<string, GameObject>();
			damageAnimationsPool = new Dictionary<string, List<GameObject>>();

			foreach (string name in damageAnimationNames){
				GameObject prefab = (GameObject)Resources.Load("BattlePagePrefabs/" + name);
				if (prefab) {
					damageAnimations[name] = prefab;
					damageAnimationsPool[name] = new List<GameObject>();
				}
			}
		}

		//Initial entry text
		//This looks funky, either revise or just take out
	//	GameObject entryTxt = PopupManager.OpenPopup("PopOutText");
	//	PopupManager.SetTextForSimplePopup(entryTxt, "Stage Battle!");


		NewPlayerExperienceManager.FireEvent("NewPlayerExperience3");

		//Fire any other intro text for this level
		NewPlayerExperienceManager.FireEvent(state.GetStageLevelKey());


		if(Postman.RESPONSE== null) {
			// todo: show popup saying disconnected and reboot game from start.
			return;
		}
	}


	// Update is called once per frame
	void Update() {


		//Don't track input if we have another window up
		if (PopupManager.PopupActive || !monsterCanBeAttacked) {
			popupWasActive = true;
			return;
		}

		//Delay a frame when closes a popup
		//So that the close click doesn't trigger an attack
		if (popupWasActive) {
			popupWasActive = false;
			return;
		}

		//	if (Input.touches.Length > 0) {
		if (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)){

			Touch firstTouch;
			if (Application.isEditor){
				//Workaround for editor
				firstTouch = new Touch();
				firstTouch.position = Input.mousePosition;
				if (Input.GetMouseButtonDown(0)) {
					firstTouch.phase = TouchPhase.Began;
				}else if (Input.GetMouseButtonUp(0)){
					firstTouch.phase = TouchPhase.Ended;
				}else{
					firstTouch.phase = TouchPhase.Moved;
				}

			}
			else{
			 	firstTouch = Input.touches[0];
			}


			if (IsWithinClickableArea(firstTouch)) {
				//Debug.Log("Touch inside clickableArea");
			
				switch (firstTouch.phase) {
					case TouchPhase.Began:
						couldBeSwipe = true;
						startPos = firstTouch.position;
						startTime = Time.time;
						break;

					/*case TouchPhase.Moved:
						if (Mathf.Abs(firstTouch.position.y - startPos.y) > comfortZone) {
							couldBeSwipe = false;
						}
						break; */

					case TouchPhase.Stationary:
						couldBeSwipe = false;
						break;

					case TouchPhase.Ended:
						float swipeTime = Time.time - startTime;
						float swipeDist = (firstTouch.position - startPos).magnitude;

						if (couldBeSwipe && (swipeTime < maxSwipeTime) && (swipeDist > minSwipeDist)) {
							//float swipeDirection = Mathf.Sign(firstTouch.position.x - startPos.x);
							//Debug.Log("We got swipe in direction " + swipeDirection);
							//SwapMinion(swipeDirection);


						}else{
							//Debug.Log("Got a normal touch");
						     touchPosition = firstTouch.position;
							attackTarget();
						}
						break;
				}
			}
		}
	} 

	#endregion

	//Check to see if the click is within the main area
	bool IsWithinClickableArea(Touch firstTouch){

		if (clickableAreaWorld.Count < 1){
			GameObject[] clickableAreas = GameObject.FindGameObjectsWithTag("ClickableArea");

			foreach (GameObject clickableArea in clickableAreas) {
				RectTransform clickableRectTransform = clickableArea.GetComponent<RectTransform>();
		
				Vector3[] corners = new Vector3[4];
				clickableRectTransform.GetWorldCorners(corners);
				Vector3 topLeft = corners[0];

				// Rescale the size appropriately if we change the canvas scale
				Vector2 size = new Vector2(clickableRectTransform.rect.size.x,
					              clickableRectTransform.rect.size.y);

				clickableAreaWorld.Add(new Rect(topLeft, size));
			}
		}
			
		foreach (Rect clickableArea in clickableAreaWorld) {
			if (clickableArea.Contains(firstTouch.position)) {
				return true;
			}
		}
	
		return false;
	}
		


	#region Button Handlers

	//Clicking on a hero either uses the special if the hero is charged up
	//or it brings up the details popup
	public void OnHeroClick(string heroid){

		Hero heroDetails = state.GetHeroByID(heroid);

		if (heroDetails.GetSpecialChargePercentage() == 1.0f && heroDetails.GetHealth() > 0) {
			Dictionary<string, string> body = new Dictionary<string, string>();
			body["battle_id"] = state.GetBattleID();
			body["target_id"] = state.GetActiveMinionPosition().ToString();
			body["hero_id"] = heroDetails.GetID();

			if (state.GetBattleID() == string.Empty) {
				Debug.Log("No Battle id for this monster!");
			} else if (monsterCanBeAttacked) {
				StartCoroutine(Postman.SpecialAttack(body, AttackResponse));

			}

		} else {
			/*
			 * 
			 * //Reenable this once we polish it more
			StartCoroutine(SetMonsterCanBeAttacked(false));
			GameObject heroDetailsPopup = PopupManager.OpenPopup("HeroDetailsPopup");
			heroDetailsPopup.transform.Find("HeroNameTxt").GetComponent<Text>().text = heroDetails.GetName();
			//We need to enable and disable the monster attacking in order to avoid getting a monster
			//attack when we close the popup
			heroDetailsPopup.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(
				delegate {
					StartCoroutine(SetMonsterCanBeAttacked(true));
				}
			); */
		}

	}
		
	public void OnMenuBtnClick(){

		//Menu open is used only to track if the option menu is open
		if (menuOpen) {
			BattleCanvas.transform.Find("Menu").GetComponent<Animator>().SetTrigger("Close");
			StartCoroutine(SetMonsterCanBeAttacked(true));
		} else {
			BattleCanvas.transform.Find("Menu").GetComponent<Animator>().SetTrigger("Open");
			StartCoroutine(SetMonsterCanBeAttacked(false));
		}

		//flips the value
		menuOpen = !menuOpen;
	}

	//Leaves and tells the server to kill the battle
	public void OnAbandonBtnClick(){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["battle_id"] = state.GetBattleID();

		StartCoroutine(Postman.AbandonBattle(body, SceneLoader.LoadHome));
	}

	public void OnRaidDamageTrackerClick(){
		//This is a full screen popup
		GameObject popup = PopupManager.OpenPopup("RaidDetailsPopup");
		SetupRaidDetailsPopup(popup.transform);
	}

	public void OnReviveBtnClick(){
		Debug.Log("Doing revive");

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["battle_id"] = state.GetBattleID();

		StartCoroutine(Postman.ReviveTeam(body, ReviveHeroes));
	}

	//Leaves without abandoning the battle
	public void OnLeaveBtnClick(){

		SceneManager.LoadScene("Home");
	}

	//Brings up the loot dropped by this monster
	public void OnLootBtnClick(){

		//first check to see if we have the reward list
		if (lootList.Count == 0) {
			Dictionary<string, string> body = new Dictionary<string, string>();
			body["battle_id"] = battleConfigID;
			StartCoroutine(Postman.GetRewardList(body, ParseRewardList));
		} else {
			GameObject popup = PopupManager.OpenPopup("EmptyFullScreenPopup");
			SetupLootPopup(popup.transform);
		}

	}

	//Brings up the battle log for this monster
	public void OnLogBtnClick(){
		//Debug.Log("OnLogBtnClick");

		if (logOpen) {
			BattleCanvas.transform.Find("Log").GetComponent<Animator>().SetTrigger("Close");
			StartCoroutine(SetMonsterCanBeAttacked(true));

		} else {
			//close the menu
			BattleCanvas.transform.Find("Menu").GetComponent<Animator>().SetTrigger("Close");
			menuOpen = false;

			PopulateLog();
			StartCoroutine(SetMonsterCanBeAttacked(false));
			BattleCanvas.transform.Find("Log").GetComponent<Animator>().SetTrigger("Open");
		}

		logOpen = !logOpen;
	}

	public void OnRankBtnClick(){
	}

	public void OnAutoBtnClick(){
		autoPlay = !autoPlay;

		//StartCoroutine(DoAutoPlay());
	}



	#endregion

	public void ParseRewardList(){

		//parse answer
		Dictionary<string, fsData> rewards = Postman.RESPONSE["rewards"].AsDictionary;

		foreach (KeyValuePair<string, fsData> entry in rewards) {
			Dictionary<string, fsData> itemData = entry.Value.AsDictionary;
			Item item = (Item)ScriptableObject.CreateInstance("Item");
			item.Init(entry.Key, itemData);

			lootList[entry.Key] = item;
		}
			

		GameObject popup = PopupManager.OpenPopup("EmptyFullScreenPopup");
		SetupLootPopup(popup.transform);

	}

	//Adds logs entries to the battle log
	public void PopulateLog(){

		GameObject logEntryPrefab = (GameObject)Resources.Load("BattlePagePrefabs/BattleLogEntry");
		Transform logHolder = BattleCanvas.transform.Find("Log/LogHolder");
		for (int i = 0; i < logHolder.childCount; i++) {
			Destroy(logHolder.GetChild(i).gameObject);
		}

		Queue<string> battleLog = state.GetBattleLog();

		foreach (string entry in battleLog) {
			GameObject logEntry = Instantiate(logEntryPrefab, logHolder, false);
			logEntry.GetComponent<Text>().text = entry;
		}
	}

	//We can't let the monster to be attackable right away as the click event is still going on
	//and will be picked up by the update
	public IEnumerator SetMonsterCanBeAttacked(bool value){
		if (value == false) {
			monsterCanBeAttacked = false;
		} else {
			//Wait for a frame
			yield return 0;
			monsterCanBeAttacked = true;
		}
	}

	//Is autoplay supposed to do everything or just specials?
	public IEnumerator DoAutoPlay(){

		//Check to see if we have a special
		while (autoPlay) {
			//Debug.Log("Doing Auto action");
			attackTarget();
			yield return new WaitForSeconds(1);
		}
	
	}

	public void attackTarget(){

		//check to see if we're using auto play
		Dictionary<string, string> body = new Dictionary<string, string>();


		if (autoPlay) {
			//Do any specials we've got saved up instead of the attack
			foreach (KeyValuePair<string, Hero> entry in currentHeroes) {
				if (entry.Value.GetSpecialChargePercentage() == 1) {
					body["battle_id"] = state.GetBattleID();
					body["target_id"] = state.GetActiveMinionPosition().ToString();
					body["hero_id"] = entry.Value.GetID();
					StartCoroutine(Postman.SpecialAttack(body, AttackResponse));
					return;
				}
			}
		}

		body["battle_id"] = state.GetBattleID();
		body["target_id"] = state.GetActiveMinionPosition().ToString();

		StartCoroutine(Postman.Attack(body, AttackResponse));
	}

	#region Update Functions
	//Update functions are called on startup and everytime we get an attack response back from the server
	//Setup functinos are called once on startup


	void UpdateDisplayFromBattleState(BattleState state){

		UpdateHeroes(state);
		UpdatePlayer(state);
		UpdateMonsters(state);
		UpdateMonsterStatusEffects(state);

		if (state.GetIsRaidBattle()) {
			UpdateDamageTracker(state);
		}
		
	}


	void AttackResponse() {

		//Debug.Log("In attack Response"); 
		//FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		if (!Postman.RESPONSE.ContainsKey("error")) {

			state.UpdateState(Postman.RESPONSE);

			UpdateDisplayFromBattleState(state);

			Dictionary<string, fsData> battleDict = Postman.RESPONSE["battle"].AsDictionary;
			Dictionary<string, fsData> minionsDict = battleDict["minions"].AsDictionary;
			Dictionary<string, fsData> actions = Postman.RESPONSE["actions"].AsDictionary;
			Dictionary<string, fsData> playerData = Postman.RESPONSE["player"].AsDictionary;
							

			//Play any animations related to these actions
			PlayMonsterCounterAttack(actions);

			//Hold on attack animaion until after the monster counter attack is finished
			StartCoroutine(PlayAttackAnimation(actions));

			//Check to see if we won
			if (state.GetIsVictory() && Postman.RESPONSE.ContainsKey("rewards")) {
				//FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);
				NewPlayerExperienceManager.FireEvent("NewPlayerExperience4");
				StartCoroutine(CreateVictoryPopup(Postman.RESPONSE, 2.0f));
			}

			//Or lost
			if (state.GetIsDefeat()) {
				if (!state.GetIsRaidBattle()) {

					//FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);
					GameObject resultPopup = PopupManager.OpenPopup("BattleDefeatPopup");
					GameObject AbandonBtn = resultPopup.transform.Find("AbandonBtn").gameObject;
					AbandonBtn.GetComponent<Button>().onClick.AddListener(delegate {
						OnAbandonBtnClick();
					});
				} else {
					GameObject resultPopup = PopupManager.OpenPopup("RaidDefeatPopup");

					GameObject AbandonBtn = resultPopup.transform.Find("AbandonBtn").gameObject;
					AbandonBtn.GetComponent<Button>().onClick.AddListener(delegate {
						OnAbandonBtnClick();
					});

					GameObject ReviveBtn = resultPopup.transform.Find("ReviveBtn").gameObject;
					ReviveBtn.GetComponent<Button>().onClick.AddListener(delegate {
						OnReviveBtnClick();
					});

					GameObject LeaveBtn = resultPopup.transform.Find("LeaveBtn").gameObject;
					LeaveBtn.GetComponent<Button>().onClick.AddListener(delegate {
						OnLeaveBtnClick();
					});
				}
			}
		} else {
			if (Postman.RESPONSE["error"].AsString == "Not Enough Energy") {
				//show energy recharge popup

			}
		}
	}


	//Update the monster health and charge
	void UpdateMonsters(BattleState state){

		Minion activeMinion = state.GetActiveMinion();

		Text monsterName = BattleCanvas.transform.Find("TopStats/MonsterName").GetComponent<Text>();
		monsterName.text = activeMinion.GetName();

		AnimatedBar monsterHealth = BattleCanvas.transform.Find("TopStats/hpBar").GetComponent<AnimatedBar>();
		AnimationManager.AnimateBar(monsterHealth, activeMinion.GetHealthPercentage(), 1);

		AnimatedBar monsterCharge = BattleCanvas.transform.Find("TopStats/spBar").GetComponent<AnimatedBar>();
		AnimationManager.AnimateBar(monsterCharge, activeMinion.GetSpecialChargePercentage(), 1);

		Text monsterLevel = BattleCanvas.transform.Find("TopStats/LevelTxt2").GetComponent<Text>();
		monsterLevel.text = activeMinion.GetLevel();

		//If we just killed the monster, disable attacks and get rid of it.
		if (activeMinion.GetHealth() <= 0) {
			monsterCanBeAttacked = false; // temp work around

			AnimationManager.instance.PlayAnimation(monsterAnimator, "monsterDeath", false);
		}


	}

	//Update the icon images only
	void UpdateMonsterStatusEffects(BattleState state){

		Dictionary<string, string> abilityList = state.GetMinionStatusEffectIcons();

		int i = 0;
		foreach (KeyValuePair<string, string> entry in abilityList) {

			GameObject statusEffect = BattleCanvas.transform.Find("MonsterStatusEffects/StatusImg" + i.ToString()).gameObject;
			statusEffect.SetActive(true);

			Texture2D texture = (Texture2D)Resources.Load("Images/Battle/" +entry.Value);
			if (texture) {
				Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
				statusEffect.GetComponent<Image>().sprite = newSprite;
			}

			i++;
		}

		for (int j = i; j < 4; j++){
			BattleCanvas.transform.Find("MonsterStatusEffects/StatusImg" + j.ToString()).gameObject.SetActive(false);
		}
	
	}
		
	//Update the player's team's health and charge values
	void UpdateHeroes(BattleState state){

		int i;
		for (i = 0; i < 4; i++) {
			Hero hero = state.GetHeroAtSlot(i);

			if (hero == null) {
				break;
			}

			heroesGameObjects[i].SetActive(true);

			Transform effectIconHolder = heroesGameObjects[i].transform.Find("StatusEffectIcons");
			for (int j = 0; j < effectIconHolder.childCount; j++){
				//todo: Setup the icon if we find a status effect
				effectIconHolder.GetChild(j).gameObject.SetActive(false);
			}

			AnimatedBar healthBar = heroesGameObjects[i].transform.Find("InfoPlate/healthBar").GetComponent<AnimatedBar>();
			AnimationManager.AnimateBar(healthBar, hero.GetHealthPercentage(), 1);
		
			AnimatedBar spBar = heroesGameObjects[i].transform.Find("InfoPlate/spBar").GetComponent<AnimatedBar>();
			AnimationManager.AnimateBar(spBar, hero.GetSpecialChargePercentage(), 1);

			if (hero.GetHealth() > 0) {
				heroesGameObjects[i].transform.Find("Image").GetComponent<Image>().color = Color.gray;
			}

			Animator heroFrameAnimator = heroesGameObjects[i].transform.Find("frame").GetComponent<Animator>();

			if (hero.GetSpecialChargePercentage() >= 1 && hero.GetHealth() > 0) {
				//shouldn't this be part of the animation
				heroesGameObjects[i].transform.Find("frame").GetComponent<Image>().color = new Color(.169f, .624f, 1);
				AnimationManager.instance.PlayAnimation(heroFrameAnimator, "Glow", true);
				NewPlayerExperienceManager.FireEvent("hero_special_ready");
			} else {
				heroesGameObjects[i].transform.Find("frame").GetComponent<Image>().color = Color.white;
				AnimationManager.instance.PlayAnimation(heroFrameAnimator, "Idle", true);
			}

			//update details popup
			string HeroID = hero.GetID();

			heroesGameObjects[i].GetComponent<Button>().onClick.RemoveAllListeners();
			heroesGameObjects[i].GetComponent<Button>().onClick.AddListener(delegate {
				OnHeroClick(HeroID);
			});
		}

		//hide the heroes if we have empty slots
		for (int j = i; j < 4; j++) {
			heroesGameObjects[j].SetActive(false);
		}

	}
		
	//update the player energy
	void UpdatePlayer(BattleState state){
		
		int currentEnergy = state.GetPlayerEnergy();
		int maxEnergy = state.GetPlayerMaxEnergy();
		string energyString = currentEnergy.ToString() + "/" + maxEnergy.ToString();
		float barFill = (float)currentEnergy / (float)maxEnergy;

		BattleCanvas.transform.Find("Energy/EnergyTxt").GetComponent<Text>().text = energyString;
		BattleCanvas.transform.Find("Energy/EnergyBar").GetComponent<Image>().fillAmount = barFill;
	}

	//Updates the damage tracker for muliplayer raid battles
	void UpdateDamageTracker(BattleState state){

		//Gets the UI game object
		Transform damageTracker = BattleCanvas.transform.Find("RaidDamageTracker");

		//Clear out the old entries except for the header text
		for (int i = 1; i < damageTracker.childCount; i++) {
			Destroy(damageTracker.GetChild(i).gameObject);
		}

		List<KeyValuePair<string, int>> damageList = state.GetDamageList();
		damageList.Sort((a, b) => {
			return b.Value.CompareTo(a.Value);
		});

		GameObject rankPrefab = (GameObject)Resources.Load("BattlePagePrefabs/PlayerRank");

		int j = 1;
		foreach (KeyValuePair<string, int> entry in damageList) {
			GameObject rankTarget = Instantiate(rankPrefab, damageTracker, false);
			rankTarget.transform.Find("NameTxt").GetComponent<Text>().text = j.ToString() + ". " + entry.Key;
			rankTarget.transform.Find("DamageTxt").GetComponent<Text>().text = AbbviateNumberText( entry.Value.ToString());
			j++;

		}
	}


	#endregion

	#region Popups

	//Only used for raids
	void SetupRaidDetailsPopup(Transform popupTransform){

		float plankYOffset = 344;
		float plankHeight = -90;
		raidDetailsPlankPrefab = (GameObject)Resources.Load("BattlePagePrefabs/RaidDetailsPlank");
		Transform content = popupTransform.Find("Scroll View/Viewport/Content");

		foreach (KeyValuePair<string, int> entry in state.GetDamageList()) {
			GameObject newPlank = Instantiate(raidDetailsPlankPrefab, content, false);
			newPlank.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, plankYOffset);
			plankYOffset += plankHeight;
			newPlank.transform.Find("NameTxt").GetComponent<Text>().text = entry.Key;
			newPlank.transform.Find("DmgTxt").GetComponent<Text>().text = entry.Value.ToString();
		}
	}

	//Loot popup for displaying what this stage/raid drops
	void SetupLootPopup(Transform popupTransform){

		GameObject lootContentPrefab = (GameObject)Resources.Load("BattlePagePrefabs/LootContent");
		GameObject itemPrefab = (GameObject)Resources.Load("BattlePagePrefabs/VictoryPopupReward");

		GameObject lootContent = (GameObject)Instantiate(lootContentPrefab, popupTransform, false);
		Transform rareItemHolder = lootContent.transform.Find("RareHolder");

		foreach (KeyValuePair<string, Item> entry in lootList) {
			GameObject item = Instantiate(itemPrefab, rareItemHolder, false);
			item.transform.Find("NameTxt").GetComponent<Text>().text = entry.Value.GetName();
			item.transform.Find("AmountTxt").GetComponent<Text>().text = "1";

			//Change Image here too
		}

	
	}

	//Makes the victory popup for when the player finishes a stage
	IEnumerator CreateVictoryPopup(Dictionary<string, fsData> fullResponse, float delay){

		yield return new WaitForSeconds(delay);

		GameObject contentPrefab = (GameObject)Resources.Load("BattlePagePrefabs/VictoryPopupContent");
		GameObject rewardsPrefab = (GameObject)Resources.Load("BattlePagePrefabs/VictoryPopupReward");
		GameObject heroRewardPrefab = (GameObject)Resources.Load("BattlePagePrefabs/VictoryPopupRewardHero");
		GameObject heroPrefab = (GameObject)Resources.Load("BattlePagePrefabs/VictoryPopupHero");

		List<fsData> rewards = fullResponse["rewards"].AsList;

		if (contentPrefab && rewardsPrefab) {
			GameObject resultPopup = PopupManager.OpenPopup("BattleVictoryPopup");

			Transform rewardsHolder = resultPopup.transform.Find("VictoryPopupContent/Rewards");

			for (int i = 0; i < rewards.Count; i++) {
				//todo add support for second row
				Dictionary<string, fsData> rewardInfo = rewards[i].AsDictionary;
				FullSerializer.fsSerializer.LogDictionary(rewardInfo);
				if (rewardInfo.ContainsKey("id")) {

					//Hero Drop
					//GameObject newReward = (GameObject)Instantiate(heroRewardPrefab, rewardsHolder, false);

					if (rewardInfo.ContainsKey("type") && rewardInfo["type"].AsInt64 == 2) {
						GameObject newReward = (GameObject)Instantiate(heroRewardPrefab, rewardsHolder, false);

						Hero rewardedHero = (Hero)ScriptableObject.CreateInstance("Hero");
						rewardedHero.InitWithID(rewardInfo["id"].AsInt64.ToString());
						string imagePath = rewardedHero.GetPortraitPath();
						Texture2D rewardTexture = ClientCache.GetTexture(imagePath);

						if (rewardTexture) {
							Sprite rewardImage = Sprite.Create(rewardTexture, new Rect(0, 0, rewardTexture.width, rewardTexture.height), new Vector2(0.5f, 0.5f));
							newReward.transform.Find("NameTxt").GetComponent<Text>().text = rewardedHero.GetName();
							newReward.transform.Find("Image").GetComponent<Image>().sprite = rewardImage;
						}
					//Item Drop
					} else {
						GameObject newReward = (GameObject)Instantiate(rewardsPrefab, rewardsHolder, false);

						Item rewardItem = (Item)ScriptableObject.CreateInstance("Item");
						rewardItem.Init(rewardInfo["id"].AsInt64.ToString(), rewardInfo);
						string imagePath = rewardItem.GetImage();

						Texture2D rewardTexture = ClientCache.GetTexture(imagePath);
						if (rewardTexture) {
							Sprite rewardImage = Sprite.Create(rewardTexture, new Rect(0, 0, rewardTexture.width, rewardTexture.height), new Vector2(0.5f, 0.5f));
							newReward.transform.Find("NameTxt").GetComponent<Text>().text = rewardItem.GetName();
							newReward.transform.Find("AmountTxt").GetComponent<Text>().text = "x" + rewardInfo["amount"].AsInt64.ToString();
							newReward.transform.Find("Image").GetComponent<Image>().sprite = rewardImage;

						}
					}
				}
			}

			Dictionary<string, fsData> heroXPData = fullResponse["heroLevels"].AsDictionary;

			//Add in hero xp rewards
			Transform heroHolder = resultPopup.transform.Find("VictoryPopupContent/HeroTeam");
			foreach (KeyValuePair<string, Hero> entry in state.GetHeroes()) {
				GameObject hero = (GameObject)Instantiate(heroPrefab, heroHolder.transform, false);

				//find out how much XP the hero gained
				Dictionary<string, fsData> heroXPGain = heroXPData[entry.Value.GetID()].AsDictionary;

				Transform xpBar = hero.transform.Find("xpBar").transform;

				//todo: what if they gain more than a whole level?
				bool levelGained = false;
				if (heroXPGain["currentLevel"].AsInt64 > heroXPGain["prevLevel"].AsInt64) {
					levelGained = true;
				}

				xpBar.GetComponent<Image>().fillAmount = (float)(heroXPGain["prevPct"].AsInt64) / 100.0f;
				xpBar.GetComponent<AnimatedBar>().StartAnimation((float)(heroXPGain["currentPct"].AsInt64) / 100.0f, 1, levelGained);
	
				hero.transform.Find("OldLevelTxt").GetComponent<Text>().text = heroXPGain["currentLevel"].AsInt64.ToString();
				hero.transform.Find("NewLevelTxt").GetComponent<Text>().text = (heroXPGain["currentLevel"].AsInt64 + 1).ToString();

				//Setup portrait
				char[] charsToTrim = { '"' };
				Image heroPortrait = hero.transform.Find("Image").GetComponent<Image>();
				String portaitPath = entry.Value.GetPortraitPath().Trim(charsToTrim);
				Texture2D portaitTexture = ClientCache.GetTexture(portaitPath);
				Sprite newPortait = Sprite.Create(portaitTexture, new Rect(0, 0, portaitTexture.width, portaitTexture.height), new Vector2(0.5f, 0.5f));
				heroPortrait.sprite = newPortait;

				//setup frame
				Image frameImage = hero.transform.Find("frame").GetComponent<Image>();
				String frameName = entry.Value.GetFrameName();
				Texture2D frameTexture = (Texture2D)Resources.Load("Images/Battle/Hero_frames/" + frameName);
				Sprite newFrame = Sprite.Create(frameTexture, new Rect(0, 0, frameTexture.width, frameTexture.height), new Vector2(0.5f, 0.5f));
				frameImage.sprite = newFrame;

			}
				
			//Set return btn to return to stage select
			Button returnBtn = resultPopup.transform.Find("VictoryPopupContent/ReturnBtn").GetComponent<Button>();

			//The first time we kill a monster, display a message and take us back to home intead of stage select
			if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience4")){
				returnBtn.onClick.AddListener(ReturnToHome);
			} else {
				returnBtn.onClick.AddListener(ReturnToStageSelect);

			}
		}
	}


	void ReturnToStageSelect(){
		StartCoroutine(Postman.GetStages(SceneLoader.LoadStages));
	}

	void ReturnToHome(){
		StartCoroutine(Postman.GetHome(SceneLoader.LoadHome));
	}

	void ReviveHeroes(){
		Debug.Log("reviving heroes");

		AttackResponse();
	}

	#endregion


	#region Setup Fucntions
	//These functions are called only once at startup

	void SetupInitialDisplay(BattleState state){

		SetupHeroImages(state);
		SetupBackground(state);
		SetupMonster(state);
	}
		
	//Loads the hero images at startup
	void SetupHeroImages(BattleState state){

		for (int i = 0; i < 4; i++) {
			Hero hero = state.GetHeroAtSlot(i);
			if (hero == null)
				continue;

			Image frameImage =  heroesGameObjects[i].transform.Find("frame").GetComponent<Image>();
			String frameName = hero.GetFrameName();
			Texture2D frameTexture = (Texture2D)Resources.Load("Images/Battle/Hero_frames/" + frameName);
			Sprite newFrame = Sprite.Create(frameTexture, new Rect(0, 0, frameTexture.width, frameTexture.height), new Vector2(0.5f, 0.5f));

			frameImage.sprite = newFrame;

			Image portaitImage =  heroesGameObjects[i].transform.Find("Image").GetComponent<Image>();
			char[] charsToTrim = {'"'};

			String portaitPath = hero.GetPortraitPath().Trim(charsToTrim);
			//Debug.Log("Image path = " + portaitPath + " for hero ");

			Texture2D portaitTexture = ClientCache.GetTexture(portaitPath);
			Sprite newPortait = Sprite.Create(portaitTexture, new Rect(0, 0, portaitTexture.width, portaitTexture.height), new Vector2(0.5f, 0.5f));
			portaitImage.sprite = newPortait;

		}
	}
		


	//Loads the background image for this stage
	void SetupBackground(BattleState state){
		
		string backgroundImage = state.GetBackgroundImage();
		Image background = BattleCanvas.transform.Find("Background").GetComponent<Image>();

		Texture2D backgroundTexture = ClientCache.GetTexture(backgroundImage);
		Sprite newBackground = Sprite.Create(backgroundTexture, new Rect(0, 0, backgroundTexture.width, backgroundTexture.height), new Vector2(0.5f, 0.5f));
		background.sprite = newBackground;
	}

	void SetupMonster(BattleState state){

		Minion minion = state.GetActiveMinion();
			Image monster = BattleCanvas.transform.Find("Monsters/Monster0").GetComponent<Image>();

		//Get the first minion's image
		//We never display more than 1 minion at a time
		char[] charsToTrim = {'"'};

		string monsterImage = minion.GetImage().Trim(charsToTrim);

		Texture2D monsterTexture = ClientCache.GetTexture(monsterImage);
		Sprite newMonsterImage = Sprite.Create(monsterTexture, new Rect(0, 0, monsterTexture.width, monsterTexture.height), new Vector2(0.5f, 0.5f));
		monster.sprite = newMonsterImage;

		//Add offset if any
		Transform monsterPos = BattleCanvas.transform.Find("Monsters");
		monsterPos.localPosition = minion.GetPositionOffset();

	}

	#endregion

	#region Animations

	//Todo: Get these to act off the battle state
	void PlayMonsterCounterAttack(Dictionary<string, fsData> actions){
		if (actions.ContainsKey("enemy")){
		//	FullSerializer.fsSerializer.LogDictionary(actions);
			GameObject popup = PopupManager.OpenPopup("DropDownText");
			PopupManager.SetTextForSimplePopup(popup, "COUNTER ATTACK");

			//Eh, doesn't look that good
			//AnimationManager.instance.PlayAnimation(monsterAnimator, "monsterCounterAttack2", false);

			Dictionary<string, fsData> enemyAttack = actions["enemy"].AsDictionary;
			List<fsData> logs = enemyAttack["logs"].AsList;
			AddCounterAttackTxt(logs[0].AsString);
				

			GameObject playerToHit = heroesGameObjects[int.Parse(enemyAttack["targetPosition"].AsString)];
			Animator heroAnimator = playerToHit.GetComponent<Animator>();
			AnimationManager.instance.PlayAnimation(heroAnimator, "HeroHit");

			//Spawn Damage text
			//long damage = enemyAttack["damage"].AsInt64;
			//AddDamageTxt(damage, playerToHit.transform.position);

		} 
	}

	public void MonsterCounterAttackFinished(){
		//Debug.Log("Finished counter attack");
		monsterDoingCounterAttack = false;
	}


	IEnumerator PlayAttackAnimation(Dictionary<string, fsData> response){

		//FullSerializer.fsSerializer.LogDictionary(response);

		while (monsterDoingCounterAttack) {
			yield return null;
		}

		AnimationManager.instance.PlayAnimation(monsterAnimator, "monsterHit", "monsterAnim", true);


		//Play monster hit animation
		string damagePrefabName = damageAnimationNames[(UnityEngine.Random.Range(0, damageAnimationNames.Length))];			
		AddHitAnimation(monsters[0].transform.localPosition, damagePrefabName);
			
		Dictionary<string, fsData> playerDict = response["player"].AsDictionary;
		//FullSerializer.fsSerializer.LogDictionary(playerDict);
		int attackingHeroPosition = int.Parse(playerDict["position"].AsString);
		Animator heroAnimator = heroesGameObjects[attackingHeroPosition].GetComponent<Animator>();
		AnimationManager.instance.PlayAnimation(heroAnimator, "HeroAttack", "heroJiggle", false);

		List<fsData> logs = playerDict["logs"].AsList;

		AddBattleLogTxt(logs[0].AsString);

		long damage = playerDict["damage"].AsInt64;
		string heroID = playerDict["id"].AsInt64.ToString();

		string[] damageTextAnimations = { "DamageTxtFlingRight", "DamageTxtFloat", "DamageTxtShake", "DamageTxtFlingLeft" };
		int rand = UnityEngine.Random.Range(0, damageTextAnimations.Length);	
		AddDamageTxt(damage, new Vector2(0, 0), damageTextAnimations[rand]);
	
		//check for ability usage
		if (playerDict.ContainsKey("ability") && playerDict["ability"].AsInt64 == 1){
			//Do ablility animation
			DoAbilityAnimationForHero(heroID);
		}
	}

	//Object batching for damage text
	public void AddDamageTxt(long damage, Vector2 startingPos, string animationClip = "DamageTxtFloat"){

		//Debug.Log(startingPos);
		if (damageTxtObjects.Count > 0) {
			//Reuse old damage text

			damageTxtObjects[0].SetActive(true);
			damageTxtObjects[0].transform.Find("DamageTxt").GetComponent<Text>().text = damage.ToString() + "!";
			damageTxtObjects[0].transform.localPosition = startingPos;
			Animator damageTxtAnimator = damageTxtObjects[0].GetComponent<Animator>();
			AnimationManager.instance.PlayAnimation(damageTxtAnimator, animationClip);
			damageTxtObjects.RemoveAt(0);
		} else {
			//create new damage text
			GameObject damagePrefab = (GameObject)Resources.Load("BattlePagePrefabs/DamageTextHolder");
			GameObject damageTxt = Instantiate(damagePrefab, BattleCanvas.transform);
			damageTxt.transform.localPosition = startingPos;
			damageTxt.transform.Find("DamageTxt").GetComponent<Text>().text = damage.ToString() + "!";
			Animator damageTxtAnimator = damageTxt.GetComponent<Animator>();
			AnimationManager.instance.PlayAnimation(damageTxtAnimator, animationClip);


		}
	}

	public void StoreDamageTxt(GameObject damageTxt){
		damageTxtObjects.Add(damageTxt);
	}

	public void AddHitAnimation(Vector2 startingPos, string prefabName){
		List<GameObject> existingHitAnimations = damageAnimationsPool[prefabName];
		//Debug.Log("attempting do create hit animation " + prefabName);

		if (existingHitAnimations.Count > 0) {
			//Debug.Log("found existing animation " + prefabName);

			existingHitAnimations[0].SetActive(true);
			existingHitAnimations[0].transform.localPosition = startingPos;
			Animator hitAnimator = existingHitAnimations[0].GetComponent<Animator>();
			//existingHitAnimations[0].GetComponent<Animator>().SetTrigger("play");
			AnimationManager.instance.PlayAnimation(hitAnimator, "play", true);
			existingHitAnimations.RemoveAt(0);
		} else {
			//Debug.Log("making new animation " + prefabName);

			GameObject damageAnimationPrefab = damageAnimations[prefabName];
			GameObject damageAnimation = Instantiate(damageAnimationPrefab, BattleCanvas.transform);
			damageAnimation.transform.localPosition = startingPos;
			damageAnimation.name = prefabName;

		}
	}
			
	public void StoreDamageAnimation(GameObject animation, string prefabName){
		animation.SetActive(false);
		damageAnimationsPool[prefabName].Add(animation);
	}

	//Adds floating text for battle log
	void AddBattleLogTxt(string logTxt){
		battleLogTxt.GetComponent<Text>().text = logTxt;
		battleLogTxt.GetComponent<Animator>().SetTrigger("fadeout");
	}

	void AddCounterAttackTxt(string logTxt){
		counterAttackTxt.GetComponent<Text>().text = logTxt;
		counterAttackTxt.GetComponent<Animator>().SetTrigger("delayedFadeout");

	}


	//Does the setup and plays the animation for hero abilities
	public void DoAbilityAnimationForHero(string heroID){

		Hero hero = state.GetHeroByID(heroID);
		//Debug.Log("Doing animation for hero " + hero.GetName());
			
		Image heroImage = heroAbilityAniamtionHolder.transform.Find("HeroAbilityEffect").GetComponent<Image>();

		String imagePath = hero.GetImagePath();
		Texture2D texture = ClientCache.GetTexture(imagePath);
		//Debug.Log("Image path = " + imagePath + " for hero " + entry.Value.GetName());

		if (texture) {
			Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
			heroImage.sprite = newSprite;
		} else {
			Debug.Log("Failed loading " + imagePath);
		}	
			

	//	heroAbilityAniamtionHolder.GetComponent<Animator>().SetTrigger("ability");
		Animator abilityAnimator = heroAbilityAniamtionHolder.GetComponent<Animator>();
		AnimationManager.instance.PlayAnimation(abilityAnimator, "ability", "AbilityAnimation", false);
	}

	#endregion


	string AbbviateNumberText(string stringInput){

		double input = double.Parse(stringInput);

		if (input >= 1000000) {
			return Math.Round(input / 1000000).ToString() + "M";
		} else if (input >= 1000) {
			return Math.Round(input / 1000).ToString() + "K";
		} else {
			return input.ToString();
		}
	}

}
