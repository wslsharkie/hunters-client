﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class ButtonTouch : MonoBehaviour, IPointerExitHandler, IPointerUpHandler {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// Check if there is a touch
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
			// Check if finger is over a UI element
			if(EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) == false) {
				EventSystem.current.SetSelectedGameObject(null);
			}
		} else {
			EventSystem.current.SetSelectedGameObject(null);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		EventSystem.current.SetSelectedGameObject(null);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		EventSystem.current.SetSelectedGameObject(null);
	}
}
