﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.UI;
using ExitGames.Client.Photon.Chat;

public class ChatManager : MonoBehaviour, IChatClientListener {

	// set in inspector. Demo channels to join automatically.
	public string[] ChannelsToJoinOnConnect; 
	// set in inspector. Up to a certain degree, previously sent messages can be fetched for context
	public int HistoryLengthToFetch; 

	// mainly used for GUI/input
	private string selectedChannelName; 

	public string UserName { get; set; }
	public ChatClient chatClient;

	public RectTransform ChatPanel;     // set in inspector (to enable/disable panel)
	public InputField InputFieldChat;   // set in inspector
	public Text CurrentChannelText;     // set in inspector
	public Toggle ChannelToggleToInstantiate; // set in inspector
	public Button ChatSendButton;
	public Button CloseButton;
	private readonly Dictionary<string, Toggle> channelToggles = new Dictionary<string, Toggle>();

	private bool Connected = false;

	private static string WelcomeText = "Welcome to ";
	private static string HelpText = "\n\\subscribe <list of channelnames> subscribes channels." +
		"\n\\unsubscribe <list of channelnames> leaves channels." +
		"\n\\msg <username> <message> send private message to user." +
		"\n\\clear clears the current chat tab. private chats get closed." +
		"\n\\help gets this help message.";

	void Awake() {
		if(NavBarManager.ChatManager == null) {
			gameObject.SetActive(false);
			NavBarManager.ChatManager = gameObject.GetComponent<ChatManager>();
		} else {
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		// this must run in background or it will drop connection if not focussed.
		Application.runInBackground = true;
	}

	public void Connect() {
		if (string.IsNullOrEmpty(UserName))
		{
			UserName = Player.GetName();
		}

		chatClient = new ChatClient(this);
		string chatAppId = ChatSettings.Instance.AppId;
		chatClient.Connect(chatAppId, "1.0", new AuthenticationValues (UserName));
		PopupManager.OpenPopup("LoadingPopup");
	}

	public void OnEnable() {
		//Debug.Log("CHAT ENABLED");
		//Debug.Log(Connected);
		if(Connected) {
			LoadChatPanel();
		} else {
			Connect();
		}
	}

	/// <summary>To avoid that the Editor becomes unresponsive, disconnect all Photon connections in OnApplicationQuit.</summary>
	public void OnApplicationQuit()
	{
		if (this.chatClient != null)
		{
			this.chatClient.Disconnect();
		}
	}

	void OnDestroy() {
		if (this.chatClient != null)
		{
			this.chatClient.Disconnect();
		}	
	}

	// Update is called once per frame
	void Update () {
		if (this.chatClient != null) {
			// make sure to call this regularly! it limits effort internally, so calling often is ok!
			this.chatClient.Service();
		}
	}

	public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
	{
		if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
		{
			UnityEngine.Debug.LogError(message);
		}
		else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
		{
			UnityEngine.Debug.LogWarning(message);
		}
		else
		{
			UnityEngine.Debug.Log(message);
		}
	}

	public void OnConnected()
	{
		if (this.ChannelsToJoinOnConnect != null && this.ChannelsToJoinOnConnect.Length > 0)
		{
			this.chatClient.Subscribe(this.ChannelsToJoinOnConnect, this.HistoryLengthToFetch);
		}

		this.chatClient.AddFriends(new string[] {"tobi", "ilya"}); // Add some users to the server-list to get their status updates
		this.chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).

		Connected = true;
		PopupManager.ClosePopup("LoadingPopup");
		LoadChatPanel();
	}

	public void OnDisconnected()
	{
		Connected = false;
	}

	public void OnChatStateChange(ChatState state)
	{
		// use OnConnected() and OnDisconnected()
		// this method might become more useful in the future, when more complex states are being used.

		//this.StateText.text = state.ToString();
	}
		
	public void OnSubscribed(string[] channels, bool[] results)
	{
		// in this demo, we simply send a message into each channel. This is NOT a must have!
		foreach (string channel in channels)
		{
			//this.chatClient.PublishMessage(channel, "says 'hi'."); // you don't HAVE to send a msg on join but you could.

			if (this.ChannelToggleToInstantiate != null)
			{
				this.InstantiateChannelButton(channel);

			}
		}

		Debug.Log("OnSubscribed: " + string.Join(", ", channels));

		// select first subscribed channel in alphabetical order
		if (this.chatClient.PublicChannels.Count > 0)
		{
			var l = new List<string>(this.chatClient.PublicChannels.Keys);
			l.Sort();
			string selected = l[0];
			if (this.channelToggles.ContainsKey(selected))
			{
				ShowChannel(selected);
				foreach (var c in this.channelToggles)
				{
					c.Value.isOn = false;
				}
				this.channelToggles[selected].isOn = true;
				AddMessageToSelectedChannel(WelcomeText+selected+" chat!");
			}
		}
	}

	public void OnUnsubscribed(string[] channels)
	{
		foreach (string channelName in channels)
		{
			if (this.channelToggles.ContainsKey(channelName))
			{
				Toggle t = this.channelToggles[channelName];
				Destroy(t);

				this.channelToggles.Remove(channelName);
			}
		}
	}

	public void OnGetMessages(string channelName, string[] senders, object[] messages)
	{
		if (channelName.Equals(this.selectedChannelName))
		{
			// mainly used for GUI/input// update text
			ShowChannel(this.selectedChannelName);
		}
	}

	public void OnPrivateMessage(string sender, object message, string channelName)
	{
		// as the ChatClient is buffering the messages for you, this GUI doesn't need to do anything here
		// you also get messages that you sent yourself. in that case, the channelName is determinded by the target of your msg
		//this.InstantiateChannelButton(channelName);

		byte[] msgBytes = message as byte[];
		if (msgBytes != null)
		{
			Debug.Log("Message with byte[].Length: "+ msgBytes.Length);
		}
		if (this.selectedChannelName.Equals(channelName))
		{
			ShowChannel(channelName);
		}
	}

	public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
	{
		// this is how you get status updates of friends.
		// this demo simply adds status updates to the currently shown chat.
		// you could buffer them or use them any other way, too.

		// TODO: add status updates
		//if (activeChannel != null)
		//{
		//    activeChannel.Add("info", string.Format("{0} is {1}. Msg:{2}", user, status, message));
		//}

		Debug.LogWarning("status: " + string.Format("{0} is {1}. Msg:{2}", user, status, message));
	}

	public void ShowChannel(string channelName)
	{
		if (string.IsNullOrEmpty(channelName))
		{
			return;
		}

		ChatChannel channel = null;
		bool found = this.chatClient.TryGetChannel(channelName, out channel);
		if (!found)
		{
			Debug.Log("ShowChannel failed to find channel: " + channelName);
			return;
		}

		this.selectedChannelName = channelName;
		this.CurrentChannelText.text = channel.ToStringMessages();
		Debug.Log("ShowChannel: " + this.selectedChannelName);
	}

	private void InstantiateChannelButton(string channelName)
	{
		if (this.channelToggles.ContainsKey(channelName))
		{
			Debug.Log("Skipping creation for an existing channel toggle.");
			return;
		}

		Toggle cbtn = (Toggle)GameObject.Instantiate(this.ChannelToggleToInstantiate);
		cbtn.gameObject.SetActive(true);
		cbtn.GetComponentInChildren<ChatChannelSelector>().SetChannel(channelName);
		cbtn.transform.SetParent(this.ChannelToggleToInstantiate.transform.parent, false);

		this.channelToggles.Add(channelName, cbtn);
	}

	public void AddMessageToSelectedChannel(string msg)
	{
		ChatChannel channel = null;
		bool found = this.chatClient.TryGetChannel(this.selectedChannelName, out channel);
		if (!found)
		{
			Debug.Log("AddMessageToSelectedChannel failed to find channel: " + this.selectedChannelName);
			return;
		}

		if (channel != null)
		{
			channel.Add(GameConfig.GAME_NAME, msg);
		}
	}

	public void LoadChatPanel() {
		if(ChatPanel || ChatPanel != null) {
			return;
		}

		Canvas canvas = FindObjectOfType<Canvas>();
		GameObject chatResource = (GameObject)Resources.Load("Prefabs/ChatPanel");
		GameObject chatPanel = (GameObject)Instantiate(chatResource, canvas.transform, false);
		chatPanel.gameObject.SetActive(false);

		// Links
		ChatPanel = chatPanel.GetComponent<RectTransform>();
		InputFieldChat = chatPanel.transform.Find("InputBar Panel/Chat InputField")
			.GetComponent<InputField>();
		CurrentChannelText = chatPanel.transform.Find("ChatOutput Panel/Scroll Panel/Grid/Selected Channel Text")
			.GetComponent<Text>();
		ChannelToggleToInstantiate = chatPanel.transform.Find("ChannelBar Panel/Channel Toggle")
			.GetComponent<Toggle>();
		ChatSendButton = chatPanel.transform.Find("InputBar Panel/Chat Send Button")
			.GetComponent<Button>();
		CloseButton = chatPanel.transform.Find("Close Button")
			.GetComponent<Button>();

		InputFieldChat.onEndEdit.AddListener(delegate{OnEnterSend();});
		ChatSendButton.onClick.AddListener(delegate{OnClickSend();});
		CloseButton.onClick.AddListener(delegate{OnClickClose();});

		if(Connected && ChannelToggleToInstantiate != null) {
			OnSubscribed(ChannelsToJoinOnConnect, null);
		}

		chatPanel.gameObject.SetActive(true);
	}

	public void OnClickClose() {
		Destroy(ChatPanel.gameObject);
		OnUnsubscribed(ChannelsToJoinOnConnect);
		gameObject.SetActive(false);
	}

	public void OnEnterSend()
	{
		if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
		{
			SendChatMessage(this.InputFieldChat.text);
			this.InputFieldChat.text = "";
		}
	}

	public void OnClickSend()
	{
		if (this.InputFieldChat != null)
		{
			SendChatMessage(this.InputFieldChat.text);
			this.InputFieldChat.text = "";
		}
	}

	public int TestLength = 2048;
	private byte[] testBytes = new byte[2048];

	private void SendChatMessage(string inputLine)
	{
		if (string.IsNullOrEmpty(inputLine))
		{
			return;
		}
		if ("test".Equals(inputLine))
		{
			if (this.TestLength != this.testBytes.Length)
			{
				this.testBytes = new byte[this.TestLength];
			}

			this.chatClient.SendPrivateMessage(this.chatClient.AuthValues.UserId, testBytes, true);
		}
			
		bool doingPrivateChat = this.chatClient.PrivateChannels.ContainsKey(this.selectedChannelName);
		string privateChatTarget = string.Empty;
		if (doingPrivateChat)
		{
			// the channel name for a private conversation is (on the client!!) always composed of both user's IDs: "this:remote"
			// so the remote ID is simple to figure out

			string[] splitNames = this.selectedChannelName.Split(new char[] { ':' });
			privateChatTarget = splitNames[1];
		}
		//UnityEngine.Debug.Log("selectedChannelName: " + selectedChannelName + " doingPrivateChat: " + doingPrivateChat + " privateChatTarget: " + privateChatTarget);


		if (inputLine[0].Equals('\\'))
		{
			string[] tokens = inputLine.Split(new char[] {' '}, 2);
			if (tokens[0].Equals("\\help"))
			{
				PostHelpToCurrentChannel();
			}
			if (tokens[0].Equals("\\state"))
			{
				int newState = int.Parse(tokens[1]);
				this.chatClient.SetOnlineStatus(newState, new string[] {"i am state " + newState}); // this is how you set your own state and (any) message
			}
			else if (tokens[0].Equals("\\subscribe") && !string.IsNullOrEmpty(tokens[1]))
			{
				this.chatClient.Subscribe(tokens[1].Split(new char[] {' ', ','}));
			}
			else if (tokens[0].Equals("\\unsubscribe") && !string.IsNullOrEmpty(tokens[1]))
			{
				this.chatClient.Unsubscribe(tokens[1].Split(new char[] {' ', ','}));
			}
			else if (tokens[0].Equals("\\clear"))
			{
				if (doingPrivateChat)
				{
					this.chatClient.PrivateChannels.Remove(this.selectedChannelName);
				}
				else
				{
					ChatChannel channel;
					if (this.chatClient.TryGetChannel(this.selectedChannelName, doingPrivateChat, out channel))
					{
						channel.ClearMessages();
					}
				}
			}
			else if (tokens[0].Equals("\\msg") && !string.IsNullOrEmpty(tokens[1]))
			{
				string[] subtokens = tokens[1].Split(new char[] {' ', ','}, 2);
				if (subtokens.Length < 2) return;

				string targetUser = subtokens[0];
				string message = subtokens[1];
				this.chatClient.SendPrivateMessage(targetUser, message);
			}
		}
		else
		{
			if (doingPrivateChat)
			{
				this.chatClient.SendPrivateMessage(privateChatTarget, inputLine);
			}
			else
			{
				this.chatClient.PublishMessage(this.selectedChannelName, inputLine);
			}
		}
	}

	private void PostHelpToCurrentChannel()
	{
		this.CurrentChannelText.text += HelpText;
	}
}
