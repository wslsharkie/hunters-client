﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using System.Linq;
using System;
using System.IO;

public class ClientCache : MonoBehaviour {

	private static string[] ResourceKeyNames = {"image", "json"};
	public static Dictionary<string, Texture2D> IMAGES = new Dictionary<string, Texture2D>();

	// http://answers.unity3d.com/questions/1219561/saving-json-file-to-device.html
	public static Dictionary<string, Dictionary<string, fsData>> CONFIGS = 
		new Dictionary<string, Dictionary<string, fsData>>();

	// hash of the server version
	public static Dictionary<string, string> JSONversion = new Dictionary<string, string>();

	public static int TotalResourcesToLoad = 0;
	public static int ResourcesLoaded = 0;

	public static bool UPDATE = true;

	public static bool ImagesJSONUpdated = false;

	private static ClientCache _Instance;

	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if(_Instance == null) {
			_Instance = this;
		} else if(_Instance != this) {
			Debug.LogWarning("Should never create ClientCache again!");
			Destroy(gameObject);
			return;
		}
	}

	/// <summary>
	/// Test - writing and reading a text file. Should work with any byte stream (like the one from Texture2D.EncodeToPNG)
	/// </summary>
	void Start() {
		//		Debug.Log ("Application.persistentDataPath=" + Application.persistentDataPath, this);
		//
		//		Debug.Log ("saving...", this);
		//		var bytes = System.Text.Encoding.UTF8.GetBytes ("test test test");
		//		Save ("test.txt", bytes);
		//
		//		Debug.Log ("loading...", this);
		//		var loadedBytes = Load ("test.txt");
		//		var str=System.Text.Encoding.UTF8.GetString (loadedBytes);
		//		Debug.Log ("got string: " + str, this);

		// create resource directories on local client device
		ClientCache.CreateDirectory("images");
		ClientCache.CreateDirectory("json");

		// battle configs
		//ClientCache.CreateDirectory("json/battles");
	}

	public static void Save(string name, byte[] bytes) {
		var path=System.IO.Path.Combine(Application.persistentDataPath, name);
		System.IO.File.WriteAllBytes (path, bytes);
	}

	public static byte[] Load(string name) {
		var path=System.IO.Path.Combine(Application.persistentDataPath, name);
		return System.IO.File.ReadAllBytes (path);
	}

	public static string jsonVersionKey(string jsonName) {
		return jsonName + "-version";
	}

	private static List<string> FindResources(List<string> resources, Dictionary<string, fsData> response) {
		foreach (KeyValuePair<string, fsData> entry in response) {
			// Debug.Log(entry.Key);
			// Debug.Log(Postman.RESPONSE[entry.Key]);

			if(entry.Value.IsDictionary) {
				resources = ClientCache.FindResources(resources, entry.Value.AsDictionary);
			}

			if(entry.Value.IsString 
				&& ResourceKeyNames.Contains(entry.Key)
				&& resources.Contains(entry.Value.AsString) == false) {
				resources.Add(entry.Value.AsString);

				// store api version
				if(response.ContainsKey("v")) {
					JSONversion[entry.Value.AsString] = response ["v"].AsString;
				} else {
					JSONversion[entry.Value.AsString] = "0";
				}
			}
		}

		return resources;
	}

	public static void CreateDirectory(string dirName) {
		DirectoryInfo dirInf = new DirectoryInfo(Application.persistentDataPath + "/" + dirName); 

		if(!dirInf.Exists) { 
			Debug.Log("Creating subdirectory");
			dirInf.Create();
		}
	}

	public static void ClearCache() {
		DirectoryInfo dirInf = new DirectoryInfo(Application.persistentDataPath + "/");
		dirInf.Delete(true);
	}

	public static bool LoadCacheImage(string resource) {
		Texture2D tex = null;
		string filePath = Path.Combine(Application.persistentDataPath, resource);

		if(File.Exists(filePath)) {
			byte[] fileData = ClientCache.Load(resource);
			tex = new Texture2D (2, 2);
			tex.LoadImage(fileData); // this will auto-resize the texture dimensions.
		} else {
			tex = (Texture2D)Resources.Load("Bundle/"+resource);
		}

		if(tex == null) {
			Debug.LogWarning(resource + " not found in cache!");
			return false;
		}

		ClientCache.IMAGES[resource] = tex;

		Debug.Log(resource + " loaded from cache!");
		ClientCache.ResourcesLoaded++;
		return true;
	}

	public static bool LoadJSON(string resource) {
		string currentVersion = PlayerPrefs.GetString(ClientCache.jsonVersionKey(resource));
		string apiVersion = JSONversion[resource];

		if(!currentVersion.Equals(apiVersion)) {
			return false;
		}

		string filePath = Path.Combine(Application.persistentDataPath, resource);

		if(File.Exists(filePath) == false) {
			Debug.LogWarning(resource + " not found in cache!");
			return false;
		}

		byte[] fileData = ClientCache.Load(resource);
		String jsonText = System.Text.Encoding.UTF8.GetString(fileData);
		ClientCache.CONFIGS[resource] = JsonSerializer.FromJSon(jsonText);

		Debug.Log(resource + " loaded from cache!");
		ClientCache.ResourcesLoaded++;
		return true;
	}

	public static IEnumerator DownloadImage(string resource) {
		WWW www = new WWW(GameConfig.IMAGE_URL+resource);

		Debug.Log(GameConfig.IMAGE_URL + resource);
		yield return www;

		// store in cache
		if(string.IsNullOrEmpty(www.error) && www.bytes.Length > 0) {
			Texture2D tex = new Texture2D (2, 2);
			www.LoadImageIntoTexture(tex);
			ClientCache.IMAGES[resource] = tex;

			ClientCache.Save(resource, www.bytes);
		} else {
			Debug.Log("Failed to download resource!");
		}

		ClientCache.ResourcesLoaded++;
		Debug.Log(resource+" downloaded");
	}

	public static IEnumerator DownloadJSON(string resource) {
		WWW www = new WWW(GameConfig.JSON_URL+resource);

		Debug.Log(GameConfig.JSON_URL + resource);
		yield return www;

		// store in cache
		if(string.IsNullOrEmpty(www.error) && www.text.Length > 0) {
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes (www.text);
			ClientCache.CONFIGS[resource] = JsonSerializer.FromJSon(www.text);

			ClientCache.Save(resource, bytes);
			// save version value
			PlayerPrefs.SetString(ClientCache.jsonVersionKey(resource), JSONversion[resource]);
		} else {
			Debug.Log("Failed to download resource!");
		}

		ClientCache.ResourcesLoaded++;
		Debug.Log(resource +" downloaded");
	}

	public static Texture2D GetTexture(string fileName){

		if (GameManager.LOADING != false) {
			return null;
		}

		if (IMAGES.ContainsKey(fileName)) {
			return IMAGES[fileName];
		} else if(LoadCacheImage(fileName)) {
			return IMAGES[fileName];
		} else {
			return null;
		}
	}

	public static Dictionary<string, fsData> GetConfig(string configName) {
		if (GameManager.LOADING != false) {
			return null;
		}

		if (CONFIGS.ContainsKey(configName)) {
			return CONFIGS[configName];
		} else {
			return null;
		}
	}

	public static void FindPossibleIdsInConfig(
		string jsonConfigName, 
		Dictionary<string, List<string>> jsonToIds,
		Dictionary<string, fsData> response
	) {
		Dictionary<string, fsData> config = CONFIGS[jsonConfigName];

		foreach(KeyValuePair<string, fsData> entry in response) {
			// try to add the key 
			if(config.ContainsKey(entry.Key)) {
				jsonToIds[jsonConfigName].Add(entry.Key);
			}

			// try to add value if key contains id in string...
			if(entry.Value.IsString && 
				(entry.Key.Contains("id") || entry.Key.Contains("Id"))
				&& config.ContainsKey(entry.Value.AsString)) {

				jsonToIds[jsonConfigName].Add(entry.Value.AsString);
			} else if(entry.Value.IsDictionary) {
				FindPossibleIdsInConfig(jsonConfigName, jsonToIds, entry.Value.AsDictionary);
			}
		}
	}

	/**
	 * Attempts to find ids in the response that match to client configs. 
	 * Note: Not using this function for now since there is no way to gaurantee
	 * the response has the correct matching config json name
	 */
	public static Dictionary<string, List<string>> FindConfigIdsForResponse(
		Dictionary<string, List<string>> jsonToIds, 
		Dictionary<string, fsData> response
	) {

		foreach (KeyValuePair<string, fsData> entry in response) {
			// check for config key
			// potential json config
			string jsonConfigName = entry.Key+".json";

			if(CONFIGS.ContainsKey(jsonConfigName) && entry.Value.IsDictionary) {
				if(jsonToIds.ContainsKey(jsonConfigName) == false) {
					jsonToIds [jsonConfigName] = new List<string> ();
				}
				FindPossibleIdsInConfig(jsonConfigName, jsonToIds, entry.Value.AsDictionary);
			} else if(entry.Value.IsDictionary) {
				FindConfigIdsForResponse(jsonToIds, entry.Value.AsDictionary);
			}
		}

		return jsonToIds;
	}

	public static IEnumerator CheckResourcesLoaded(Action action) {
		while(ClientCache.ResourcesLoaded < ClientCache.TotalResourcesToLoad) {
			yield return null;
		}

	//	Debug.Log("Cache Loaded!");

		if(ImagesJSONUpdated) {
			LoadImagesJSON(action);
		} else {
			GameManager.LOADING = false;
			GameManager.OnResourcesLoaded();
			action.Invoke();
		}

//		if(CONFIGS.ContainsKey("images.json")) {
//			List<string> resources = ClientCache.FindResources(new List<string>(), CONFIGS["images.json"]);
//		}

//		Debug.Log("*******");
//		Dictionary<string, List<string>> jsonToIds = LoadConfigResources(new Dictionary<string, List<string>>(), Postman.RESPONSE);
//		foreach(KeyValuePair<string, List<string>> entry in jsonToIds) {
//			Debug.Log(entry.Key);
//			foreach(var id in entry.Value) {
//				Debug.Log(id);
//			}
//		}

		//		foreach(KeyValuePair<string, Texture2D> entry in ClientCache.IMAGES) {
		//			Debug.Log(entry.Key);
		//			Debug.Log(entry.Value);
		//		}
	}

	public static void CheckImagesJSONUpdated(string resource) {
		if(resource == "images.json") {
			string currentVersion = PlayerPrefs.GetString(ClientCache.jsonVersionKey(resource));
			string apiVersion = JSONversion[resource];

			if(!currentVersion.Equals(apiVersion)) {
				ImagesJSONUpdated = true;
			}
		}
	}

	/**
	 * When the images.json is updated
	 * client will attempt to download 
	 * all images in the images.json again
	 */
	public static void LoadImagesJSON(Action action) {
		List<string> resources = new List<string> ();

		foreach(var entry in CONFIGS["images.json"]) {
			if(entry.Key.Contains(".png") || entry.Key.Contains(".jpg")) {
				resources.Add(entry.Key);
			}
		}

		Debug.Log("Loading images from images.json!");

		ImagesJSONUpdated = false;
		TotalResourcesToLoad = resources.Count();
		ResourcesLoaded = 0;

		Debug.Log(TotalResourcesToLoad + " resources to be loaded.");

		_Instance.StartCoroutine(CheckResourcesLoaded(action));

		foreach(string resource in resources) {
			bool loaded = LoadCacheImage(resource);

			if(loaded == false) {
				// download if not in cache
				_Instance.StartCoroutine(DownloadImage(resource));
			}		
		}
	}

	public static void LoadResources(Action action) {
		// reset image cache
		//IMAGES = new Dictionary<string, Texture2D>();

		List<string> resources = FindResources(new List<string>(), Postman.RESPONSE);

		//		foreach(string s in resources) {
		//			Debug.Log(s);
		//		}

		ImagesJSONUpdated = false;
		TotalResourcesToLoad = resources.Count();
		ResourcesLoaded = 0;

	//	Debug.Log(TotalResourcesToLoad + " resources to be loaded.");

		_Instance.StartCoroutine(CheckResourcesLoaded(action));

		foreach(string resource in resources) {
			bool loaded = false;

			if(resource.Contains(".png")) {
				// check cache
				loaded = LoadCacheImage(resource);

				if(loaded == false) {
					// download if not in cache
					_Instance.StartCoroutine(DownloadImage(resource));
				}
			} else if(resource.Contains(".json")) {
				// check cache
				loaded = LoadJSON(resource);
				if(GameConfig.DEBUG || loaded == false) {
					// download if not in cache
					_Instance.StartCoroutine(DownloadJSON(resource));
					// if the resource to download is images.json
					// then we will need to download all the images in images.json
				}

				CheckImagesJSONUpdated(resource);
			}
		}
	}
}
