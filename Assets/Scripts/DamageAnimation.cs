﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAnimation : MonoBehaviour {

	BattleManager manager;

	void Start () {
		if (!manager) {
			manager = GameObject.FindGameObjectWithTag("PageManager").GetComponent<BattleManager>();
		}
	}

	public void FinishedAnimation(){
		//this.gameObject.GetComponent<Animator>().enabled = false;
		//this.gameObject.SetActive(false);

		manager.StoreDamageAnimation(this.gameObject, this.name);


	}


}
