﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTxt : MonoBehaviour {

	BattleManager manager;

	void Start () {
		if (!manager) {
			manager = GameObject.FindGameObjectWithTag("PageManager").GetComponent<BattleManager>();
		}
	}

	public void FinishedAnimation(){
		manager.StoreDamageTxt(this.gameObject);
	}

}
