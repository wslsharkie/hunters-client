﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class Ability : ScriptableObject {

	Dictionary<string, string> config;
	char[] charsToTrim = {'"'};


	public void Init(int abilityID) {
		Dictionary<string, fsData> abilitiesJSON = ClientCache.GetConfig("abilities.json");

	//	FullSerializer.fsSerializer.LogDictionary(abilitiesJSON[abilityID.ToString()].AsDictionary);
		config = JsonSerializer.ParseFSData(abilitiesJSON[abilityID.ToString()].AsDictionary);


	}

	public string GetDescription(){
		if (config.ContainsKey("desc")) {
			return config["desc"].Trim(charsToTrim);
		}
		return string.Empty;
	}

	public string GetName(){
		if (config.ContainsKey("name")) {
			return config["name"].Trim(charsToTrim);
		}
		return string.Empty;

	}

}
