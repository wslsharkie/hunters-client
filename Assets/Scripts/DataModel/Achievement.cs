﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;
using System;

public class Achievement : ScriptableObject {
	public Dictionary<string, string> Config;
	public Dictionary<string, string> Instance;

	public void Init(string achId, Dictionary<string, fsData> response) {
		Dictionary<string, fsData> config = ClientCache.GetConfig("achievements.json");
		Dictionary<string, fsData> achievementData = config[achId].AsDictionary;

		Config = JsonSerializer.ParseFSData(achievementData);
		Instance = JsonSerializer.ParseFSData(response);

		//Debug.Log ("we have " + Instance.Count + " keys in instance and " + response.Count + " keys in response");
		//FullSerializer.fsSerializer.LogDictionary (response);
	}

	public string GetName(){
		return Config["name"];
	}

	public string GetValueForKey(string key){
		if (Config.ContainsKey (key)) {
			Debug.Log("Achievement " + key + " lookup returning from Config = " + Config [key].ToString());
			return Config [key];
		}

		if (Instance.ContainsKey (key)) {
			Debug.Log("Achievement " + key + " lookup returning from Instance = " + Instance [key].ToString());
			return Instance [key];
		}

		return "";
	}

	public bool IsDailyAchievement() {
		bool is_daily = false;
		if (Instance.ContainsKey("is_daily")) {


			Debug.Log("Is_daily printout = " + Instance["is_daily"]);
			is_daily = String.Equals(Instance ["is_daily"], "true");
		}
		Debug.Log ("IS DAILY = " + is_daily);
		return is_daily;
	}


	public string GetOwned(){


		foreach (KeyValuePair<string, string> entry in Instance) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		}
		//return "0";
		return Instance["amount"];
	}
}
