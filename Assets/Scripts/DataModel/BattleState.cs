﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;


//This is the current state that the stage battle/raid is in

/* example battle data 

{"_id":"5982141e9473c1085f238589",
"minions":
	{"0":{"id":4,"level":8,"health":223,"healthMax":400,"charge":1,"action":1}},
"players":{"597a546a7cdb8f0cd481841c":
	{"last_atk_pos":"1",
	"team"
		:{"0":
			{"id":1,"level":2,"stars":1,"health":-83,"healthMax":67,"charge":2,"damage":57},
		"1":
			{"id":2,"level":2,"stars":1,"health":7,"healthMax":67,"charge":2,"damage":120}},
	"energy":7,
	"name":"test",
	"stageBattle":"0:1"}},
"ownerId":"597a546a7cdb8f0cd481841c",
"battleId":4,"__v":0,
"createdAt":"2017-08-02T18:04:14.200Z",
"level":1,
"state":0,
"public":true}
*/

public class BattleState : ScriptableObject {

	//public Dictionary<string, string> Config;
	//public Dictionary<string, string> Instance;

	enum BattleTypes {StageBattle, RaidBattle, PVPBattle}; 
	BattleTypes battleType;

	Dictionary<string, Hero> heroes;
	List<Minion> minions;
	int currentMinionIndex = 0;
	string battleID;
	string battleConfigID;
	string ourPlayerID = string.Empty;
	int playerEnergy;
	int playerMaxEnergy;
	List<KeyValuePair<string, int>> damageList; //List of player names and damage done
	KeyValuePair<string, int> playerDamage; //How much damage the player has done
	Queue<string> battleLog; //record of previous actions
	bool isVictory = false; //If the battle has just ended in a victory
	bool isDefeat = false; //If the battle has just ended in defeat
	string backgroundImage;
	int stage;
	int level;
	Dictionary<string, string> StatusEffectIcons;

	char[] charsToTrim = {'"'};



	public void Init(Dictionary<string, fsData> response) {
		//Dictionary<string, fsData> config = ClientCache.GetConfig("items.json");
		//Dictionary<string, fsData> item = config[itemId].AsDictionary;

	   //Config = JsonSerializer.ParseFSData(item);
		//Instance = JsonSerializer.ParseFSData(response);

		Dictionary<string, fsData> playerData = Postman.RESPONSE["player"].AsDictionary;
		Dictionary<string, fsData> battle = Postman.RESPONSE["battle"].AsDictionary;
		Dictionary<string, fsData> config = battle["config"].AsDictionary;
		Dictionary<string, fsData> instance = battle["instance"].AsDictionary;
		Dictionary<string, fsData> minionsDict = instance["minions"].AsDictionary;
		Dictionary<string, fsData> playersDict = instance["players"].AsDictionary;

		ourPlayerID = Player.GetID();
		battleConfigID = config["id"].AsInt64.ToString();
		battleID = instance["_id"].AsString;
		battleLog = new Queue<string>();
		StatusEffectIcons = new Dictionary<string, string>();

		if (config["type"].AsInt64 == 0) {
			battleType = BattleTypes.StageBattle;
		} else if (config["type"].AsInt64 == 1) {
			battleType = BattleTypes.RaidBattle;
		}




		//Build the damage list for raids
		damageList = new List<KeyValuePair<string, int>>();
		foreach (KeyValuePair<string, fsData> entry in playersDict) {
			Dictionary<string, fsData> playerDict = entry.Value.AsDictionary;
			Dictionary<string, fsData> team = playerDict["team"].AsDictionary;
			int playerDamageDone = 0;

			foreach (KeyValuePair<string, fsData> hero in team) {
				Dictionary<string, fsData> heroDmgDict = hero.Value.AsDictionary;
				playerDamageDone += (int)heroDmgDict["damage"].AsInt64;
			}

			KeyValuePair<string, int> damageEntry = new KeyValuePair<string, int>(playerDict["name"].AsString, playerDamageDone);
			damageList.Add(damageEntry);
			if (entry.Key == ourPlayerID) {
				playerDamage = damageEntry;
			}
		}


		//Load monsters
		minions = new List<Minion>();
		foreach (KeyValuePair<string, fsData> minion in minionsDict) {
			Minion updatedMinion = (Minion)ScriptableObject.CreateInstance("Minion");
			updatedMinion.Init(minion.Value.AsDictionary);
			minions.Add(updatedMinion);
		}

		//Get the name of the background image
		backgroundImage = config["image"].AsString;


		//Load player heroes
		Dictionary<string, fsData> ourPlayerDict = playersDict[ourPlayerID].AsDictionary;
		Dictionary<string, fsData> heroDict = ourPlayerDict["team"].AsDictionary;

		heroes = new Dictionary<string, Hero>();
		foreach (KeyValuePair<string, fsData> entry in heroDict) {
			//	Debug.Log(entry.Key);
			//	Debug.Log(entry.Value);

			Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
			hero.Init(entry.Value.AsDictionary);
			heroes[entry.Key] = hero;
		}

		//FullSerializer.fsSerializer.LogKeys(ourPlayerDict);

		string stageString = ourPlayerDict["stageBattle"].AsString;
		string[] split = stageString.Split(':');
		stage = int.Parse(split[0]);
		level = int.Parse(split[1]); 



	}

	//Updates only have the instance, not the config
	public void UpdateState(Dictionary<string, fsData> newData){

		//FullSerializer.fsSerializer.LogDictionary(newData);
		Dictionary<string, fsData> battle = newData["battle"].AsDictionary;

		if (battle.ContainsKey("minions")){
			Dictionary<string, fsData> minionsDict = battle["minions"].AsDictionary;

			minions = new List<Minion>();
			foreach (KeyValuePair<string, fsData> minion in minionsDict) {
				Minion updatedMinion = (Minion)ScriptableObject.CreateInstance("Minion");
				updatedMinion.Init(minion.Value.AsDictionary);
				minions.Add(updatedMinion);
			}
		}

		if (battle.ContainsKey("players") && ourPlayerID != string.Empty){
			Dictionary<string, fsData> playersDict = battle["players"].AsDictionary;
			Dictionary<string, fsData> ourPlayerDict = playersDict[ourPlayerID].AsDictionary;
			Dictionary<string, fsData> heroDict = ourPlayerDict["team"].AsDictionary;

			heroes = new Dictionary<string, Hero>();
			foreach (KeyValuePair<string, fsData> entry in heroDict) {
				//	Debug.Log(entry.Key);
				//	Debug.Log(entry.Value);

				Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
				hero.Init(entry.Value.AsDictionary);
				heroes[entry.Key] = hero;
			}
		}


		//update the damage list for raids
		if (battle.ContainsKey("players") && damageList != null) {
			Dictionary<string, fsData> playersDict = battle["players"].AsDictionary;

			foreach (KeyValuePair<string, fsData> entry in playersDict) {
				Dictionary<string, fsData> playerDict = entry.Value.AsDictionary;
				Dictionary<string, fsData> team = playerDict["team"].AsDictionary;
				int playerDamageDone = 0;

				foreach (KeyValuePair<string, fsData> hero in team) {
					Dictionary<string, fsData> heroDmgDict = hero.Value.AsDictionary;
					playerDamageDone += (int)heroDmgDict["damage"].AsInt64;
				}

				//Check to see if the player already has an entry
				if (damageList.Exists(x => x.Key == playerDict["name"].AsString))  {
					int index = damageList.FindIndex(x => x.Key == playerDict["name"].AsString);
					damageList[index] = new KeyValuePair<string, int>(playerDict["name"].AsString, playerDamageDone);
				} else { 
					KeyValuePair<string, int> damageEntry = new KeyValuePair<string, int>(playerDict["name"].AsString, playerDamageDone);
					damageList.Add(damageEntry);
				}
			}
		}


		if (newData.ContainsKey("victory")) {
			isVictory = newData["victory"].AsBool;
		}
			
		if (newData.ContainsKey("defeat")) {
			isDefeat = newData["defeat"].AsBool;;
		}

		if (newData.ContainsKey("actions")) {
			Dictionary<string, fsData> actionData = newData["actions"].AsDictionary;
			if (actionData.ContainsKey("player")) {
				Dictionary<string, fsData> playerAction = actionData["player"].AsDictionary;
				List<fsData> logs = playerAction["logs"].AsList;
				foreach (fsData entry in logs) {
					battleLog.Enqueue(entry.AsString);

					if (battleLog.Count > 10) {
						battleLog.Dequeue();
					}

				}
			}

			if (actionData.ContainsKey("enemy")){
				Dictionary<string, fsData> enemyAction = actionData["enemy"].AsDictionary;
				List<fsData> enemyLogs = enemyAction["logs"].AsList;
				foreach (fsData entry in enemyLogs) {
					battleLog.Enqueue(entry.AsString);

					if (battleLog.Count > 10) {
						battleLog.Dequeue();
					}
				}
			}
		}

		if (newData.ContainsKey("status")){
			Dictionary<string, fsData> statusEffects = newData["status"].AsDictionary;
			Dictionary<string, fsData> minionStatusEffects = statusEffects["minions"].AsDictionary;

			string minionKey = minions[currentMinionIndex].GetID();

			Dictionary<string, fsData> abilityList = minionStatusEffects[minionKey].AsDictionary;
			if (abilityList.Count > 0) {
				Dictionary<string, fsData> abilitiesJSON = ClientCache.GetConfig("abilities.json");
				foreach (KeyValuePair<string, fsData> entry in abilityList) {
					string abilityID = entry.Value.AsInt64.ToString();
					Dictionary<string, fsData> abilityConfig = abilitiesJSON[abilityID].AsDictionary;
					//Get id + image

					StatusEffectIcons[abilityID] = abilityConfig["resource"].AsString.Trim(charsToTrim);


				}
			
			}




		}
	}

	#region Getters

	public Minion GetActiveMinion(){
		if (minions.Count > 0) {
			return minions[currentMinionIndex];
		}

		return null;
	}

	public int GetActiveMinionPosition(){
		return currentMinionIndex;
	}

	public Hero GetHeroAtSlot(int slot){

		if (heroes.ContainsKey(slot.ToString())){
			return heroes[slot.ToString()];
		}

		return null;
			
	}

	public Hero GetHeroByID(string id){

		foreach (KeyValuePair<string, Hero> entry in heroes) {
			if (entry.Value.GetID() == id) {
				return entry.Value;
			}
		}

		return null;
			
	}
		
	public Dictionary<string, Hero> GetHeroes(){

		if (heroes != null)
			return heroes;
	
		return new Dictionary<string, Hero>();
	}


	public List<KeyValuePair<string, int>> GetDamageList(){
		if (damageList != null){
			return damageList;
		}else {
			return new List<KeyValuePair<string, int>>();
		}
	}

	public int GetPlayerEnergy(){
		return playerEnergy;
	}

	public int GetPlayerMaxEnergy(){
		return playerMaxEnergy;
	}

	public bool GetIsRaidBattle(){
		if (battleType == BattleTypes.RaidBattle) {
			return true;
		}
	
		return false;
	}

	public Queue<string> GetBattleLog(){
		if (battleLog != null && battleLog.Count > 0) {
			return battleLog;
		}

		return new Queue<string>();		
	}

	public bool GetIsVictory(){
		return isVictory;
	}

	public bool GetIsDefeat(){
		return isDefeat;
	}

	public Dictionary<string, string> GetMinionStatusEffectIcons(){

		if (StatusEffectIcons != null)
			return StatusEffectIcons;

		return new Dictionary<string, string>();
	}

	public  string GetBackgroundImage(){

		if (backgroundImage != null)
			return backgroundImage;

		return string.Empty;
	}

	public string GetBattleID(){
		if (battleID != null)
			return battleID;

		return string.Empty;
	}

	public string GetStageLevelKey(){

		string key = "Stage" + stage.ToString() + "Level" + level.ToString();

		return key;
	}

	#endregion

}
