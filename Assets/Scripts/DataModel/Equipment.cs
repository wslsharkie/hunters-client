﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class Equipment : ScriptableObject {
	public string HeroId;
	public Dictionary<string, string> Config;
	public Dictionary<string, string> Instance;
	public List<List<Dictionary<string, string>>> CraftingRequirements;
	public List<Dictionary<string, string>> Bonus;
	public List<string> Desc;
	char[] charsToTrim = { '"' };

	public void Init(string heroId, string equipmentId, Dictionary<string, fsData> response) {

		CraftingRequirements = new List<List<Dictionary<string, string>>>();
		Bonus = new List<Dictionary<string, string>>();
		Desc = new List<string>();
		Instance = new Dictionary<string, string>();

		Dictionary<string, fsData> config = ClientCache.GetConfig("equipment.json");

		//First level has no bonus or crafting requirements
		Dictionary<string, string> emptyBonusDict = new Dictionary<string, string>();
		List<Dictionary<string, string>> emptyIngredientList = new List<Dictionary<string, string>>();
		Bonus.Add(emptyBonusDict);
		CraftingRequirements.Add(emptyIngredientList);
		Desc.Add(string.Empty);

		foreach (KeyValuePair<string, fsData> entry in response) {
			Instance[entry.Key] = entry.Value.ToString();
		}

		if(config.ContainsKey(equipmentId)) {

			Dictionary<string, fsData> equipment = config[equipmentId].AsDictionary;

			Config = JsonSerializer.ParseFSData(equipment);

			int i = 1; //start at 1 because we already have an existing element
			foreach (fsData ingredientsRequired in equipment["levels"].AsList) {
				CraftingRequirements.Add(new List<Dictionary<string, string>>());
				foreach (fsData ingredient in ingredientsRequired.AsList) {
					CraftingRequirements[i].Add(JsonSerializer.ParseFSData(ingredient.AsDictionary));
				}
				i++;
			}


			foreach (fsData bonusForLevel in equipment["bonus"].AsList) {
				Bonus.Add(JsonSerializer.ParseFSData(bonusForLevel.AsDictionary));
			}

			foreach (fsData descForLevel in equipment["desc"].AsList) {
				Desc.Add(descForLevel.AsString);
			}
				
		}

		Instance = JsonSerializer.ParseFSData(response);

		HeroId = heroId; 
	}

	public string GetName(){
		return Config["name"].Trim(charsToTrim);
	}

	//Does some bounds checking
	//Returns the max level if we go over
	public Dictionary<string, string> GetBonusForLevel(string level){
		int equipmentLvl = int.Parse(level);

		if (Bonus.Count == 0) {
			return null;
		} else if (equipmentLvl >= Bonus.Count) {
			return Bonus[Bonus.Count-1]; 
		} else {
			return Bonus[equipmentLvl];
		}
	}

	public string GetBonusTextForLevel(string level){
		int equipmentLvl = int.Parse(level);
		Dictionary<string, string> bonusDict;
		string BonusText = "";

		if (Bonus.Count == 0) {
			BonusText = "No Bonus";
			return BonusText;
		} else if (equipmentLvl >= Bonus.Count) {
			bonusDict = Bonus[Bonus.Count-1]; 
		} else {
			bonusDict = Bonus[equipmentLvl];
		}

		foreach (KeyValuePair<string, string> entry in bonusDict) {
			BonusText += "+" + entry.Value;
			BonusText += " " + entry.Key + "\n";
		}

		return BonusText;
	}

	public string GetDescForLevel(string level){
		int equipmentLvl = int.Parse(level);
		string DescTxt = "No Bonus";

		if (Desc.Count == 0) {
			return DescTxt;
		}else if (equipmentLvl >= Desc.Count) {
			DescTxt = Desc[Desc.Count-1]; 
		} else {
			DescTxt = Desc[equipmentLvl];
		}

		if (DescTxt == string.Empty) {
			DescTxt = "No Bonus";
		}

		return DescTxt;
	}

	public List<Dictionary<string, string>> GetIngredientsForLevel(string level){
		int equipmentLvl = int.Parse(level);

		if (CraftingRequirements.Count == 0) {
			return null;
		} else if(equipmentLvl >= CraftingRequirements.Count){
			return CraftingRequirements[0]; //This is always an empty list
		}else{
			return CraftingRequirements[equipmentLvl];
		}
	}

	public string GetLevel(){
		return Instance["level"];
	}

	public string GetID(){
		return Instance["id"];
	}
}
