﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class Hero : ScriptableObject {
	public Dictionary<string, string> config;
	public Dictionary<string, string> instance;
	public Dictionary<string, Dictionary<string, string>> status;
	public string heroID = string.Empty;
	char[] charsToTrim = {'"'};

	string[] frames = { "White_1", "White_1", "Bronze_1", "Silver_1", "LightBlue_1", "Gold_1" };
	const int essencesRequiredForLevel1 = 501;


	public void Init(Dictionary<string, fsData> response) {
		Dictionary<string, fsData> heroesJSON = ClientCache.GetConfig("heroes.json");



		instance = JsonSerializer.ParseFSData(response);

		//We can't have the same id field for heroes in the team page 
		// and in the battle page don't ask why
		if (instance.ContainsKey("heroId")) {
			heroID = instance["heroId"];
		}else if (instance.ContainsKey("id")){
			heroID = instance["id"];
		}


		config = JsonSerializer.ParseFSData(heroesJSON[heroID].AsDictionary);
		//Anything that's not a string/int, we'll need to specially parse;
		config["ability"] = ParseAbilties(heroesJSON[heroID].AsDictionary);


	}

	//Makes a hero object with only a config
	public void InitWithID(string id){

		heroID = id;
		instance = new Dictionary<string, string>();

		//id -1 is for empty hero slots
		if (id != "-1") {
			Dictionary<string, fsData> heroesJSON = ClientCache.GetConfig("heroes.json");
			config = JsonSerializer.ParseFSData(heroesJSON[id].AsDictionary);
		} else {
			config = new Dictionary<string, string>();
		}

	}

	public string GetID(){
		return heroID;
	}

	public string GetName(){
		if (config.ContainsKey("name")) {
			return config["name"].Trim(charsToTrim);
		}
		return string.Empty;
	}

	/*public Dictionary<string, Dictionary<string, string>> getStatusEffects(){
		return status;
	}*/

	public string GetCharge(){
		if (instance.ContainsKey("charge")) {
			return instance["charge"];
		}
		return string.Empty;
	}

	public int GetHealth() {
		if (instance.ContainsKey("health")) {
			return int.Parse(instance["health"]);
		}
		return 0;
	}

	public string GetImagePath(){
		if (config.ContainsKey("image")) {
			return config["image"].Trim(charsToTrim);
		}
		return string.Empty;
	}

	public string GetPortraitPath(){
		if (config.ContainsKey("portraitImage")) {
			return config["portraitImage"].Trim(charsToTrim);
		}
		return string.Empty;
	}

	public string GetFrameName(){
		if (instance.ContainsKey("stars")) {
			int stars = GetStars();
			return frames[stars];
		}
		return frames[0];
	}

	public int GetStars(){
		if (instance.ContainsKey("stars")) {
			return int.Parse(instance["stars"]);
		}
		return 0;
	}

	public float GetHealthPercentage(){
	
		if (instance.ContainsKey("health") && instance.ContainsKey("healthMax")) {
			return float.Parse(instance["health"]) / float.Parse(instance["healthMax"]);
		}
		return 0;
	}

	public float GetSpecialChargePercentage(){

		if (instance.ContainsKey("charge") && config.ContainsKey("chargeMax")) {
			return float.Parse(instance["charge"]) / float.Parse(config["chargeMax"]);
		}
		return 0;

	}

	public int GetSpeed() {
		if (config.ContainsKey("speed")){
			return int.Parse(config["speed"]);
		}
		return 0;
	}

	public int GetCurrentXP(){
		if (instance.ContainsKey("xp")){
			return int.Parse(instance["xp"]);
		}
		return 0;
	}

	public int GetXPToNextLevel(){
		if (instance.ContainsKey("xpToLevel")){
			return int.Parse(instance["xpToLevel"]);
		}
		return 0;
	}

	public string GetLevel(){
		if (instance.ContainsKey("level")){
			return instance["level"];
		}
		return string.Empty;

	}

	//Convert a list of ability ids to a comma seperated string
	string ParseAbilties(Dictionary<string, fsData> config){

		string abilityIDs = string.Empty;
		List<fsData> abilityList = config["abilities"].AsList;

		foreach (fsData entry in abilityList) {
			abilityIDs += entry.AsInt64.ToString() + ",";
		}

		//Remove the final comma
		abilityIDs = abilityIDs.Remove(abilityIDs.Length-1);

		return abilityIDs;

	}
	 
	public List<Ability> GetAbilities(){
		List<Ability> abilities = new List<Ability>();

		string[] abilityIDs = config["ability"].Split(',');

		foreach (string id in abilityIDs) {
			Ability ability = (Ability)ScriptableObject.CreateInstance("Ability");
			ability.Init(int.Parse(id));
			abilities.Add(ability);
		}

		return abilities;
	}

	public string GetEssence(){
		if (instance.ContainsKey("essence")){
			return instance["essence"];
		}
		return string.Empty;

	}

	public string GetAttack(){
		if (instance.ContainsKey("attack")){
			return instance["attack"];
		}
		return string.Empty;
	}

	public string GetDefense(){
		if (instance.ContainsKey("defense")){
			return instance["defense"];
		}
		return string.Empty;
	}

	public string GetHealthMax(){
		if (instance.ContainsKey("health")){
			return instance["health"];
		}
		return string.Empty;

	}
	public string GetCrit(){
		if (config.ContainsKey("criticalChance")){
			return config["criticalChance"];
		}
		return string.Empty;
	}
	public string GetResist(){
		if (config.ContainsKey("resistance")){
			return config["resistance"];
		}
		return string.Empty;
	}
	public string GetAgility(){
		if (config.ContainsKey("agility")){
			return config["agility"];
		}
		return string.Empty;
	}

	public float GetPercentIntoCurrentLevel(){

		if (instance.ContainsKey("experienceAtLevel") && instance.ContainsKey("experienceToNextLevel")) {
			return float.Parse(instance["experienceAtLevel"]) / float.Parse(instance["experienceToNextLevel"]);
		}

		return 0;
	}

	public bool GetOwned(){
		if (instance.ContainsKey("stars")){
			return int.Parse(instance["stars"]) > 0;
		}
		return false;

	}
		

}
