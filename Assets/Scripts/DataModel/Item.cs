﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class Item : ScriptableObject {
	public Dictionary<string, string> Config;
	public Dictionary<string, string> Instance;

	public void Init(string itemId, Dictionary<string, fsData> response) {
		Dictionary<string, fsData> config = ClientCache.GetConfig("items.json");
		Dictionary<string, fsData> item = config[itemId].AsDictionary;

		Config = JsonSerializer.ParseFSData(item);
		Instance = JsonSerializer.ParseFSData(response);
	}

	public string GetName(){
		return Config["name"];
	}

	public string GetOwned(){
		/*foreach (KeyValuePair<string, string> entry in Instance) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		} */
		//return "0";
		return Instance["amount"];
	}

	public string GetImage(){
		return Config["image"];
	}
}
