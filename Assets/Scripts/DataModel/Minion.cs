﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;

public class Minion : ScriptableObject {

	public Dictionary<string, string> instance;
	public Dictionary<string, string> config;

	//{"id":0,"level":1,"health":65,"healthMax":73,"charge":0,"status":[]}


	public void Init(Dictionary<string, fsData> minionDict) {


		instance = new Dictionary<string, string>();
		config = new Dictionary<string, string>();
		Dictionary<string, fsData> minionJSON = ClientCache.GetConfig("minions.json");

		instance = JsonSerializer.ParseFSData(minionDict);

		string minionID = instance["id"];

		config = JsonSerializer.ParseFSData(minionJSON[minionID].AsDictionary);

		//FullSerializer.fsSerializer.LogDictionary(minionDict);
		/*foreach (KeyValuePair<string, string> entry in config) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		}*/

			
	}

	public int GetHealth(){
		if (instance.ContainsKey("health")) {
			return int.Parse(instance["health"]);
		}

		return 0;
	}

	public string GetName(){
		if (config.ContainsKey("name")) {
			char[] charsToTrim = {'"'};

			return config["name"].Trim(charsToTrim);
		}
		return string.Empty;
	}

	public string GetLevel(){
		if (instance.ContainsKey("level")) {
			return instance["level"];
		}

		return string.Empty;
	}

	public int GetMaxHealth(){
		if (instance.ContainsKey("healthMax")) {
			return int.Parse(instance["healthMax"]);
		}

		return 0;
	}

	public string GetID(){
		if (config.ContainsKey("id")) {
			return config["id"];
		}

		return string.Empty;
	}

	public string GetImage(){
		if (config.ContainsKey("image")){
			return config["image"];
		}

		return string.Empty;
	}

	public float GetHealthPercentage(){
		if (instance.ContainsKey("health") && instance.ContainsKey("healthMax")) {
			return float.Parse(instance["health"]) / float.Parse(instance["healthMax"]);
		}
		return 0;

	}

	public float GetSpecialChargePercentage(){

		if (instance.ContainsKey("action") && config.ContainsKey("actionMax")) {
			return float.Parse(instance["action"]) / float.Parse(config["actionMax"]);
		}
		return 0;

	}

	public Vector2 GetPositionOffset(){
		if (config.ContainsKey("displayXOffset") && config.ContainsKey("displayYOffset")) {
			Debug.Log("found position offset");
			return new Vector2(float.Parse(config["displayXOffset"]), float.Parse(config["displayYOffset"]));
		}

		Debug.Log("didn't find position offset");

		return Vector2.zero;

	}
}
