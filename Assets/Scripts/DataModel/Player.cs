﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;
using System;

public class Player {
	public static Dictionary<string, string> Instance = null;

	public static void Init(Dictionary<string, fsData>response) {
		if(!response.ContainsKey("player")) {
			Debug.LogError ("No player key found in response!");
			return;
		}

		Dictionary<string, string> player = JsonSerializer.ParseFSData(response["player"].AsDictionary);

		if(Instance == null) {
			Instance = player;
		} else {
			// only update the keys/values the server returns
			foreach(var entry in player) {
				Instance[entry.Key] = player[entry.Key];
			}
		}
	}
		
	public static string GetValue(string key) {
		return (Instance.ContainsKey(key)) ? Instance[key].Trim(GameConfig.CharsToTrim) : "";
	}

	public static float GetFloatValue(string key) {
		return (Instance.ContainsKey(key)) ? (float)Int32.Parse(Instance[key]) : 0f;
	}

	public static string GetID(){
		if (Instance.ContainsKey("_id")) {
			return Instance["_id"];
		}

		return string.Empty;
	}

	public static string GetName(){
		return (Instance.ContainsKey("name")) ? Instance["name"].Trim(GameConfig.CharsToTrim) : "";
	}

	public static string GetSquadId() {
		return (Instance.ContainsKey("squadId")) ? Instance["squadId"].Trim(GameConfig.CharsToTrim) : "";
	}
}
