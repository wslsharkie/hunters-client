﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class PlayerLoadouts : ScriptableObject {

	List<List<Hero>> loadouts;
	string activeLoadout = "0";

	public void Init(List<List<string>> heroIDs) {

		loadouts = new List<List<Hero>>();

		foreach (List<string> loadout in heroIDs) {
			List<Hero> heroList = new List<Hero>();
			foreach (string id in loadout) {
				Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
				hero.InitWithID(id);
				heroList.Add(hero);
			}
			loadouts.Add(heroList);
		}
	}

	public string GetFlatHeroIDs() {

		string flatHeroIDs = string.Empty;

		foreach (List<Hero> loadout in loadouts) {
			foreach (Hero hero in loadout) {
				flatHeroIDs += hero.GetID() + ":";
			}
		}

		//remove the final colon
		if(flatHeroIDs.Length > 0){
			flatHeroIDs = flatHeroIDs.Remove(flatHeroIDs.Length-1);
		}

		return flatHeroIDs;
	}

	public void ChangeHero(int loadout, int position, string heroID){
		
		Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
		hero.InitWithID(heroID);

		loadouts[loadout][position] = hero;

	}

	public string GetActiveLoadout(){
		return activeLoadout;
	}

	public void SetActiveLoadout(string newActiveLoadout){
		activeLoadout = newActiveLoadout;
	}

	//Overloaded comparison operator
	public static bool operator ==(PlayerLoadouts a, PlayerLoadouts b){
		
		if (object.ReferenceEquals(null, a) && object.ReferenceEquals(null, b)) {
			return true;
		} else if (object.ReferenceEquals(null, a) || object.ReferenceEquals(null, b)) {
			return false;
		}

		if (a.GetActiveLoadout() != b.GetActiveLoadout()) {
			return false;
		}


		for(int i = 0; i < a.loadouts.Count; i ++) {
			for(int j = 0; j < a.loadouts[i].Count; j++){
				if (a.loadouts[i][j].GetID() != b.loadouts[i][j].GetID()) {
					return false;
				}
			}
		}

		return true;
	}

	//Companion not equals operator
	public static bool operator !=(PlayerLoadouts a, PlayerLoadouts b){

		if (object.ReferenceEquals(null, a) && object.ReferenceEquals(null, b)) {
			return false;
		} else if (object.ReferenceEquals(null, a) || object.ReferenceEquals(null, b)) {
			return true;
		}

		if (a.GetActiveLoadout() != b.GetActiveLoadout()) {
			return true;
		}


		for(int i = 0; i < a.loadouts.Count; i ++) {
			for(int j = 0; j < a.loadouts[i].Count; j++){
				if (a.loadouts[i][j].GetID() != b.loadouts[i][j].GetID()) {
					return true;
				}
			}
		}

		return false;
	}

}
