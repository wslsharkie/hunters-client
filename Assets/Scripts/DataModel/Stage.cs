﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;
using System;

public class Stage : ScriptableObject {
	public Dictionary<string, string> Config;
	public List<StageBattle> Battles; // player stage battles
	public bool Locked = true;

	public void Init(string stageId, Dictionary<string, fsData>response) {
		Dictionary<string, fsData> stagesJSON = ClientCache.GetConfig("stages.json");
		Dictionary<string, fsData> stageJSON = stagesJSON[stageId].AsDictionary;
		Config = JsonSerializer.ParseFSData(stageJSON);
		List<fsData> battleConfigs = stageJSON["battles"].AsList;

		if(response.Count > 0) {
			Locked = false;

			Battles = new List<StageBattle>();
			foreach(var configEntry in battleConfigs) {
				StageBattle battle = (StageBattle)ScriptableObject.CreateInstance("StageBattle");
				Dictionary<string, fsData> battleConfig = configEntry.AsDictionary;
				Dictionary<string, fsData> battleInstance = null;
				string battleId = battleConfig["id"].AsInt64.ToString();

				if(response.ContainsKey(battleId)) {
					battleInstance = response[battleId].AsDictionary;
				}

				battle.Init(stageId, battleConfig, battleInstance);
				Battles.Add(battle);
			}
		}
	}

	public string GetValue(string key) {
		return (Config.ContainsKey(key)) ? Config[key].Trim(GameConfig.CharsToTrim) : "";
	}

	public float GetFloatValue(string key) {
		return (Config.ContainsKey(key)) ? (float)Int32.Parse(Config[key]) : 0f;
	}

	public string GetName() {
		return Config["name"].Trim(GameConfig.CharsToTrim);
	}
}
