﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullSerializer;


public class StageBattle : ScriptableObject {
	private string StageID;
	public Dictionary<string, string> CONFIG = null;
	public Dictionary<string, string> INSTANCE = null;
	public bool Locked = true;

	public void Init(string stageId, Dictionary<string, fsData>config, Dictionary<string, fsData>instance) {
		StageID = stageId;
		CONFIG = JsonSerializer.ParseFSData(config);
		INSTANCE = new Dictionary<string, string>();

		if(instance != null && instance.Count > 0) {
			INSTANCE = JsonSerializer.ParseFSData(instance);
			Locked = false;
		}
	}

	public string GetValue(string key) {
		string value = "";

		if(CONFIG.ContainsKey(key)) {
			value = CONFIG [key];
		} else if(INSTANCE != null && INSTANCE.ContainsKey(key)){
			value = INSTANCE [key];
		}

		return value;
	}

	public float GetFloatValue(string key) {
		return (CONFIG.ContainsKey(key)) ? (float)Int32.Parse(CONFIG[key]) : 0f;
	}

	public string GetID() {
		return CONFIG["id"];
	}

	public string GetName() {
		return CONFIG["name"].Trim(GameConfig.CharsToTrim);
	}

	public int GetTimesCompleted(){
		if (INSTANCE.ContainsKey("completed")){
			return int.Parse(INSTANCE["completed"]);
		}
	
		return 0;
	}

	public int GetCompletionsRequired(){
		if (INSTANCE.ContainsKey("compl_needed")){
			return int.Parse(INSTANCE["compl_needed"]);
		}

		return 1;
	}
}
