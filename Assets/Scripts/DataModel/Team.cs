﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class Team {
	public const bool DEBUG = true;

	public static Dictionary<string, Hero> TEAM = null;

	public static void Init(Dictionary<string, fsData>response) {
		if(!response.ContainsKey("team")) {
			Debug.LogError ("No team key found in response!");
		}

		TEAM = new Dictionary<string, Hero>();

		foreach(KeyValuePair<string, fsData> entry in response["team"].AsDictionary) {
			Hero hero = ScriptableObject.CreateInstance("Hero") as Hero;
			hero.Init(entry.Value.AsDictionary);
			TEAM.Add (entry.Key, hero);
		}
	}

	public static Hero FindAttackingHero() {
		float max = 0;
		Hero attackingHero = null;	

		foreach(KeyValuePair<string, Hero> entry in TEAM) {
			Hero hero = entry.Value;

			if(hero.GetHealth() <= 0) {
				continue;
			}
		
			max += hero.GetSpeed();
		}

		float roll = Random.Range (1f, max);
		float rangeMin = 1f;
		float rangeMax = 1f;

		foreach(KeyValuePair<string, Hero> entry in TEAM) {
			Hero hero = entry.Value;

			if(hero.GetHealth() <= 0) {
				continue;
			}

			rangeMax = rangeMin + hero.GetSpeed();

			if(roll >= rangeMin && roll <= rangeMax) {
				attackingHero = hero;
				break;
			}

			rangeMin = rangeMax + 1f;
		}

		return attackingHero;
	}
}
