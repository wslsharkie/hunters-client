﻿using UnityEngine;
using System.Collections;
using FullSerializer;
using System.Collections.Generic;

public class GameConfig {



	#if UNITY_EDITOR
		//DEBUG
		public const bool DEBUG = true;
		public const string	SERVER_URL = "http://localhost:3000/";
		public const string	IMAGE_URL = "http://localhost:3000/images/";
		public const string	JSON_URL = "http://localhost:3000/json/";
	#else
		// RELEASE:
		public const bool DEBUG = false;
		public const string SERVER_URL = "http://34.208.93.104/";
		public const string IMAGE_URL = "http://34.208.93.104/images/";
		public const string JSON_URL = "http://34.208.93.104/json/"; 
	#endif




	public const string GAME_NAME = "Wyld Hunters";
	public static char[] CharsToTrim = {'"'};

	private static string _ACCESS_TOKEN = "";

	public static Dictionary<string, fsData> RESPONSE = null;

	public static string GetAccessToken() {
		if (string.Equals(_ACCESS_TOKEN, "")) {
			_ACCESS_TOKEN = PlayerPrefs.GetString("access_token");
		}

		return _ACCESS_TOKEN;
	}

	public static void DeleteToken() {
		PlayerPrefs.DeleteKey("access_token");
		_ACCESS_TOKEN = "";
	}
}
