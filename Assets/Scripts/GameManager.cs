﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FullSerializer;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public static bool LOADING = false;
	public static PlayerLoadouts currentLoadouts;
	public static PlayerLoadouts savedLoadouts;

	void Awake(){
		if(Instance == null) {
			Instance = this;
			DontDestroyOnLoad(gameObject);
		} else if(Instance != this) {
			Destroy(gameObject);    
		}

		//SceneManager.sceneLoaded += SavePlayerLoadouts;
		SceneManager.sceneLoaded += OnSceneLoad;
	}

	// Use this for initialization
	void Start () {


		//for testing. clears all images
		//ClientCache.ClearCache();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnSceneLoad(Scene scene, LoadSceneMode mode){
		SavePlayerLoadouts();
	}

	void SavePlayerLoadouts(){

		//checking to see if we should save the player loadouts
		//Debug.Log("Check to see if we should resave play loadouts");

		if (savedLoadouts != null && currentLoadouts != null && savedLoadouts != currentLoadouts) {
			Dictionary<string, string> body = new Dictionary<string, string>();

			Debug.Log(currentLoadouts.GetFlatHeroIDs());
			body["teams"] = currentLoadouts.GetFlatHeroIDs();
			body["active"] = currentLoadouts.GetActiveLoadout();

			StartCoroutine(Postman.SaveTeam(body, UpdatePlayerLoadouts)); 

	
		}
	}

	void UpdatePlayerLoadouts(){
		//Debug.Log("updated player loadouts");
		List<List<string>> teamHeroIDs = new List<List<string>>();

		//Grab the hero id from the response
		Dictionary<string, fsData> teamInfo = Postman.RESPONSE["team"].AsDictionary;
		long activeLoadoutID = teamInfo["activeTeamId"].AsInt64;
		List<fsData> teams = teamInfo["teams"].AsList;
		List<fsData> currentHeroIDs = teams[(int)activeLoadoutID].AsList;

		//Setup loadouts
		int loadout = 0;
		foreach (fsData entry in teams) {
			teamHeroIDs.Add(new List<string>());
			List<fsData> heroIDs = teams[loadout].AsList;

			for (int i = 0; i < heroIDs.Count; i++) {
				teamHeroIDs[loadout].Add(heroIDs[i].AsInt64.ToString());
			}
			loadout++;
		}
		GameManager.currentLoadouts = (PlayerLoadouts)ScriptableObject.CreateInstance("PlayerLoadouts");
		GameManager.currentLoadouts.Init(teamHeroIDs);

		savedLoadouts = currentLoadouts;
	}

	public static void OnResourcesLoaded() {
		if(LOADING) {
			Debug.LogError("Game is loading!");
		}

		if(Postman.RESPONSE.ContainsKey("player")) {
			Player.Init(Postman.RESPONSE);

			if(NavBarManager.Instance != null) {
				NavBarManager.Instance.UpdateTopBar();
			}
		}
	}

	public static void ReloadGame() {
		GameObject[] GameObjects = (FindObjectsOfType<GameObject>() as GameObject[]);
		for (int i = 0; i < GameObjects.Length; i++)
		{
			Debug.Log(GameObjects [i]);
			Destroy(GameObjects[i]);
		}
		SceneLoader.LoadGame();
	}
}
