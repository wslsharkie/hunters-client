﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GatewayManager : MonoBehaviour {

	Canvas Canvas;
	Image HeroImage;
	Text ShardsText;

	void Awake() {
		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		HeroImage = Canvas.transform.Find("HeroImage").GetComponent<Image>();
		ShardsText = Canvas.transform.Find("ShardsText").GetComponent<Text>();

		ResetSummon();
	}

	// Use this for initialization
	void Start () {
		if (Application.isEditor && !GameObject.Find("GameManager")) {
			SceneLoader.LoadGame();
		}
	}
	
	public void SummonClick() {
		ResetSummon();

		Dictionary<string, string> body = new Dictionary<string, string>();
		// hard coded for now!
		body["gateway_id"] = "0";
		StartCoroutine(Postman.SummonHero(body, ProcessSummonedHero));
	}

	void ProcessSummonedHero() {
		Dictionary<string, string> award = JsonSerializer.ParseFSData(Postman.RESPONSE["award"].AsDictionary);

		Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
		hero.InitWithID(award["hero_id"]);

		ShardsText.text = "Summoned "+award["shards"] + " Essence for "+hero.GetName()+"!";

		Texture2D heroTexture = ClientCache.GetTexture(hero.GetImagePath());

		if (heroTexture != null) {
			HeroImage.sprite = Sprite.Create(
				heroTexture, 
				new Rect(0, 0, heroTexture.width, heroTexture.height), new Vector2(0.5f, 0.5f)
			);
			HeroImage.gameObject.SetActive(true);

			HeroImage.gameObject.GetComponent<Animator>().SetTrigger("summon");
		}

	}

	void ResetSummon() {
		HeroImage.gameObject.SetActive(false);
		ShardsText.text = "";
	}
}
