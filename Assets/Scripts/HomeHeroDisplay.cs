﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeHeroDisplay : MonoBehaviour {

	public List<Texture2D> heroImages;
	List<string> heroFileNames = new List<string>();
	int heroToDisplay = 0;

	// Use this for initialization
	void Start () {
		/*heroImages = new List<Texture2D>();
		Team.Init(Postman.RESPONSE);
		foreach(KeyValuePair<string, Hero> kvp in Team.TEAM) {
			Hero hero = kvp.Value;
			heroFileNames.Add(hero.GetImagePath());
		}*/
		//StartCoroutine(SetupHeroDispay());
	}

	IEnumerator SetupHeroDispay(){
		while (GameManager.LOADING) {
			yield return null;
		}

		heroImages = new List<Texture2D>();
		Debug.Log("Loading team");
		Team.Init(Postman.RESPONSE);
		foreach(KeyValuePair<string, Hero> kvp in Team.TEAM) {
			Hero hero = kvp.Value;
			heroFileNames.Add(hero.GetImagePath());
		}


		foreach (string fileName in heroFileNames) {
			Texture2D texture = ClientCache.GetTexture(fileName);
			if (texture != null) {
				heroImages.Add(texture);
			}

		}

		//Display a random hero image
		Texture2D heroTexture = heroImages[Random.Range(0, heroImages.Count)];
		Sprite newSprite = Sprite.Create(heroTexture, new Rect(0, 0, heroTexture.width, heroTexture.height), new Vector2(0.5f, 0.5f));	
		gameObject.GetComponent<Image>().sprite = newSprite;


		//Not sure if I like the animation
		//gameObject.GetComponent<Animator>().SetTrigger("Start");
	}

	void SwapHeroImage(){
		heroToDisplay++;

		if(heroToDisplay == heroImages.Count) {
			heroToDisplay = 0;
		}
			
		Texture2D texture = heroImages[heroToDisplay];
		Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));	
		gameObject.GetComponent<Image>().sprite = newSprite;

	}
}
