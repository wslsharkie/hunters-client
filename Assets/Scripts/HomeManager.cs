﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using FullSerializer;

public class HomeManager : MonoBehaviour {

	private GameObject _CurrentPopup;

	void Start () {
		if (Application.isEditor 
			&& !GameObject.FindGameObjectWithTag("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
		}
			

		if(Postman.RESPONSE.ContainsKey("team")) {
			Dictionary<string, fsData> team = Postman.RESPONSE["team"].AsDictionary;
			//Debug.Log(Postman.RESPONSE["team"]);
		}

		NewPlayerExperienceManager.FireEvent("alpha");

		if (NewPlayerExperienceManager.FireEvent("NewPlayerExperience0")) {
			//Start the glow effect
			GameObject.Find("WorldBtn").GetComponent<Animator>().enabled = true;
		}

		//Direct the player towards the hero page at this point
		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience4")){
			if (NewPlayerExperienceManager.FireEvent("NewPlayerExperience5")) {
				NavBarManager.BotBar.transform.Find("HeroBtn").GetComponent<Animator>().SetTrigger("glow");
			}
		}
	}
		
	public void CampaignMapOnClick() {
		StartCoroutine(Postman.GetStages(SceneLoader.LoadStages));
	}
		
	public void InboxBtnOnClick() {
		StartCoroutine(Postman.GetInbox(SceneLoader.LoadInbox));
	}

	public void PVPBtnOnClick(){

		GameObject popup = PopupManager.OpenPopup("DialogPopup");
		PopupManager.SetTextForSimplePopup(popup, "Feature disabled for Alpha");


		//StartCoroutine(Postman.GetPVPList(SceneLoader.LoadPVPList));
	}


	/*
	public void QuestBattlePressed() {
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["battle_id"] = "0";
		StartCoroutine(Postman.CreateBattle(body, SceneLoader.LoadBattle));
	}*/

	public void TeamBtnOnClick(){
		StartCoroutine(Postman.GetTeam(SceneLoader.LoadTeam));
	}

	public void RaidBtnOnClick(){

		//GameObject popup = PopupManager.OpenPopup("DialogPopup");
		//PopupManager.SetTextForSimplePopup(popup, "Feature disabled for Alpha");

		StartCoroutine(Postman.GetRaids(this.LoadBattleOrRaidList));
	}

	public void OnNewsBtnClick(){
		SceneLoader.LoadNews();
	}

	public void MoreBtnClick() {
		SceneLoader.LoadSettings();
	}

	//Determine if we should open the raid List or go directly to battle
	public void LoadBattleOrRaidList(){

		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		}

		if (Postman.RESPONSE.ContainsKey("list")) {
			SceneLoader.LoadRaid();
		} else {
			SceneLoader.LoadBattle();
		}
	}

	public void FormSquadPressed() {
		_CurrentPopup = Instantiate(Resources.Load("Popups/TextField", typeof(GameObject))) as GameObject;
		_CurrentPopup.SetActive(false);

		// set confirm button action
		Button confirm = _CurrentPopup.GetComponentInChildren<Button>();
		confirm.onClick.AddListener(PostFormSquad);
		_CurrentPopup.SetActive(true);
	}

	public void PostFormSquad() {
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["name"] = "New Squad";
		StartCoroutine(Postman.CreateSquad(body, SceneLoader.SquadFormed));
	}
}

