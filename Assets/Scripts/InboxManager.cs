﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InboxManager : MonoBehaviour {

	public Canvas inboxCanvas;
	public GameObject LoadingPopup;
	public GameObject DailyLogList;
	public GameObject LifetimeLogList;
	public ScrollingList DailyScrollArea;
	public ScrollingList LifetimeScrollArea;
	//public GameObject DailyScrollArea;
	//public GameObject LifetimeScrollArea;
	public Toggle DailyToggle;
	public Toggle LifetimeToggle;
	Dictionary<string, Achievement> DailyAchievements;
	Dictionary<string, Achievement> LifetimeAchievements;


	// Use this for initialization
	void Start () {

		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

//		if (!LoadingPopup)
//			LoadingPopup = GameObject.FindGameObjectWithTag("loadingPopup");

		inboxCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();

		DailyLogList = inboxCanvas.gameObject.transform.Find("DailyLogList").gameObject;
		LifetimeLogList = inboxCanvas.gameObject.transform.Find("LifetimeLogList").gameObject;

		DailyScrollArea = DailyLogList.transform.Find ("ScrollingList").gameObject.GetComponent<ScrollingList>();
		//DailyScrollArea = DailyLogList.transform.Find ("DailyScrollArea/ScrollContent").gameObject;
		LifetimeScrollArea = LifetimeLogList.transform.Find ("ScrollingList").gameObject.GetComponent<ScrollingList>();
		//LifetimeScrollArea = LifetimeLogList.transform.Find ("LifetimeScrollArea/ScrollContent").gameObject;

		DailyAchievements = new Dictionary<string, Achievement> ();
		LifetimeAchievements = new Dictionary<string, Achievement> ();

		DailyToggle = inboxCanvas.transform.Find ("DailyToggle").GetComponent<Toggle> ();
		LifetimeToggle = inboxCanvas.transform.Find ("LifetimeToggle").GetComponent<Toggle> ();

		DailyToggle.onValueChanged.AddListener(onDailyToggleClick);
		LifetimeToggle.onValueChanged.AddListener(onLifetimeToggleClick);


		SetupInbox (Postman.RESPONSE);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetupInbox(Dictionary<string, fsData> data){

		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			Debug.Log ("!!! POSTMAN PRINT for Inbox Page");
			Debug.Log (entry.Key);
			Debug.Log (entry.Value);
		}

		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			//Debug.Log ("+++++++++ Instantiating achievement scriptable objects");
			Dictionary<string, fsData> achData = entry.Value.AsDictionary;
			//Debug.Log(achData);
			Achievement newAch = (Achievement)ScriptableObject.CreateInstance("Achievement");
			string achID = entry.Key.ToString();
			newAch.Init(achID, achData);
			if (newAch.IsDailyAchievement()) {
				DailyAchievements [achID] = newAch;
			} else {
				LifetimeAchievements [achID] = newAch;
			}
		}
			
		char[] charsToTrim = {'"'};
		GameObject InboxPlank = (GameObject)Resources.Load ("Prefabs/LogPlank");
		float inboxPlankHeight = 0;// InboxPlank.GetComponent<RectTransform> ().rect.height;
		float anchorOffset = 0;//(DailyScrollArea.GetComponent<RectTransform> ().rect.height / 2) - (inboxPlankHeight / 2);
		bool canCollect = false;

		List<GameObject> dailyPlanks = new List<GameObject>();
		foreach (KeyValuePair<string, Achievement> ach in DailyAchievements) {
			GameObject instanceOfPlank = Instantiate (InboxPlank, DailyScrollArea.gameObject.transform, false);
			//instanceOfPlank.transform.localPosition = new Vector2 (0, anchorOffset);
			instanceOfPlank.transform.Find ("titleText").GetComponent<Text> ().text = ach.Value.GetName().Trim(charsToTrim);
			instanceOfPlank.transform.Find ("descriptionText").GetComponent<Text> ().text = ach.Value.GetValueForKey ("desc").Trim(charsToTrim);
			instanceOfPlank.transform.Find ("rewardsText").GetComponent<Text> ().text = ach.Value.GetValueForKey ("rewards").Trim(charsToTrim);
			canCollect = bool.Parse(ach.Value.GetValueForKey ("canCollect"));
			if (canCollect) {
				instanceOfPlank.transform.Find ("claimButton").GetComponent<Button> ().onClick.AddListener (delegate {
					OnClaimClick (ach.Key);
				});
			} else {
				instanceOfPlank.transform.Find ("claimButton").gameObject.SetActive (false);
			}
			//anchorOffset -= inboxPlankHeight;
			dailyPlanks.Add (instanceOfPlank);
		}

		//add Dummy achievements to show scrollView
//		int sum = 10; //# of achievements
//		for (int i = 0; i < sum; i++) {
//			GameObject instanceOfPlank = Instantiate (InboxPlank, DailyScrollArea.gameObject.transform, false);
//			instanceOfPlank.transform.localPosition = new Vector2 (0, anchorOffset);
//			instanceOfPlank.transform.FindChild ("titleText").GetComponent<Text> ().text = "Text" + i.ToString ();
//			anchorOffset -= inboxPlankHeight;
//		}
	

		//setup Lifetime Planks
		inboxPlankHeight = 0;//InboxPlank.GetComponent<RectTransform> ().rect.height;
		anchorOffset = 0;//(LifetimeScrollArea.GetComponent<RectTransform> ().rect.height / 2) - (inboxPlankHeight / 2);

		List<GameObject> lifetimePlanks = new List<GameObject>();
		foreach (KeyValuePair<string, Achievement> ach in LifetimeAchievements) {
			GameObject instanceOfPlank = Instantiate (InboxPlank, LifetimeScrollArea.gameObject.transform, false);
			//instanceOfPlank.transform.localPosition = new Vector2 (0, anchorOffset);
			instanceOfPlank.transform.Find ("titleText").GetComponent<Text> ().text = ach.Value.GetName().Trim(charsToTrim);
			instanceOfPlank.transform.Find ("descriptionText").GetComponent<Text> ().text = ach.Value.GetValueForKey ("desc").Trim(charsToTrim);
			instanceOfPlank.transform.Find ("rewardsText").GetComponent<Text> ().text = ach.Value.GetValueForKey ("rewards").Trim(charsToTrim);
			canCollect = bool.Parse(ach.Value.GetValueForKey ("canCollect"));
			if (canCollect) {
				instanceOfPlank.transform.Find ("claimButton").GetComponent<Button> ().onClick.AddListener (delegate {
					OnClaimClick (ach.Key);
				});
			} else {
				instanceOfPlank.transform.Find ("claimButton").gameObject.SetActive (false);
			}
			//anchorOffset -= inboxPlankHeight;
			lifetimePlanks.Add(instanceOfPlank);
		}


//		sum = 10; //# of achievements
//		for (int i = 0; i < sum; i++) {
//			GameObject instanceOfPlank = Instantiate (InboxPlank, LifetimeScrollArea.gameObject.transform, false);
//			instanceOfPlank.transform.localPosition = new Vector2 (0, anchorOffset);
//			instanceOfPlank.transform.FindChild ("titleText").GetComponent<Text> ().text = "Text" + i.ToString ();
//			anchorOffset -= inboxPlankHeight;
//		}

		DailyScrollArea.Populate(dailyPlanks);
		LifetimeScrollArea.Populate(lifetimePlanks);

		LifetimeLogList.SetActive (false);
		DailyLogList.SetActive (true);

	}

	void onDailyToggleClick(bool toggleValue) {
		LifetimeLogList.SetActive (false);
		DailyLogList.SetActive (true);
	}

	void onLifetimeToggleClick(bool toggleValue) {
		DailyLogList.SetActive (false);
		LifetimeLogList.SetActive (true);
	}

	public void OnClaimClick(string achievement_id){

		Dictionary<string, string> body = new Dictionary<string, string>();

		body["ach_id"] = achievement_id;

		StartCoroutine(Postman.ClaimAchievement(body, OnClaimResponse));

		//popup.transform.FindChild ("HeroNameTxt").GetComponent<Text> ().text = "Hello World! " + achievement_id;
		//show loading popup
		//LoadingPopup.SetActive(true);
	}

	public void OnClaimResponse() {
		//hide the loading popup
		//LoadingPopup.SetActive(false);

		GameObject popup = PopupManager.OpenPopup("TextSmallPopup");
		PopupManager.SetTextForSimplePopup(popup, "Rewards Claimed");
	//	popup.transform.Find("Layout/Text").GetComponent<Text> ().text = "Rewards Claimed";

		/*
		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			if (entry.Key == "rewards") {
				popup.transform.Find ("Text").GetComponent<Text> ().text = entry.Value.ToString();
			}
		} */

		//SceneLoader.LoadHome();
	}
}
