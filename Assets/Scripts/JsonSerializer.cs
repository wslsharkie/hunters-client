﻿using System;
using System.Collections.Generic;
using FullSerializer;

public static class JsonSerializer {
	private static readonly fsSerializer _serializer = new fsSerializer();

	public static string Serialize(Type type, object value) {
		// serialize the data
		fsData data;
		_serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();

		// emit the data via JSON
		return fsJsonPrinter.CompressedJson(data);
	}

	public static object Deserialize(Type type, string serializedState) {
		// step 1: parse the JSON data
		fsData data = fsJsonParser.Parse(serializedState);

		// step 2: deserialize the data
		object deserialized = null;
		_serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

		return deserialized;
	}

	public static Dictionary<string, fsData> FromJSon(string serializedState) {
		fsData data = fsJsonParser.Parse(serializedState);
		return data.AsDictionary;
	}

	// This function will skip all values that can not be converted to string!
	public static Dictionary<string, string> ParseFSData(Dictionary<string, fsData> dictionary) {
		Dictionary<string, string> parseDictionary = new Dictionary<string, string>();

		foreach(KeyValuePair<string, fsData> entry in dictionary) {
			if(entry.Value.IsString || entry.Value.IsInt64 || entry.Value.IsBool || entry.Value.IsDouble) {
				parseDictionary[entry.Key] = entry.Value.ToString().Trim(GameConfig.CharsToTrim);
			}
		}

		return parseDictionary;
	}
}
