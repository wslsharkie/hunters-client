﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimationActions : MonoBehaviour {

	BattleManager manager;

	// Use this for initialization
	void Start () {
		 manager = GameObject.FindGameObjectWithTag("PageManager").GetComponent<BattleManager>();
	}

	public void CounterAttackFinished(){
		manager.MonsterCounterAttackFinished();
	}

}
