﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FullSerializer;
using UnityEngine.UI;


public class NavBarManager : MonoBehaviour {

	public static NavBarManager Instance;

	public static Stack<string> sceneHistory;
	public static GameObject TopBar;
	public static GameObject BotBar;
	public static ChatManager ChatManager;
	public static string lastChat;

	public enum NavBarState {TopOnly, BottomOnly, BothVisble, BothHidden};
	public static NavBarState currentBarState;

	void Awake() {
		//Check if instance already exists
		if(Instance == null) {
			Instance = this;
		} else if(Instance != this) {
			Debug.LogWarning("Should never create NavBarManager again!");
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start () {
		//Need to fire this off manually the first time as the sceneloaded event as already occured
		//by now
		SceneManager.sceneLoaded += OnSceneLoaded;

		// We need this since NavBarManager is first created on Home Scene
		// Should move NavBarManager to LoadGame
		Instance.LoadNavBars();
	}

	void Destroy(){
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		Instance.LoadNavBars();
	}
		
	//creates the nav bars but doesn't populate them with new data
	//We use old data if we have any
	void LoadNavBars(){
		//sceneHistory.Push(scene.name);

		//Check to see if both bars exist
		if (!TopBar){
			TopBar = GameObject.Find("TopBar");
			if (!TopBar) {
				GameObject topbarPrefab = (GameObject)Resources.Load("Prefabs/TopBar");

				GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
				if (canvas){
					TopBar = Instantiate(topbarPrefab, canvas.transform, false);
					TopBar.transform.localPosition = Vector2.zero;
				}
			} 
		}

		if (!BotBar){
			BotBar = GameObject.Find("BotBar");
			if (!BotBar) {
				GameObject botbarPrefab = (GameObject)Resources.Load("Prefabs/BotBar");

				GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
				if (canvas){
					BotBar = Instantiate(botbarPrefab, canvas.transform, false);
					BotBar.transform.localPosition = Vector2.zero;
					SetupBotNavBar();
				}
			} 

			SetActiveBotNav(SceneManager.GetActiveScene().name);
		}

		if(Player.Instance != null && Player.Instance.Count > 0) {
			UpdateTopBar();
		}
	}

	public void BackBtnClick(){
		string previousScene = sceneHistory.Pop();

		//todo: check to make sure the scene is in the build settings
		//todo: load it if it isn't
		if (previousScene != string.Empty) {
			SceneManager.LoadScene(previousScene);
		}
	}

	public void HomeBtnClick(){
		StartCoroutine(Postman.GetHome(SceneLoader.LoadHome));
	}

	public void OnSquadBtnClick(){
		if (SceneManager.GetActiveScene().name != "Squad") {

			if (Player.GetSquadId() == string.Empty) {
				StartCoroutine(Postman.GetSquads(SceneLoader.LoadSquad));
			} else {
				Dictionary<string, string> body = new Dictionary<string, string>();
				body["squad_id"] = Player.GetSquadId();

				StartCoroutine(Postman.GetSquadInfo(body, SceneLoader.LoadSquad));
			}
		}

	}
	public void ItemsBtnClick(){
	}
	public void GuildBtnClick(){
	}

	public void OnHeroBtnClick(){
		if (SceneManager.GetActiveScene().name != "Team") {
			StartCoroutine(Postman.GetTeam(SceneLoader.LoadTeam));
		}
	}

	public void OnShopBtnClick(){
		Debug.Log("On Shop Btn click");

		if (SceneManager.GetActiveScene().name != "Shop") {
			StartCoroutine(Postman.GetTeam(SceneLoader.LoadShop));
		}
	}

	public void OnHomeBtnClick(){	
		if (SceneManager.GetActiveScene().name != "Home") {
			StartCoroutine(Postman.GetHome(SceneLoader.LoadHome));
		}
		
	}

	public void OnProfileBtnClick(){
		if (SceneManager.GetActiveScene().name != "Profile") {

			Dictionary<string, string> body = new Dictionary<string, string>();
			body["player_id"] = Player.GetID();

			StartCoroutine(Postman.GetProfile(body, SceneLoader.LoadProfile));
		}

	}

	public void OnChatBtnClick() {
		ChatManager.gameObject.SetActive(true);
	}

	public void OnSummonBtnClick() {
		if (SceneManager.GetActiveScene().name != "Gateway") {
			SceneLoader.LoadGateway();
		}
	}

	//Connects all the buttons for the bottem nav
	public void SetupBotNavBar(){

		BotBar.transform.Find("HomeBtn").GetComponent<Button>().onClick.AddListener(delegate {
			OnHomeBtnClick();
		});

		BotBar.transform.Find("HeroBtn").GetComponent<Button>().onClick.AddListener(delegate {
			OnHeroBtnClick();
		});

		BotBar.transform.Find("ShopBtn").GetComponent<Button>().onClick.AddListener(delegate {
			OnShopBtnClick();	
		});

		BotBar.transform.Find("SquadBtn").GetComponent<Button>().onClick.AddListener(delegate {
			OnSquadBtnClick();
		});

		BotBar.transform.Find("ProfileBtn").GetComponent<Button>().onClick.AddListener(delegate {
			OnProfileBtnClick();
		});

		BotBar.transform.Find("SummonBtn").GetComponent<Button>().onClick
			.AddListener(()=>OnSummonBtnClick());

		BotBar.transform.Find("ChatBtn").GetComponent<Button>().onClick
			.AddListener(()=>OnChatBtnClick());
	}

	public void SetActiveBotNav(string sceneName){

		string path = ""; 

		if (sceneName == "Home") {
			path = "HomeBtn/Highlight";
		} else if (sceneName == "Team") {
			path = "HeroBtn/Highlight";
		} else if (sceneName == "Squad") {
			path = "SquadBtn/Highlight";
		}

		if (path != string.Empty) {
			GameObject highLight = BotBar.transform.Find(path).gameObject;
			highLight.SetActive(true);
		}
	}

	public void UpdateTopBar(){


		//Attempt to find the top bar
		if (TopBar) {
			float energy 		= Player.GetFloatValue("energy");
			float energyMax 	= Player.GetFloatValue("energyMax");
			string name 		= Player.GetName();
			float coin 			= Player.GetFloatValue("coin");
			float gems 			= Player.GetFloatValue("gems");
			float currentLevel 	= Player.GetFloatValue("level");
			float xp			= Player.GetFloatValue("experience");
			float xpToNextLevel = Player.GetFloatValue("experienceToNextLevel");

			TopBar.transform.Find("Energy/EnergyTxt").GetComponent<Text>().text = energy.ToString() + " / " + energyMax.ToString();
			float test =  (float)energy / (float)energyMax;
			TopBar.transform.Find("Energy/EnergyBar").GetComponent<Image>().fillAmount = test;

			TopBar.transform.Find("PlayerNameTxt").GetComponent<Text>().text = name;

			TopBar.transform.Find("GoldTxt").GetComponent<Text>().text = coin.ToString();
			TopBar.transform.Find("GemTxt").GetComponent<Text>().text = gems.ToString();

			TopBar.transform.Find("Level/PlayerLvlTxt").GetComponent<Text>().text = currentLevel.ToString();
			TopBar.transform.Find("Level/LevelBar").GetComponent<Image>().fillAmount = 
				(float)xp / (float)xpToNextLevel;


			//topBar.transform.FindChild("XpTxt").GetComponent<Text>().text = stamina.ToString() + "/" + staminaMax.ToString();
		}
	}

	public void updateLastChat(string lastChat){
		BotBar.transform.Find("Chat/LastChatTxt").GetComponent<Text>().text = lastChat;
	}

	public void SetBarVisibility(NavBarState newState){

		currentBarState = newState;

		switch (newState) {
			case NavBarState.BothHidden:
				TopBar.SetActive(false);
				BotBar.SetActive(false);
				break;
			case NavBarState.BothVisble:
				TopBar.SetActive(true);
				BotBar.SetActive(true);
				break;
			case NavBarState.TopOnly:
				TopBar.SetActive(true);
				BotBar.SetActive(false);
				break;
			case NavBarState.BottomOnly:
				TopBar.SetActive(false);
				BotBar.SetActive(true);
				break;
			default:
				break;
		}
	}

}