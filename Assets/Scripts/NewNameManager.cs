﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class NewNameManager : MonoBehaviour {
	 
	Authentication auth;

	// Use this for initialization
	void Start () {

		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

		if (!auth) {
			auth = GameObject.FindGameObjectWithTag("Authentication").GetComponent<Authentication>();
		}
	}
	

	public void OnConfirmBtnClick(){

		GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");

		InputField NameInputField = canvas.GetComponentInChildren<InputField>();
		auth.ConfirmNamePressed(NameInputField.text);
	}
}
