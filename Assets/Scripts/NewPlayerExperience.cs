﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;

public static class NewPlayerExperienceManager {

	static Dictionary<string, string> EventText;

	// Use this for initialization
	public static void init() {

		//Todo: load this from config
		Dictionary<string, fsData> newplayerJson = ClientCache.GetConfig("newplayer.json");
		if (newplayerJson != null) {
			EventText = JsonSerializer.ParseFSData(newplayerJson);
		}

	}

	public static bool FireEvent(string eventKey){

		if (EventText == null) {
			init();
		}

		if (EventText != null && EventText.ContainsKey(eventKey) &&
			(!PlayerPrefs.HasKey(eventKey) || PlayerPrefs.GetInt(eventKey) == 0)) {

			GameObject storyPopup = PopupManager.OpenPopup("DialogPopup", eventKey);
			PopupManager.SetTextForSimplePopup(storyPopup, EventText[eventKey]);
			PlayerPrefs.SetInt(eventKey, 1);
			return true;
		} 

		return false;
	}

	public static bool HasEventBeenTriggered(string eventKey){
		if (PlayerPrefs.HasKey(eventKey) && PlayerPrefs.GetInt(eventKey) == 1) {
			return true;
		}

		return false;
	}

	public static void ResetEvents(){
		foreach(KeyValuePair<string, string> entry in EventText){
			PlayerPrefs.SetInt(entry.Key, 0);
		}

		PlayerPrefs.Save();
	}
	
}
