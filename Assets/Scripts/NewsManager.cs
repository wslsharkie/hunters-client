﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewsManager : MonoBehaviour {

	string Url = "http://www.wyldhunters.com/punBB/index.php";
	public GUIText status;
	WebViewObject webViewObject;
	float topMargin = 0.115f;
	float botMargin = 0.1475f;


	IEnumerator Start()
	{
		webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
		webViewObject.Init(
			cb: (msg) =>
			{
				Debug.Log(string.Format("CallFromJS[{0}]", msg));
				status.text = msg;
				status.GetComponent<Animation>().Play();
			},
			err: (msg) =>
			{
				Debug.Log(string.Format("CallOnError[{0}]", msg));
				status.text = msg;
				status.GetComponent<Animation>().Play();
			},
			ld: (msg) =>
			{
				Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
				#if !UNITY_ANDROID
				webViewObject.EvaluateJS(@"
                  window.Unity = {
                    call: function(msg) {
                      window.location = 'unity:' + msg;
                    }
                  }
                ");
				#endif
			},
			enableWKWebView: true);
		webViewObject.SetMargins(0, (int)(Screen.height * topMargin), 0, (int)(Screen.height * botMargin));
		webViewObject.SetVisibility(true);

		if (Url.StartsWith("http")) {
			webViewObject.LoadURL(Url.Replace(" ", "%20"));
		} else {
			var exts = new string[]{
				".jpg",
				".html"  // should be last
			};
			foreach (var ext in exts) {
				var url = Url.Replace(".html", ext);
				var src = System.IO.Path.Combine(Application.streamingAssetsPath, url);
				var dst = System.IO.Path.Combine(Application.persistentDataPath, url);
				byte[] result = null;
				if (src.Contains("://")) {  // for Android
					var www = new WWW(src);
					yield return www;
					result = www.bytes;
				} else {
					result = System.IO.File.ReadAllBytes(src);
				}
				System.IO.File.WriteAllBytes(dst, result);
				if (ext == ".html") {
					webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
					break;
				}
			}
		}
		yield break;
	}

}
	
