﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PVPListManager : MonoBehaviour {

	Canvas canvas;

	// Use this for initialization
	void Start () {

		Debug.Log("We're here in start of pvp list manager");

		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

		if (!canvas) {
			canvas = GameObject.FindWithTag("Canvas").GetComponent<Canvas>();
		}



		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);
		/*foreach (fsData entry in Postman.RESPONSE) {
			Debug.Log(entry);
		}*/

		SetupList(Postman.RESPONSE);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GenerateList(){

		Debug.Log("Generating pvp list");
		StartCoroutine(Postman.GeneratePVPList(GenerateListResponse));
	}

	void GenerateListResponse(){

		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

	}

	void SetupList(Dictionary<string, fsData> response){
	
		Dictionary<string, fsData> pvpList = response["list"].AsDictionary;
		float plankHeight = 350;
		float yOffset = 412;

		GameObject plankPrefab = (GameObject)Resources.Load("PVPListPrefabs/PVPPlank");

		foreach (KeyValuePair<string, fsData> entry in pvpList) {
			GameObject plank = Instantiate(plankPrefab, canvas.transform, false);
			string playerID = entry.Key;
			Dictionary<string, fsData> playerFields = entry.Value.AsDictionary;

			plank.transform.Find("PlayerNameTxt").GetComponent<Text>().text = playerFields["name"].AsString;
			plank.transform.Find("LvlTxt").GetComponent<Text>().text = "LVL " + playerFields["level"].AsInt64.ToString();
			plank.transform.Find("RankTxt").GetComponent<Text>().text = "Score " + playerFields["score"].AsString;

			plank.transform.localPosition = new Vector2(0, yOffset);
			yOffset -= plankHeight;


			//Create hero objects from their team data
			int i = 0;
			Dictionary<string, fsData> team = playerFields["team"].AsDictionary;
			foreach (KeyValuePair<string, fsData> heroData in team) {
				Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
				hero.Init(heroData.Value.AsDictionary);

				char[] charsToTrim = {'"'};
				string portaitImage =  hero.GetPortraitPath().Trim(charsToTrim);

				Debug.Log(portaitImage);

				Texture2D texture = ClientCache.GetTexture(portaitImage);
				Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
				plank.transform.Find("Hero" + i.ToString() + "/Image").GetComponent<Image>().sprite = newSprite;
				i++;
			}

			plank.GetComponent<Button>().onClick.AddListener(delegate {
				OnPVPPlankClick(playerID);
			});

				

		}
	}


	void OnPVPPlankClick(string playerID){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = playerID;

		StartCoroutine(Postman.CreatePVPBattle(body, SceneLoader.LoadPVP));
	}
}
