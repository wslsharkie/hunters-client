﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PVPManager : MonoBehaviour {

	public Canvas canvas;
	public Dictionary<string, Hero> playerHeroes;
	public Dictionary<string, Hero> enemyHeroes;

	NavBarManager navBars;
	List<Rect> clickableAreaWorld;
	string currentTargetID;


	void Start () {

		if (!canvas) {
			canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		}

		if (!navBars) {
			navBars = GameObject.Find("NavBarManager").GetComponent<NavBarManager>();

		}

		navBars.SetBarVisibility(NavBarManager.NavBarState.BothHidden);

		Debug.Log("PVP manager");
		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		playerHeroes = new Dictionary<string, Hero>();
		enemyHeroes = new Dictionary<string, Hero>();

		//Parse out

		SetupPlayerTeam();
		SetupEnemyTeam();
	}


	void Update(){
	
		if (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)) {
			//Debug.Log(Input.mousePosition);

			Touch firstTouch;
			if (Application.isEditor) {
				//Workaround for editor
				firstTouch = new Touch();
				firstTouch.position = Input.mousePosition;
				if (Input.GetMouseButtonDown(0)) {
					firstTouch.phase = TouchPhase.Began;
				} else if (Input.GetMouseButtonUp(0)) {
					firstTouch.phase = TouchPhase.Ended;
				} else {
					firstTouch.phase = TouchPhase.Moved;
				}

			} else {
				firstTouch = Input.touches[0];
			}


			if (IsWithinClickableArea(firstTouch)) {


				switch (firstTouch.phase) {
					case TouchPhase.Ended:
						AttackTarget();
						break;
				}
			}

		}
	}


	bool IsWithinClickableArea(Touch firstTouch){

		if (clickableAreaWorld.Count < 1){
			GameObject[] clickableAreas = GameObject.FindGameObjectsWithTag("ClickableArea");

			foreach (GameObject clickableArea in clickableAreas) {
				RectTransform clickableRectTransform = clickableArea.GetComponent<RectTransform>();

				Vector3[] corners = new Vector3[4];
				clickableRectTransform.GetWorldCorners(corners);
				Vector3 topLeft = corners[0];

				// Rescale the size appropriately if we change the canvas scale
				Vector2 size = new Vector2(clickableRectTransform.rect.size.x,
					clickableRectTransform.rect.size.y);

				clickableAreaWorld.Add(new Rect(topLeft, size));
			}
		}

		foreach (Rect clickableArea in clickableAreaWorld) {
			if (clickableArea.Contains(firstTouch.position)) {
				return true;
			}
		}

		return false;

	}

	void SetupPlayerTeam(){

		//iterate through the heroes

		//for  
	}

	void SetupEnemyTeam(){
	}

	void AttackTarget(){
	}



}
