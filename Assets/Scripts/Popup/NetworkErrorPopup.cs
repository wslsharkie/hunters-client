﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkErrorPopup : MonoBehaviour {

	const string PopupName = "NetworkErrorPopup";

	public static void OpenPopup() {
		PopupManager.OpenPopup(PopupName);
	}

	public void OnRetryClick() {
		GameManager.ReloadGame();
	}
}
