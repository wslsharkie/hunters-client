﻿using UnityEngine;
using System.Collections;

public class PopupActions : MonoBehaviour {

	//This needs to be the name of the popup prefab
	public void onCloseBtnClick(){
		PopupManager.ClosePopup(this.gameObject.name);
	}

	public void onCloseBtnClick(string name){
		PopupManager.ClosePopup(this.gameObject.name);
	}

	public void ClosePopup(){
		PopupManager.ClosePopup(this.gameObject.name);
	}
}
