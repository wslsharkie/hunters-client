﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour {

	static Dictionary<string, GameObject> popupPrefabs;
	static Dictionary<string, GameObject> popupCache;
	static Stack<GameObject> popupStack;
	static Canvas originalCanvas;

	static public bool PopupActive;
	public static PopupManager Instance;

	void Awake(){
		//Check if instance already exists
		if(Instance == null) {
			Instance = this;
			DontDestroyOnLoad(gameObject);
		} else if(Instance != this) {
			Destroy(gameObject);    
		}
	}

	// Use this for initialization
	void Start () {

		popupPrefabs = new Dictionary<string, GameObject>();
		popupCache = new Dictionary<string, GameObject>();
		popupStack = new Stack<GameObject>();
		PopupActive = false;

		//Clear the popup flag and cache
		SceneManager.sceneLoaded += delegate{
			PopupActive = false;

			if (popupCache != null){
				popupCache.Clear();
			}

			if(GameManager.LOADING) {
				PopupManager.OpenPopup("LoadingPopup");
				StartCoroutine(PopupManager.CloseLoadingPopup());
			}
		};
	}

	public static GameObject OpenPopup(string popupPrefabName) {
		
		string popupName = popupPrefabName;

		return OpenPopup(popupName, popupPrefabName);
	}
		
	static public GameObject OpenPopup(string popupPrefabName, string popupName){
		Canvas canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();

		if(!canvas) {
			canvas = FindObjectOfType<Canvas>();
		}

		if(!canvas) {
			Debug.LogError(popupPrefabName + " must have a canvas!");
			return null;
		}

		return OpenPopup(popupName, popupPrefabName, canvas);
	}

	static public GameObject OpenPopup(string popupName, string prefabName, Canvas currentCanvas){
		//Get the popup object
		GameObject newPopup;
		originalCanvas = currentCanvas;

		//EnableDisableAllPriorButtons(false, originalCanvas);

		//Grab the popup from the cache if it already created
		if (popupCache.ContainsKey(popupName)) {
			newPopup = popupCache[popupName];
		//
		} else if (popupPrefabs.ContainsKey(prefabName)) {
			newPopup = (GameObject)Instantiate(popupPrefabs[prefabName], currentCanvas.transform, false);
			newPopup.name = popupName; //need the name so we can close it later
			popupCache[popupName] = newPopup;
			newPopup.SetActive(false);
		} else {
			GameObject newPrefab = (GameObject)Resources.Load("Popups/" + prefabName);
			popupPrefabs[prefabName] = newPrefab; 
			newPopup = (GameObject)Instantiate(popupPrefabs[prefabName], currentCanvas.transform, false);
			newPopup.name = popupName;
			popupCache[popupName] = newPopup;
			newPopup.SetActive(false);
		}
			
		//If popup is already up, add the new popup to the queue
		if (PopupActive) {
			popupStack.Push(newPopup);
		} else {
			newPopup.SetActive(true);
		}
			

		PopupActive = true;

		return newPopup;
	}

	static public void ClosePopup(string popupName){

		//Instead of destorying the popup. We deactive it to save it for later
		GameObject popup = popupCache[popupName];
		popup.SetActive(false);

		//if there is another popup queued up, return to it
		if (popupStack.Count > 0) {
			GameObject oldPopup = popupStack.Pop ();
			oldPopup.SetActive (true);
		} else {
			EnableDisableAllPriorButtons(true, originalCanvas);
			originalCanvas = null;
			PopupActive = false;
		}

	}
		
	static public void EnableDisableAllPriorButtons(bool enabled, Canvas canvas) {
		Button[] buttons = canvas.GetComponentsInChildren<Button>();

		for (int buttonCount = 0; buttonCount < buttons.Length; buttonCount++) {
			Button btn = buttons[buttonCount];
			btn.enabled = enabled;
		}

		Toggle[] toggles = canvas.GetComponentsInChildren<Toggle>();

		for (int toggleCount = 0; toggleCount < toggles.Length; toggleCount++) {
			Toggle tgl = toggles[toggleCount];
			tgl.enabled = enabled;
		}

//		Debug.Log ("processed " + buttons.Length.ToString() + " number of buttons");
//		Debug.Log ("enabled = " + enabled);
//
//		Debug.Log ("processed " + toggles.Length.ToString() + " number of toggles");
//		Debug.Log ("enabled = " + enabled);

	}

	//Sets text for popups that only have a single text field
	static public void SetTextForSimplePopup(GameObject popup, string newText){

		Text childText = popup.GetComponentInChildren<Text>();
		/*for (int i = 0; i < popup.transform.childCount; i++) {
			childText = popup.transform.GetChild(i).gameObject.GetComponent<Text>();
			if (childText != null) {
				break;
			}
		}*/

		if (childText != null) {
			childText.text = newText;
		}
	}

	public static IEnumerator CloseLoadingPopup() {
		while(GameManager.LOADING) {
			yield return null;
		}

		if(PopupActive) {
			PopupManager.ClosePopup("LoadingPopup");
		}
	}
}
