﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerErrorPopup : MonoBehaviour {

	const string PopupName = "ServerErrorPopup";
	Text ErrorMessage;

	void Awake() {
		ErrorMessage = gameObject.GetComponentInChildren<Text>();
	}

	// Use this for initialization
	void Start () {
		string serverError = Postman.RESPONSE["error"].AsString;
		Debug.Log("Server Error - "+serverError);
		ErrorMessage.text = serverError;
	}

	// Update is called once per frame
	void Update () {

	}

	public static void OpenPopup() {
		PopupManager.OpenPopup(PopupName);
	}

	public void OnCloseClick() {
		PopupManager.ClosePopup(PopupName);
		//SceneLoader.LoadHome();
	}
}
