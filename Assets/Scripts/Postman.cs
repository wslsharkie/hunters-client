﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using FullSerializer;

public class Postman {

	public static Dictionary<string, fsData> RESPONSE = null;

	/*** API CALLS ***/
	public static IEnumerator GetHome(Action action) {
		string url = GameConfig.SERVER_URL+"home";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	public static IEnumerator GetStages(Action action) {
		string url = GameConfig.SERVER_URL+"stages";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	public static IEnumerator GetInbox(Action action) {
		string url = GameConfig.SERVER_URL+"achievements";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	#region Inbox Scene calls for achievements

	public static IEnumerator ClaimAchievement(Dictionary<string, string> body, Action action) {
		string url = GameConfig.SERVER_URL +"achievements/collect/";
		WWWForm form = new WWWForm();
		form.AddField("ach_id", body["ach_id"]);
		return Postman.ProcessWWW(Postman.SendPost(url, form), action);
	}

	#endregion

	#region Hero Page Calls
		
	public static IEnumerator GetTeam(Action action) {
		string url = GameConfig.SERVER_URL+"teams";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	public static IEnumerator SaveTeam(Dictionary<string, string> body, Action action){
		string url = GameConfig.SERVER_URL+"teams/set/";

		WWWForm form = new WWWForm();
		form.AddField("teams", body["teams"]);
		form.AddField("active", body["active"]);
		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator CreateSquad(Dictionary<string, string> body, Action action) {
		string url = GameConfig.SERVER_URL+"squad/create/";

		WWWForm form = new WWWForm();
		form.AddField("name", body["name"]);

		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator GetEquipmentForHero(string heroID, Action action){
		string url = GameConfig.SERVER_URL+"equipment/"+heroID;
		return Postman.ProcessWWW(SendGet(url), action);

	}

	public static IEnumerator ResetEquipment(Dictionary<string, string> body, Action action){

		string URL = GameConfig.SERVER_URL+"equipment/reset/";
		WWWForm form = new WWWForm();
		form.AddField("hero_id", body["hero_id"]);
		return Postman.ProcessWWW(Postman.SendPost(URL, form), action);

	}

	public static IEnumerator CraftEquipment(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"equipment/craft/";

		WWWForm form = new WWWForm();
		form.AddField("hero_id", body["HeroID"]);
		form.AddField("equipment_id", body["EquipmentID"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator PromoteHunter(Dictionary<string, string> body, Action action){

		string heroID = body["hero"];

		string url = GameConfig.SERVER_URL+"heroes/stars/" + heroID;

		WWWForm form = new WWWForm();
		form.AddField("hero_id", body["hero"]);

		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator CreateHunter(Dictionary<string, string> body, Action action){
	
		string heroID = body["hero"];
		string url = GameConfig.SERVER_URL+"heroes/summon/" + heroID;

		WWWForm form = new WWWForm();
		form.AddField("hero_id", body["hero"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}
		
	#endregion

	#region Battle Page Calls

	public static IEnumerator CreateBattle(Dictionary<string, string> body, Action action) {
		// begin request for client id and secret
		string url = GameConfig.SERVER_URL+"stages/" + body["stage_id"] + "/battles/" + body["battle_id"];

		//do we really need to pass the form?
		//Answer:Yes we do, as there's an error
		WWWForm form = new WWWForm();
		form.AddField("stage_id", body["stage_id"]);
		form.AddField("battle_id", body["battle_id"]);
		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator Attack(Dictionary<string, string> body, Action action){

		//battleRouter.route('/:object_id/attack/?')
		string url = GameConfig.SERVER_URL+"battles/"+ body["battle_id"]+"/attack/";


		WWWForm form = new WWWForm();
		form.AddField("battle_id", body["battle_id"]);
		form.AddField("position", body["target_id"]);
		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator SpecialAttack(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"battles/"+ body["battle_id"]+"/special/";

		WWWForm form = new WWWForm();
		form.AddField("hero_id", body["hero_id"]);
		form.AddField("position", body["target_id"]);
		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator AbandonBattle(Dictionary<string, string> body, Action action){
		string url = GameConfig.SERVER_URL+"battles/"+ body["battle_id"] +"/quit";
		WWWForm form = new WWWForm();
		form.AddField("battle_id", body["battle_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator GetRewardList(Dictionary<string, string> body, Action action){
		string url = GameConfig.SERVER_URL+"battles/rewards/";
		WWWForm form = new WWWForm();
		form.AddField("battle_id", body["battle_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator ReviveTeam(Dictionary<string, string> body, Action action){
		string url = GameConfig.SERVER_URL+"battles/recharge/";
		WWWForm form = new WWWForm();
		form.AddField("battle_id", body["battle_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);
	}


	#endregion

	#region Raid List calls

	public static IEnumerator GetRaids(Action action){
	//	Debug.Log("Doing get raids");
		string url = GameConfig.SERVER_URL+"raidlist/";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	public static IEnumerator GeneratePlayerList(Action action){
		string url = GameConfig.SERVER_URL+"playerlist/test/generate";
		WWWForm form = new WWWForm();
		form.AddField("empty","value");


		return Postman.ProcessWWW(SendPost(url,form), action);
	}

	public static IEnumerator GenerateRaidList(Action action){
		WWWForm form = new WWWForm();
		form.AddField("empty","value");

		string url = GameConfig.SERVER_URL+"raidlist/test/generate";
		return Postman.ProcessWWW(SendPost(url,form), action);
	}

	public static IEnumerator JoinBattle(Dictionary<string, string> body, Action action){
		WWWForm form = new WWWForm();
		form.AddField("battle_id", body["battle_id"]);

		string url = GameConfig.SERVER_URL + "battles/"+ body["battle_id"] + "/join";
		Debug.Log("Joining battle with url " + url);

		return Postman.ProcessWWW(SendPost(url,form), action);

	}

	public static IEnumerator ClearRaidList(Action action){
		string url = GameConfig.SERVER_URL+"battles/list/delete";
		WWWForm form = new WWWForm();
		form.AddField("empty","value");


		return Postman.ProcessWWW(SendPost(url,form), action);

	}


	#endregion

	#region Squad Page Calls

	public static IEnumerator GetSquads(Action action){
		
		string url = GameConfig.SERVER_URL+"squads/list";
		return Postman.ProcessWWW(SendGet(url), action);
	}

	public static IEnumerator GenerateSquads(Dictionary<string, string> body, Action action){
		WWWForm form = new WWWForm();
		form.AddField("empty","value");

		string url = GameConfig.SERVER_URL + "squads/generate/";

		return Postman.ProcessWWW(SendPost(url,form), action);

	}

	public static IEnumerator GetSquadInfo(Dictionary<string, string> body, Action action){

		string squadID = body["squad_id"];
		string url = GameConfig.SERVER_URL + "squads";

		WWWForm form = new WWWForm();
		form.AddField("squad_id", squadID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator JoinSquad(Dictionary<string, string> body, Action action){

		string squadID = body["squad_id"];
		string url = GameConfig.SERVER_URL + "squads/" + squadID + "/join";


		WWWForm form = new WWWForm();
		form.AddField("squad_id", squadID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator LeaveSquad(Dictionary<string, string> body, Action action){

		string squadID = body["squad_id"];
		string url = GameConfig.SERVER_URL + "squads/leave";


		WWWForm form = new WWWForm();
		form.AddField("squad_id", squadID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator KickPlayer(Dictionary<string, string> body, Action action){

		string playerID = body["player_id"];
		string url = GameConfig.SERVER_URL + "squads/kick";
		//58c6fce74fb40b4c335d698f
		WWWForm form = new WWWForm();
		form.AddField("target_id", playerID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator PromotePlayer(Dictionary<string, string> body, Action action){

		string playerID = body["player_id"];
		string url = GameConfig.SERVER_URL + "squads/promote";

		WWWForm form = new WWWForm();
		form.AddField("target_id", playerID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator DemotePlayer(Dictionary<string, string> body, Action action){

		string playerID = body["player_id"];
		string url = GameConfig.SERVER_URL + "squads/demote";

		WWWForm form = new WWWForm();
		form.AddField("target_id", playerID);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}



	#endregion

	#region PVP
	public static IEnumerator GetPVPList(Action action){

		string url = GameConfig.SERVER_URL+"arena/list";
		return Postman.ProcessWWW(SendGet(url), action);
	}


	public static IEnumerator GeneratePVPList(Action action){

		string url = GameConfig.SERVER_URL+"arena/test/rank";

		WWWForm form = new WWWForm();
		form.AddField("test", "test");

		return Postman.ProcessWWW(SendPost(url, form), action);

	}


	public static IEnumerator CreatePVPBattle(Dictionary<string, string> body, Action action){
		
		string url = GameConfig.SERVER_URL+"arena/create";

		Debug.Log("Trying to crate a battle with player " + body["player_id"]);
		WWWForm form = new WWWForm();
		form.AddField("player_id", body["player_id"]);
		return Postman.ProcessWWW(SendPost(url, form), action);

	}
		

	#endregion

	#region Profile page calls
	public static IEnumerator GetProfile(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"players/profile";
		WWWForm form = new WWWForm();
		form.AddField("player_id", body["player_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	public static IEnumerator AddAlly(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"allies/test";
		WWWForm form = new WWWForm();
		form.AddField("player_id", body["player_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator AddAllyTestFunction(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"allies/test";
		WWWForm form = new WWWForm();
		form.AddField("player_id", body["player_id"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}
		
	#endregion

	#region Account Binding Calls

	public static IEnumerator RequestCode(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"clients/code";
		WWWForm form = new WWWForm();
		form.AddField("password", body["password"]);
		form.AddField("client_id", body["client_id"]);
		form.AddField("client_secret", body["client_secret"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	public static IEnumerator RecoverAccountClick(Dictionary<string, string> body, Action action){

		string url = GameConfig.SERVER_URL+"clients/recover";
		WWWForm form = new WWWForm();
		form.AddField("password", body["password"]);
		form.AddField("code", body["code"]);

		return Postman.ProcessWWW(SendPost(url, form), action);

	}

	#endregion

	#region Gateway Summon Calls

	public static IEnumerator SummonHero(Dictionary<string, string> body, Action action){
		string url = GameConfig.SERVER_URL+"gateways/summon/"+body["gateway_id"];
		WWWForm form = new WWWForm();
		form.AddField("player_id", Player.GetID());
		return Postman.ProcessWWW(SendPost(url, form), action);
	}

	#endregion

	/*** MAIN ***/
	public static WWW SendPost(string url, WWWForm form) {
		GameManager.LOADING = true;
		string accessToken = GameConfig.GetAccessToken();

		Dictionary<string, string> headers = form.headers;
		headers.Add("Authorization", "Bearer "+accessToken);

		return new WWW(url, form.data, headers);
	}

	public static WWW SendGet(string url) {
		GameManager.LOADING = true;
		string accessToken = GameConfig.GetAccessToken();

		WWWForm form = new WWWForm();
		Dictionary<string, string> headers = form.headers;
		headers.Add("Authorization", "Bearer " + accessToken);

		//Debug.Log("Token "+accessToken);
		
		return new WWW(url, null, headers);
	}

	public static bool ValidateReponse(WWW www) {
		if (!string.IsNullOrEmpty(www.error) || www.text.Equals("")) {
			
			if(www.text.Equals("Unauthorized")) {
				GameConfig.DeleteToken();
			}
				
			Debug.Log(www.error);
			return false;
		}

		return true;
	}

	public static IEnumerator ProcessWWW(WWW www, Action action) {
		yield return www;

		if(Postman.ValidateReponse(www) == false) {
			NetworkErrorPopup.OpenPopup();
			yield break;
		}
								
		Postman.RESPONSE = JsonSerializer.FromJSon(www.text);

		if(Postman.RESPONSE.ContainsKey("error")) {
			GameObject popup = PopupManager.OpenPopup("ServerErrorPopup", Postman.RESPONSE["error"].AsString);
			PopupManager.SetTextForSimplePopup (popup, Postman.RESPONSE ["error"].AsString);
			yield break;
		}

		if (Postman.RESPONSE.ContainsKey("NewPlayerExperience")){
			NewPlayerExperienceManager.FireEvent(Postman.RESPONSE["NewPlayerExperience"].AsString);
		}

		ClientCache.LoadResources(action);
	}
}
