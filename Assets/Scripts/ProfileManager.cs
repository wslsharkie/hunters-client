﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FullSerializer;
using UnityEngine.UI;


public class ProfileManager : MonoBehaviour {

	Canvas profileCanvas;
	string playerID;

	// Use this for initialization
	void Start () {
		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

		if (!profileCanvas) {
			profileCanvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		}

		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		SetupProfile(Postman.RESPONSE);

	}
	
	void SetupProfile (Dictionary<string, fsData> response) {


		Dictionary<string, fsData> player = response["player"].AsDictionary;
		Dictionary<string, fsData> arena = response["arena"].AsDictionary;
		Dictionary<string, fsData> squad;
		if (!response["squad"].IsNull) {
			squad = response["squad"].AsDictionary;
		} else {
			squad = new Dictionary<string, fsData>();
		}
		Dictionary<string, fsData> team = response["team"].AsDictionary;
		Dictionary<string, fsData> allies = response["allies"].AsDictionary;

		Transform scrollContent = profileCanvas.transform.Find("Scroll View/Viewport/Content").transform;
		playerID = player["_id"].AsString;

		//Setup the team
		int i = 0;
		foreach (KeyValuePair<string, fsData> entry in team) {
			Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
			hero.Init(entry.Value.AsDictionary);

			//Set frame
			string frameName = hero.GetFrameName();
			Texture2D frameTexture = (Texture2D)Resources.Load("Images/Battle/Hero_frames/" + frameName);
			Sprite frame = Sprite.Create(frameTexture, new Rect(0, 0, frameTexture.width, frameTexture.height), new Vector2(0.5f, 0.5f));
			scrollContent.Find("Team/Hero" + i.ToString() + "/frame").GetComponent<Image>().sprite = frame;

			Image frameImage = scrollContent.Find("Team/Hero" + i.ToString() + "/frame").GetComponent<Image>();

			//Set stars
			Texture2D starsTexture = (Texture2D)Resources.Load("Images/Hero/rarity stars/" + hero.GetStars() + "_stars");
			Sprite stars = Sprite.Create(starsTexture, new Rect(0, 0, starsTexture.width, starsTexture.height), new Vector2(0.5f, 0.5f));
			scrollContent.Find("Team/Hero" + i.ToString() + "/stars").GetComponent<Image>().sprite = stars;

			//Portait
			string portaitPath = hero.GetPortraitPath();
			Texture2D portaitTexture = ClientCache.GetTexture(portaitPath);
			if (portaitTexture != null) {
				Sprite newPortait = Sprite.Create(portaitTexture, new Rect(0, 0, portaitTexture.width, portaitTexture.height), new Vector2(0.5f, 0.5f));
				scrollContent.Find("Team/Hero" + i.ToString() + "/Image").GetComponent<Image>().sprite = newPortait;
			}

			i++;
		}

		//Hide any remaining heroes if they don't have a full team
		for (; i < 4; i++){
			scrollContent.Find("Team/Hero" + i.ToString()).gameObject.SetActive(false);

		}
			
		//Setup the player info
		//profileCanvas.transform.Find("PlayerName/Text").GetComponent<Text>().text = player["name"].AsString;
		scrollContent.Find("PlayerName/Text").GetComponent<Text>().text = Player.GetName();
		scrollContent.Find("PlayerLevel/Text").GetComponent<Text>().text = "LEVEL " + player["level"].AsInt64.ToString();
		scrollContent.Find("PVPRank/Text").GetComponent<Text>().text = "RANK:  " + arena["score"].AsInt64.ToString();

		//If squad is null, player isn't part of a squad
		if (squad.Count == 0) {
			scrollContent.Find("Guild/GuildNameTxt").GetComponent<Text>().text = "NO SQUAD";
			scrollContent.Find("Guild/RankTxt").GetComponent<Text>().text = string.Empty;
			scrollContent.Find("Guild/MembersTxt").GetComponent<Text>().text = string.Empty;
		} else {
			string squadID = "insert squad id here";
			scrollContent.Find("Guild/Image").GetComponent<Button>().onClick.AddListener(delegate {
				OnSquadClick(squadID);
			});
		}

		if (playerID == Player.GetID()) {
			//Hide add ally btn
		}

		//Setup Allies
		List<fsData> allyList = allies["allies"].AsList;

		GameObject allyPrefab = (GameObject)Resources.Load("ProfilePrefabs/AllyPlank");
		foreach (fsData entry in allyList) {
			GameObject allyPlank = Instantiate(allyPrefab, scrollContent.Find("AlliesHolder"), false);
			//Setup ally plank

			Dictionary<string, fsData> allyData = entry.AsDictionary;
			string name = allyData["name"].AsString;
			string level = "Level " + allyData["level"].AsInt64.ToString();
			string squadName = allyData["squadName"].ToString();

			allyPlank.transform.Find("NameTxt").GetComponent<Text>().text = name;
			allyPlank.transform.Find("GuildText").GetComponent<Text>().text = squadName;
			allyPlank.transform.Find("LevelTxt").GetComponent<Text>().text = level;

			string playerID = allyData["id"].AsString;
			allyPlank.GetComponent<Button>().onClick.AddListener(delegate {
				OnAllyClick(playerID);
			});


		}

	}

	public void OnAddAllyClick(){

		Debug.Log("On ally btn click");

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = playerID;

		StartCoroutine(Postman.AddAllyTestFunction(body, AddAllyResponse));
	}

	void AddAllyResponse(){
		
		GameObject popup = PopupManager.OpenPopup("TextSmallPopup");
		PopupManager.SetTextForSimplePopup(popup, "Added Ally!");
	}

	public void OnSquadClick(string squadID){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = squadID;

		StartCoroutine(Postman.GetSquadInfo(body, SceneLoader.LoadSquad));
	}

	void OnAllyClick(string playerID){

		Debug.Log("got on ally click");

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = playerID;

		StartCoroutine(Postman.GetProfile(body, SceneLoader.LoadProfile));

	}

}
