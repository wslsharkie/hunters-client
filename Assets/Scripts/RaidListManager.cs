﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RaidListManager : MonoBehaviour {

	public Canvas Canvas;
	public ScrollingList ScrollingList;
	public GameObject RaidButtonPrefab;
	public Text NoRaidsTxt;

	#region Unity Functions

	void Awake() {
		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		ScrollingList = GameObject.FindGameObjectWithTag("ScrollingList").GetComponent<ScrollingList>();
		RaidButtonPrefab = (GameObject)Resources.Load("RaidListPrefabs/RaidListItemButton");

		if (!NoRaidsTxt)
			NoRaidsTxt = GameObject.Find("NoRaidsTxt").GetComponent<Text>();
	}

	void Start () {
		if (Application.isEditor && !GameObject.Find("Authentication")) {
			SceneManager.LoadScene("LoadGame");
		}

		Text title = GameObject.FindGameObjectWithTag("Title").GetComponentInChildren<Text>();
		title.text = "Raids";

		List<GameObject> buttonList = new List<GameObject>();

		Dictionary<string, fsData> raidEntries = Postman.RESPONSE["list"].AsDictionary;

		if (raidEntries.Count > 0) {
			NoRaidsTxt.gameObject.SetActive(false);
		}

		foreach(var raidEntry in raidEntries) {
			Dictionary<string, string> raidInstance = JsonSerializer.ParseFSData(raidEntry.Value.AsDictionary);
			
			// create stage button list item
			GameObject newButton = (GameObject)Instantiate(RaidButtonPrefab);

			Text titleText = newButton.transform.Find("List/Title").GetComponent<Text>();
			Text nameText = newButton.transform.Find("List/Name").GetComponent<Text>();
			Text levelText = newButton.transform.Find("List/Level").GetComponent<Text>();

			titleText.text = raidInstance["title"];
			levelText.text = raidInstance["desc"];
		//	nameText.text = raidInstance["name"];	
	
			newButton.GetComponent<Button>().onClick
				.AddListener(() => OnJoinRaidClick(raidInstance["_id"]));

			buttonList.Add(newButton);
		}
			

		ScrollingList.Populate(buttonList);
	}

	#endregion

	#region Button Handlers

	public void OnBackBtnClick(){
		SceneManager.LoadScene("Home");
	}

	public void OnRaidPlankClick(string id){

		//58a78f929daef451cf4a0541
		Debug.Log("click on raid with battle id = " + id);
		Dictionary<string, string> body = new Dictionary<string,string>();
		body["battle_id"] = id;

		StartCoroutine(Postman.JoinBattle(body, SceneLoader.LoadBattle));
	}

	public void OnSwitchPlayerClick(){
	}

	public void OnResetBtnClick(){
		StartCoroutine(Postman.ClearRaidList(GotServerResponse));

	}

	public void OnGeneratePlayerBtnClick(){
		StartCoroutine(Postman.GeneratePlayerList(GotServerResponse));
		
	}

	public void OnGenerateBattleBtnClick(){
		StartCoroutine(Postman.GenerateRaidList(GotServerResponse));
	}

	public void OnJoinRaidClick(string raidID){
		OnRaidPlankClick(raidID);
	}		
	#endregion


	#region Server Responses

	public void GotServerResponse(){

		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		}
	}

	#endregion
}
