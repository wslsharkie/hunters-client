﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using FullSerializer;

/// <summary>
/// Scene loader.
/// Wrapper class for SceneManager.
/// </summary>
public class SceneLoader {
	public static void LoadGame() {
		SceneManager.LoadScene("LoadGame");
	}

	public static void LoadHome() {
		SceneManager.LoadScene("Home");
	}

	public static void SquadFormed() {
		Debug.Log ("Squad formed!");
	}

	public static void LoadStages() {
		SceneManager.LoadScene("MapStages");
	}

	public static void LoadBattle() {
		SceneManager.LoadScene("Battle");
	}

	public static void LoadTeam() {
		SceneManager.LoadScene("Team");
	}

	public static void LoadInbox() {
		SceneManager.LoadScene("Inbox");
	}

	public static void LoadRaid(){
		SceneManager.LoadScene("RaidList");
	}

	public static void LoadSquad(){
		SceneManager.LoadScene("Squad");
	}

	public static void LoadPVP(){
		SceneManager.LoadScene("PVP");
	}

	public static void LoadPVPList(){
		SceneManager.LoadScene("PVPList");
	}

	public static void LoadNews(){
		SceneManager.LoadScene("News");
	}

	public static void LoadSettings(){
		SceneManager.LoadScene("Settings");
	}

	public static void LoadProfile(){
		SceneManager.LoadScene("Profile");
	}

	public static void LoadBindAccount(){
		SceneManager.LoadScene("BindAccount");
	}

	public static void LoadSoundSettings(){
		SceneManager.LoadScene("SoundSettings");
	}

	public static void LoadGateway(){
		SceneManager.LoadScene("Gateway");
	}

	public static void LoadShop(){
		SceneManager.LoadScene("Shop");
	}
}