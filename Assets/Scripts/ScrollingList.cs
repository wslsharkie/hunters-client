﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingList : MonoBehaviour {

	public GameObject Grid;

	void Awake() {
		Grid = transform.Find("List/Grid").gameObject;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Populate(List<GameObject> listItems) {
		foreach(GameObject item in listItems) {
			item.transform.SetParent(Grid.transform, false);
		}
	}

	public void Clear() {
		foreach(Transform item in Grid.transform) {
			GameObject.Destroy(item.gameObject);
		}
	}
}
