﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectorPopup : MonoBehaviour {
	const string PopupName = "SelectorPopup";

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void onEnable() {

	}

	void onDisable() {
	}

	public void Populate(Dictionary<string, Action> buttons) {
		//delete last time's data if it exists
		Transform ButtonsHolder = gameObject.transform.Find("buttonsHolder").gameObject.transform;
		for (int i = 0; i < ButtonsHolder.childCount; i++) 
		{
			Destroy(ButtonsHolder.GetChild(i).gameObject);
		}

		//fill out with new buttons
		GameObject SelectorButtonTemplate = gameObject.transform.Find("selectorPopupButton").gameObject;

		foreach (KeyValuePair<string, Action> entry in buttons) {
			GameObject instanceOfButton = Instantiate (SelectorButtonTemplate, ButtonsHolder, false);
			instanceOfButton.SetActive (true);
			instanceOfButton.transform.Find ("buttonText").GetComponent<Text> ().text = entry.Key;

			instanceOfButton.GetComponent<Button> ().onClick.AddListener (delegate {
				entry.Value.Invoke();
			});

			//addListener w/ executor, callback, and params
		}


		return;
	}

	public static void OpenPopup() {
		PopupManager.OpenPopup(PopupName);
	}

	public void OnCloseClick() {
		PopupManager.ClosePopup(PopupName);
		//SceneLoader.LoadHome();
	}
}
