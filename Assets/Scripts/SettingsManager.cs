﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

	const string Title = "Settings";
	static string[] Settings = {"Bind Account", "Sound", "Clear Data"};

	public ScrollingList ScrollingList;
	public GameObject SettingsButtonPrefab;

	void Awake() {
		ScrollingList = GameObject.FindGameObjectWithTag("ScrollingList").GetComponent<ScrollingList>();
		SettingsButtonPrefab = (GameObject)Resources.Load("SettingsPrefabs/SettingsListItemButton");
	}

	// Use this for initialization
	void Start () {
		if (Application.isEditor && !GameObject.Find("GameManager")) {
			SceneLoader.LoadGame();
		}

		Text title = GameObject.FindGameObjectWithTag("Title").GetComponentInChildren<Text>();
		title.text = Title;

		LoadSettingsList();
	}
	
	public void LoadSettingsList() {
		ScrollingList.Clear();
		List<GameObject> buttonList = new List<GameObject>();

		foreach (string name in Settings) {
			GameObject newButton = (GameObject)Instantiate(SettingsButtonPrefab);

			Text nameText = newButton.transform.Find("List/Name").GetComponent<Text>();
			nameText.text = name;

			newButton.GetComponent<Button>().onClick
				.AddListener(() => LoadSceneWithName(name));

			buttonList.Add(newButton);
		}
			
		ScrollingList.Populate(buttonList);
	}

	public static void LoadSceneWithName(string name) {
		switch (name)
		{
		case "Bind Account":
			SceneLoader.LoadBindAccount();
			break;
		case "Sound":
			SceneLoader.LoadSoundSettings();
			break;
		case "Clear Data":
			PlayerPrefs.DeleteAll();
			break;
		default:
			Debug.LogError("Invalid setting name!");
			break;
		}
	}
}
