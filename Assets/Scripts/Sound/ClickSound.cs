﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class ClickSound : MonoBehaviour {

	public AudioClip Clip;

	private Button Button;
	//private AudioSource Source;

	void Awake() {
		// gameObject.AddComponent<AudioSource>();
		// Source = GetComponent<AudioSource>();

		Button = GetComponent<Button>();

		if(Clip == null) {
			Clip = (AudioClip)Resources.Load("Sounds/Menu/hunters_click");
		}
	}

	// Use this for initialization
	void Start () {
		//Source.clip = Clip;
		//Source.playOnAwake = false;

		Button.onClick.AddListener(() => SoundManager.Instance.PlaySingle(Clip));
	}
}
