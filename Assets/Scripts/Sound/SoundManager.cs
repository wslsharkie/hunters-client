﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour {

	public static SoundManager Instance = null;

	public AudioSource fxSource;
	public AudioSource musicSource;

	public AudioClip[] MusicClips;

	public const string MusicPrefKey = "Sound_Music";
	public const string EffectsPrefKey = "Sound_Effects";

	void Awake() {
		if(Instance == null) {
			Instance = this;
		} else if(Instance != this) {
			Destroy(gameObject);   
			return;
		}
	}

	// Use this for initialization
	void Start () {
		SceneManager.sceneLoaded += OnSceneLoaded;

		int musicPref = PlayerPrefs.GetInt(SoundManager.MusicPrefKey);
		int effectsPref = PlayerPrefs.GetInt(SoundManager.EffectsPrefKey);
		ToggleSoundSetting(musicSource, musicPref);
		ToggleSoundSetting(fxSource, effectsPref);
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		if(scene.name == "Home") {
			PlayMusicSingle(MusicClips[0]);
		}
	}

	public void PlayMusicSingle(AudioClip clip) {
		/*musicSource.clip = clip;
		musicSource.loop = false;
		musicSource.Play(); */
	}
		
	public void PlaySingle(AudioClip clip) {
		fxSource.clip = clip;
		fxSource.Play();
	}

	public void ToggleSoundSetting(AudioSource source, int active) {
		if(active == 0) {
			source.volume = 1;;
		} else {
			source.volume = 0;
		}
	}

	public void SetSoundSetting(string key, int setting) {
		PlayerPrefs.SetInt(SoundManager.EffectsPrefKey, setting);
	}
}
