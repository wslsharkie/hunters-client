﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettingsManager : MonoBehaviour {

	SoundManager SoundManager;
	Canvas Canvas;
	Button MusicButton;
	Button EffectsButton;
	Text MusicText;
	Text EffectsText;

	int Music;
	int Effects;

	void Awake() {
		if (Application.isEditor && !GameObject.FindGameObjectWithTag("GameManager")) {
			GameManager.ReloadGame();
			return;
		}

		SoundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		MusicButton = Canvas.transform.Find("List/Music/MusicButton").GetComponent<Button>();
		EffectsButton = Canvas.transform.Find("List/Effects/EffectsButton").GetComponent<Button>();
		MusicText = MusicButton.transform.GetComponentInChildren<Text>();
		EffectsText = EffectsButton.transform.GetComponentInChildren<Text>();
	}

	// Use this for initialization
	void Start () {
		Music = PlayerPrefs.GetInt(SoundManager.MusicPrefKey);
		Effects = PlayerPrefs.GetInt(SoundManager.EffectsPrefKey);

		UpdateButtonText(MusicText, Music);
		UpdateButtonText(EffectsText, Effects);
	}

	void UpdateButtonText(Text buttonText, int active) {
		if(active == 0) {
			buttonText.text = "On";
		} else {
			buttonText.text = "Off";
		}
	}
		
	public void OnMusicButtonClick() {
		if(Music == 0) {
			SoundManager.SetSoundSetting(SoundManager.MusicPrefKey, 1);
			Music = 1;
		} else {
			SoundManager.SetSoundSetting(SoundManager.MusicPrefKey, 0);
			Music = 0;
		}

		UpdateButtonText(MusicText, Music);
		SoundManager.ToggleSoundSetting(SoundManager.musicSource, Music);
	}

	public void OnEffectsButtonClick() {
		if(Effects == 0) {
			SoundManager.SetSoundSetting(SoundManager.EffectsPrefKey, 1);
			Effects = 1;
		} else {
			SoundManager.SetSoundSetting(SoundManager.EffectsPrefKey, 0);
			Effects = 0;
		}
		UpdateButtonText(EffectsText, Effects);
		SoundManager.ToggleSoundSetting(SoundManager.fxSource, Effects);
	}
}
