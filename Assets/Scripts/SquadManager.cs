﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FullSerializer;
using UnityEngine.UI;

//58bdc6e26cad94080eb8d2dd
//58bdc6e26cad94080eb8d2dd

public class SquadManager : MonoBehaviour {

	Canvas canvas;
	public GameObject SquadList;
	public GameObject MembersList;
	public GameObject SquadHome;
	public NavBarManager navBars;
	public GameObject CampfireButton;
	public GameObject MembersButton;
	public GameObject ThirdButton;
	public long playerRank = 2;
	public bool isOfficer = false;
	public bool isLeader = false;
	public string playerID;
	public bool allowTabs;
	public string popupPlayerId;
	public string popupSquadID;


	// Use this for initialization
	void Start () {

		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		} 

//		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
//			Debug.Log ("O.O O.O O.O POSTMAN PRINT for Squad Page");
//			Debug.Log(entry.Key);
//			Debug.Log(entry.Value);
//		}

		//Hides the nav bars from this scene
		if (!navBars) {
			navBars = GameObject.FindGameObjectWithTag("NavBarManager").GetComponent<NavBarManager>();
		}

		navBars.SetBarVisibility(NavBarManager.NavBarState.BottomOnly);

		if (!canvas) {
			canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		}

		SquadList = canvas.gameObject.transform.Find("SquadsList").gameObject;
		MembersList = canvas.gameObject.transform.Find("MembersList").gameObject;
		SquadHome = canvas.gameObject.transform.Find("SquadHome").gameObject;
		CampfireButton = canvas.gameObject.transform.Find ("CampireBtn").gameObject;//.GetComponent<Button>();
		MembersButton = canvas.gameObject.transform.Find ("MembersBtn").gameObject;//.GetComponent<Button>();
		ThirdButton = canvas.gameObject.transform.Find ("ThirdBtn").gameObject;//.GetComponent<Button>();
		playerID = Player.GetID ();
		allowTabs = true;

		//check Postman.RESPONSE for what to load
		if (Postman.RESPONSE.ContainsKey("list")){
			List<fsData> squadList = Postman.RESPONSE["list"].AsList;
			allowTabs = false;
			SetupSquadList(squadList);
		}else{
			SetupSquadHome(Postman.RESPONSE);
		}


		
	}
	

	void SetupSquadList(List<fsData> squadList){

		//Debug.Log (">>>>> starting SetupSquadList");
		int numberOfEntries = squadList.Count;
		int maxSize = 40;
		float plankHeight = 160f;
		float yOffset = -94f;
		SquadHome.SetActive(false);
		MembersList.SetActive (false);
		SquadList.SetActive(true);



		Transform contentTransform = SquadList.transform.Find("Scroll View/Viewport/Content").transform;

		List<GameObject> squadPlanks = new List<GameObject>();
		GameObject SquadPlankPrefab = (GameObject)Resources.Load("SquadPagePrefabs/SquadPlank");
		for (int i = 0; i < numberOfEntries; i++) {
			GameObject plank = Instantiate(SquadPlankPrefab, contentTransform, false);
			//Setup plank
			Dictionary<string, fsData> squad = squadList[i].AsDictionary;
			plank.transform.Find("SquadNameTxt").GetComponent<Text>().text = squad["name"].AsString;
			plank.transform.Find("AverageLevelTxt").GetComponent<Text>().text = "Pow: " + squad["avgLevel"];
			plank.transform.Find("SizeTxt").GetComponent<Text>().text = squad["size"].AsInt64.ToString() + "/" + maxSize.ToString();

			//plank.transform.localPosition = new Vector2(0, yOffset);

			string guildID = squad["_id"].AsString;
		
			plank.GetComponent<Button>().onClick.AddListener(delegate {
				GetSquadInfo(guildID);
			});

			//yOffset -= plankHeight;
			squadPlanks.Add(plank);
		}
	}

	void SetupMembersList(Dictionary<string, fsData> response) {
		//Debug.Log (">>>> starting SetupMembersList");
		Dictionary<string, fsData> squad = response["squad"].AsDictionary;
		Dictionary<string, fsData> members = squad["members"].AsDictionary;
		SquadList.SetActive(false);
		SquadHome.SetActive(false);
		MembersList.SetActive(true);

//		foreach (KeyValuePair<string, fsData> entry in members) {
//			Debug.Log ("O.O O.O O.O POSTMAN PRINT for Members");
//			Debug.Log(entry.Key);
//			Debug.Log(entry.Value);
//		}

		Transform SquadPlayerHolder = MembersList.transform.Find("Scroll View/Viewport/Content").transform;
		for (int i = 0; i < SquadPlayerHolder.childCount; i++) {
			Destroy(SquadPlayerHolder.GetChild(i).gameObject);
		}

		bool isMember = response["isMember"].AsBool;
		string squadID = squad["_id"].AsString;

		GameObject MemberPlank = (GameObject)Resources.Load ("SquadPagePrefabs/MemberPlank");

		//Setup member list
//		int squadCount = members.Count;
//		float yOffset = 91;
//		float xOffset = 0;
//		Vector2 plankSize = new Vector2(520, 160);
//		int j = 0;


		foreach (KeyValuePair<string, fsData> entry in members) {
			Dictionary<string, fsData> memberData = entry.Value.AsDictionary;
			string memberID = memberData["id"].AsString;
			long rank = memberData["rank"].AsInt64;
			if (memberID == playerID) {
				playerRank = rank;
				if (rank == 0) {
					isLeader = true;
				}

				if (rank == 1) {
					isOfficer = true;
				}
			}
		}

		foreach (KeyValuePair<string, fsData> entry in members) {
			GameObject player = Instantiate(MemberPlank, SquadPlayerHolder, false);

			Dictionary<string, fsData> memberData = entry.Value.AsDictionary;

			player.transform.Find("nameTxt").GetComponent<Text>().text = memberData["name"].AsString;

			string memberID = memberData["id"].AsString;
			long level = memberData["level"].AsInt64;
			player.transform.Find("levelTxt").GetComponent<Text>().text = "Lvl " + level.ToString();

			long rank = memberData["rank"].AsInt64;
			string rankTxt = string.Empty;
			switch (rank) {
			case 0:
				rankTxt = "Leader";
				break;
			case 1:
				rankTxt = "Officer";
				break;
			case 2:
				rankTxt = "Member";
				break;
			default:
				rankTxt = "Member";
				break;
			}

			player.transform.Find("rankTxt").GetComponent<Text>().text = rankTxt;

			Dictionary<string, Action> clickParams = new Dictionary<string, Action>();
			clickParams ["Profile"] = OnProfileClick;
			if (playerRank < rank) {
				clickParams ["Promote"] = OnPromoteBtnClick;
				clickParams ["Demote"] = OnDemoteBtnClick;
				clickParams ["Kick"] = OnKickBtnClick;
			}
				
			player.GetComponent<Button>().onClick.AddListener(delegate {
				OnMemberPlankClick (memberID, clickParams);
			});

			//Setup for managing this player
//			player.transform.Find("Manage/KickBtn").GetComponent<Button>().onClick.AddListener(delegate {
//				OnKickBtnClick(squadID);
//			});
//
//			player.transform.Find("Manage/PromoteBtn").GetComponent<Button>().onClick.AddListener(delegate {
//				OnPromoteBtnClick(squadID);
//			});



//			player.transform.localPosition = new Vector2(xOffset, yOffset);
//			if (j % 2 == 0) {
//				xOffset += plankSize.x;
//			} else {
//				xOffset = 0;
//				yOffset -= plankSize.y;
//			}
//			j++;

		}

		for (int i = 0; i < 40; i++) {
			GameObject extraPlayer = Instantiate(MemberPlank, SquadPlayerHolder, false);
		}
	}


	void SetupSquadHome(Dictionary<string, fsData> response){

		//Debug.Log (">>>>> starting SetupSquadHome");
		List<fsData> feed = new List<fsData> ();
		Dictionary<string, fsData> squad = response["squad"].AsDictionary;
		Dictionary<string, fsData> members = squad["members"].AsDictionary;
		if (response.ContainsKey ("feed")) {
			feed = response["feed"].AsList;
		}
		bool isMember = response["isMember"].AsBool;
		string squadID = squad["_id"].AsString;
		//Debug.Log("isMember = " + isMember.ToString() + " and squadID = " + squadID);

		//Establish some starting info
		foreach (KeyValuePair<string, fsData> entry in members) {
			Dictionary<string, fsData> memberData = entry.Value.AsDictionary;
			string memberID = memberData["id"].AsString;
			long rank = memberData["rank"].AsInt64;
			if (memberID == playerID) {
				playerRank = rank;
				if (rank == 0) {
					isLeader = true;
				}

				if (rank == 1) {
					isOfficer = true;
				}
			}
		}

		//Setup squad top info
		SquadList.SetActive(false);
		MembersList.SetActive(false);
		SquadHome.SetActive(true);

		//Setup top buttons
		CampfireButton.GetComponent<Button>().onClick.RemoveAllListeners();
		MembersButton.GetComponent<Button>().onClick.RemoveAllListeners();
		ThirdButton.GetComponent<Button>().onClick.RemoveAllListeners();

		if (isMember) {
			CampfireButton.GetComponent<Button>().onClick.AddListener (delegate {
				SetupSquadHome (Postman.RESPONSE);
			});

			MembersButton.GetComponent<Button>().onClick.AddListener (delegate {
				SetupMembersList (Postman.RESPONSE);
			});

			ThirdButton.transform.Find ("Text").GetComponent<Text> ().text = "Options";
			Dictionary<string, Action> options = new Dictionary<string, Action>();
			if (!isLeader) {
				options ["Leave Squad"] = OnLeaveSquadClick;
			}
			
			ThirdButton.GetComponent<Button>().onClick.AddListener (delegate {
				OnOptionsSelectorClick(squadID, options);
			});
		} else {
			CampfireButton.GetComponent<Button>().onClick.AddListener (delegate {
				SetupSquadHome (Postman.RESPONSE);
			});

			MembersButton.GetComponent<Button>().onClick.AddListener (delegate {
				SetupMembersList (Postman.RESPONSE);
			});

			ThirdButton.transform.Find ("Text").GetComponent<Text> ().text = "Join";
			ThirdButton.GetComponent<Button>().onClick.AddListener (delegate {
				OnJoinSquadClick(squadID);
			});
		}

		//Transform SquadPlayerHolder = SquadHome.transform.Find("SquadPlayers").transform;
		Transform feedTransform = SquadHome.transform.Find("Scroll View/Viewport/Content").transform;
		GameObject feedMessagePlank = (GameObject)Resources.Load ("SquadPagePrefabs/FeedMessagePlank");

		//Get rid of all children
		for (int i = 0; i < feedTransform.childCount; i++) {
			Destroy(feedTransform.GetChild(i).gameObject);
		}

		if (isMember) {
			foreach (fsData entry in feed) {
				Dictionary<string, string> message = JsonSerializer.ParseFSData(entry.AsDictionary);
				GameObject plank;
				if (message ["type"] == "5" || message.ContainsKey ("msg")) {
					plank = Instantiate (feedMessagePlank, feedTransform, false);
					string memberID = message ["pid"];

					plank.transform.Find ("nameTxt").GetComponent<Text> ().text = message ["pname"];
					plank.transform.Find ("messageTxt").GetComponent<Text> ().text = message ["msg"];
				} else {
					continue;
				}
			}
		}
			

//		Button interactionBtn = SquadHome.transform.Find("SquadBtn").GetComponent<Button>();
//		interactionBtn.onClick.RemoveAllListeners();
//		if (isMember) {
//			interactionBtn.gameObject.transform.Find("Text").GetComponent<Text>().text = "Leave";
//				
//			interactionBtn.onClick.AddListener(delegate {
//				OnLeaveSquadClick(squadID);
//			});
//		} else {
//			interactionBtn.gameObject.transform.Find("Text").GetComponent<Text>().text = "Join";
//
//			interactionBtn.onClick.AddListener(delegate {
//				OnJoinSquadClick(squadID);
//			});
//		}

			//Setup for managing this player
//			player.transform.Find("Manage/KickBtn").GetComponent<Button>().onClick.AddListener(delegate {
//				OnKickBtnClick(squadID);
//			});
//
//			player.transform.Find("Manage/PromoteBtn").GetComponent<Button>().onClick.AddListener(delegate {
//				OnPromoteBtnClick(squadID);
//			});

//		if (!isOfficer) {
//			SquadHome.transform.Find("ManageBtn").gameObject.SetActive(false);
//		}
	}

	/*
	public void OnCampFireClick(){
		if (!allowTabs) {
			return;
		}
	}

	public void OnMembersClick(){

		if (!allowTabs) 
		{
			return;
		}

		Transform playersHolder = SquadHome.transform.Find("SquadPlayers");
			
		for (int i = 0; i < playersHolder.childCount; i++) {
			Transform playerPlank = playersHolder.GetChild(i);

			//Do a check here. Only show the manage if 
			//we are a squad leader
			//we are an officer looking a regular member

			//Probably should actually store the player id and rank on the plank rather than using
			//the text as a workaround. To do later

			string rankTxt = playerPlank.Find("RankTxt").GetComponent<Text>().text;
				
			if (isOfficer && rankTxt == "Member" ||isLeader) {
					playerPlank.Find("Manage").gameObject.SetActive(true);
			}




		}
	}
	*/

	public void OnOptionsSelectorClick(string squadID, Dictionary<string, Action> buttons) {
		
		popupSquadID = squadID;

		GameObject popup = PopupManager.OpenPopup("SelectorPopup");
		SelectorPopup scriptComp = popup.GetComponent<SelectorPopup> ();
		scriptComp.Populate(buttons);


	}

	public void OnMemberPlankClick(string memberID, Dictionary<string, Action> buttons){

		popupPlayerId = memberID;
		GameObject popup = PopupManager.OpenPopup("SelectorPopup");
		SelectorPopup scriptComp = popup.GetComponent<SelectorPopup> ();
		scriptComp.Populate (buttons);
		//popup.Populate (buttons);
		//popup.transform.FindChild ("HeroNameTxt").GetComponent<Text> ().text = "Hello World! " + achievement_id;
		//show loading popup
		//LoadingPopup.SetActive(true);
	}

//	public void OnSelectorCloseClick(){
//
//		PopupManager.ClosePopup("SelectorPopup");
//		popupPlayerId = string.Empty;
//	}

	public void OnProfileClick(){

		Dictionary<string, string> body = new Dictionary<string, string>();
		PopupManager.ClosePopup("SelectorPopup");
		popupPlayerId = string.Empty;
		GameObject popup = PopupManager.OpenPopup("TextSmallPopup");
		PopupManager.SetTextForSimplePopup(popup, "Profile Page Coming Soon!");
	}

	void OnKickBtnClick()
	{
		PopupManager.ClosePopup("SelectorPopup");
		if (popupPlayerId == string.Empty)
		{
			Debug.Log ("!!! I messed up with popupPlayerId in kick");
			return;
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = popupPlayerId;
		popupPlayerId = string.Empty;

		StartCoroutine(Postman.KickPlayer(body, SquadMembersResponse));
	}

	void OnPromoteBtnClick()
	{
		PopupManager.ClosePopup("SelectorPopup");
		if (popupPlayerId == string.Empty)
		{
			Debug.Log ("!!! I messed up with popupPlayerId in kick");
			return;
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = popupPlayerId;
		popupPlayerId = string.Empty;

		StartCoroutine(Postman.PromotePlayer(body, SquadMembersResponse));
	}

	void OnDemoteBtnClick()
	{
		PopupManager.ClosePopup("SelectorPopup");
		if (popupPlayerId == string.Empty)
		{
			Debug.Log ("!!! I messed up with popupPlayerId in kick");
			return;
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["player_id"] = popupPlayerId;
		popupPlayerId = string.Empty;

		StartCoroutine(Postman.DemotePlayer(body, SquadMembersResponse));
	}

	public void OnLeaveSquadClick(){
		PopupManager.ClosePopup("SelectorPopup");
		if (popupSquadID == string.Empty)
		{
			Debug.Log ("!!! I messed up with popupSquadID in kick");
			return;
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = popupSquadID;
		popupSquadID = string.Empty;

		//Debug.Log (">>>> Sending Leave Squad request");

		StartCoroutine(Postman.LeaveSquad(body, SquadHomeResponse));


	}



	public void OnJoinSquadClick(string squadID){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = squadID;

		//Debug.Log (">>>>> Sending Join Squad request");
		StartCoroutine(Postman.JoinSquad(body, SquadHomeResponse));
	}

	public void GetSquadInfo(string squadID){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = squadID;

		StartCoroutine(Postman.GetSquadInfo(body, SquadHomeResponse));
	}

	public void GetSquadMembers(string squadID){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = squadID;

		StartCoroutine(Postman.GetSquadInfo(body, SquadMembersResponse));
	}

	public void SquadHomeResponse(){
		SetupSquadHome(Postman.RESPONSE);
	}

	public void SquadMembersResponse(){
		SetupMembersList(Postman.RESPONSE);
	}

	//Testing function to generate squads

	public void GenerateSquads(){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["squad_id"] = "5913a8fe7b697a5f1ed47d1d";

		StartCoroutine(Postman.JoinSquad(body, SceneLoader.LoadHome));
	}

	public void GeneratePlayers(){

		StartCoroutine(Postman.GeneratePlayerList(SceneLoader.LoadHome));

	}

}
