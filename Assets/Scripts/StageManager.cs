﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;
using System.Linq;
using UnityEngine.SceneManagement;


public class StageManager : MonoBehaviour {

	Dictionary<string, Stage> Stages;

	public Canvas Canvas;
	public ScrollingList ScrollingList;
	public GameObject StageButtonPrefab;
	public GameObject StageBattleButtonPrefab;

	void Awake() {

		if (Application.isEditor && !GameObject.FindGameObjectWithTag("GameManager")) {
			SceneManager.LoadScene("LoadGame");
			return;
		}


		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		ScrollingList = GameObject.FindGameObjectWithTag("ScrollingList").GetComponent<ScrollingList>();
		StageButtonPrefab = (GameObject)Resources.Load("StagePrefabs/StageListItemButton");
		StageBattleButtonPrefab = (GameObject)Resources.Load("StagePrefabs/StageBattleButton");
	}

	// Use this for initialization
	void Start () {

		Text title = GameObject.FindGameObjectWithTag("Title").GetComponentInChildren<Text>();
		title.text = "World Stages";

		LoadStages();

		NewPlayerExperienceManager.FireEvent("NewPlayerExperience1");

	}

	public void LoadStages() {
		if(Postman.RESPONSE.ContainsKey("stages") == false) {
			Debug.LogWarning("Missing stages response!");
			SceneLoader.LoadHome();
			return;
		}

		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		ScrollingList.Clear();
		Stages = new Dictionary<string, Stage>();
		List<GameObject> buttonList = new List<GameObject>();

		foreach(KeyValuePair<string, fsData> stageEntry in Postman.RESPONSE["stages"].AsDictionary) {
			// create stage data model
			Stage stage = (Stage)ScriptableObject.CreateInstance("Stage");
			stage.Init(stageEntry.Key, stageEntry.Value.AsDictionary);
			Stages[stageEntry.Key] = stage;

			// create stage button list item
			GameObject newButton = (GameObject)Instantiate(StageButtonPrefab);

			Text titleText = newButton.transform.Find("List/Title").GetComponent<Text>();
			Text nameText = newButton.transform.Find("List/Name").GetComponent<Text>();
			Text levelText = newButton.transform.Find("List/Level").GetComponent<Text>();
			GameObject LockedPlate = newButton.transform.Find("Locked").gameObject;


			titleText.text = stage.GetValue("title");
			levelText.text = stage.GetValue("desc");

			nameText.text = stage.GetName();
			if(stage.Locked) {
				LockedPlate.SetActive(true);
			} else {
				string stageKey = stageEntry.Key;
				newButton.GetComponent<Button>().onClick
					.AddListener(() => LoadBattles(stageKey));
			}

			buttonList.Add(newButton);

		}

		ScrollingList.Populate(buttonList);
	}

	public void LoadBattles(string stageId) {
		ScrollingList.Clear();
		List<GameObject> buttonList = new List<GameObject>();

		for(var battleIndex = 0; battleIndex < Stages[stageId].Battles.Count; battleIndex++) {
			StageBattle stageBattle = Stages[stageId].Battles[battleIndex];


			// todo: change to battle button
			GameObject newButton = (GameObject)Instantiate(StageBattleButtonPrefab);

			Text titleText = newButton.transform.Find("List/Title").GetComponent<Text>();
			Text nameText = newButton.transform.Find("List/Name").GetComponent<Text>();
			Text levelText = newButton.transform.Find("List/Level").GetComponent<Text>();
			Text energyText = newButton.transform.Find("Energy/Text").GetComponent<Text>();
			Image TitleBackplate = newButton.transform.Find("TitleBackplate").GetComponent<Image>();
			GameObject LockedPlate = newButton.transform.Find("Locked").gameObject;

			if (stageBattle.GetTimesCompleted() >= stageBattle.GetCompletionsRequired()){
				titleText.text = "COMPLETED";
				TitleBackplate.enabled = true;
			}else{
				Destroy(titleText.gameObject);
				TitleBackplate.enabled = false;
			}

			//titleText.text = "Completed: "+stageBattle
			levelText.text = stageBattle.GetValue("desc");
			energyText.text = stageBattle.GetValue("energy");

			nameText.text = stageBattle.GetName();
			if(stageBattle.Locked) {
				LockedPlate.SetActive(true);
			} else {
				string battleID = battleIndex.ToString();
				newButton.GetComponent<Button>().onClick
					.AddListener(() => LoadBattleFromStage(stageId, battleID));
			}

			buttonList.Add(newButton);
		}

		ScrollingList.Populate(buttonList);

		NewPlayerExperienceManager.FireEvent("NewPlayerExperience2");
	}
		
	public void LoadBattleFromStage(string stageId, string battleId){
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["stage_id"] = stageId;
		body["battle_id"] = battleId;
		StartCoroutine(Postman.CreateBattle(body, SceneLoader.LoadBattle));
	}
}
