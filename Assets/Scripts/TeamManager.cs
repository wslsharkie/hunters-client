﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TeamManager : MonoBehaviour {

	Dictionary<string, fsData> teamInfo;
	Dictionary<string, fsData> heroInfo;

	public GameObject[] currentTeamGameObjects; //Gameobjects for the team in the list and details page
	public GameObject heroPrefab; //

	//subpages
	public GameObject heroList;
	public GameObject heroDetails;
	public GameObject heroEquipment;
	public GameObject equipmentCrafting;
	public GameObject heroStory;
	public GameObject canvas;
	public GameObject activePage;

	List<string> currentTeamHeroIDs;
	List<List<string>> teamHeroIDs;
	long activeLoadoutID;
	const int numberOfLoadouts = 5;
	List<GameObject> activeLoadoutIcons;
	List<GameObject> inactiveLoadoutIcons;
	string currentHeroID = "";
	string currentEquipmentID = "";

	Dictionary<string, Hero> playerHeroes;
	Dictionary<string, Equipment> currentHeroEquipment;
	Dictionary<string, string> currentHeroEquipmentLevels;
	Dictionary<string, Item> items;

	#region Unity Functions

	void Start () {

		if (Application.isEditor) {
			if (!GameObject.Find("Authentication")) {
				SceneManager.LoadScene("LoadGame");
				return;
			}
		}

		playerHeroes = new Dictionary<string, Hero>();
		currentHeroEquipment = new Dictionary<string, Equipment>();
		teamHeroIDs = new List<List<string>>();
		items = new Dictionary<string, Item>();

		FullSerializer.fsSerializer.LogDictionary(Postman.RESPONSE);

		teamInfo = Postman.RESPONSE["team"].AsDictionary;
		heroInfo = Postman.RESPONSE["heroes"].AsDictionary;

		//Load in any missing links in the team manager

		if (!heroList) {
			heroList = GameObject.FindGameObjectWithTag("HeroList");
		}

		if (!heroDetails) {
			heroDetails = GameObject.FindGameObjectWithTag("HeroDetails");
		}

		if (!heroEquipment) {
			heroDetails = GameObject.FindGameObjectWithTag("HeroDetails");
		}

		if (!equipmentCrafting) {
			equipmentCrafting = GameObject.FindGameObjectWithTag("EquipmentCrafting");
		}

		if (!canvas) {
			canvas = GameObject.FindGameObjectWithTag("Canvas");
		}
			
		//Get current active team
		activeLoadoutID = teamInfo["activeTeamId"].AsInt64;
		List<fsData> teams = teamInfo["teams"].AsList;
		List<fsData> currentHeroIDs = teams[(int)activeLoadoutID].AsList;

		//Setup loadouts
		int loadout = 0;
		foreach (fsData entry in teams) {
			teamHeroIDs.Add(new List<string>());
			List<fsData> heroIDs = teams[loadout].AsList;

			for (int i = 0; i < heroIDs.Count; i++) {
				teamHeroIDs[loadout].Add(heroIDs[i].AsInt64.ToString());
			}
			loadout++;
		}
		GameManager.currentLoadouts = (PlayerLoadouts)ScriptableObject.CreateInstance("PlayerLoadouts");
		GameManager.currentLoadouts.Init(teamHeroIDs);

		//Temp. This should be setup at home start
		GameManager.savedLoadouts = (PlayerLoadouts)ScriptableObject.CreateInstance("PlayerLoadouts");
		GameManager.savedLoadouts.Init(teamHeroIDs);


		currentTeamHeroIDs = teamHeroIDs[(int)activeLoadoutID];

		//load hero data into hero objects
		foreach (KeyValuePair<string, fsData> entry in heroInfo) {

			//FullSerializer.fsSerializer.LogDictionary(entry.Value.AsDictionary);

			Hero hero = (Hero)ScriptableObject.CreateInstance("Hero");
			hero.Init(entry.Value.AsDictionary);
			playerHeroes[entry.Key] = hero;
		}

		activePage = heroList;
			
		SetupTopTeam();
		SetupHeroList();
		SetupLoadout();

		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience4")) {
			NewPlayerExperienceManager.FireEvent("NewPlayerExperience5");
		}

	}

	//Do some saving before we leave this scene
	void OnDestroy(){
	}

	#endregion

	#region Initial Setup

	void SetupTopTeam(GameObject newActivePage){
		activePage = newActivePage;
		SetupTopTeam();
	}

	//Setup the heroes at the top of the page
	void SetupTopTeam(){

		Transform teamTransform = activePage.transform.Find("Team");

		for (int i = 0; i < teamTransform.childCount; i++) {
			Transform heroTransform = teamTransform.Find("Hero" + i.ToString());

			//Get hero info for this id
			if (heroInfo.ContainsKey(currentTeamHeroIDs[i])) {
				Hero currentHero = playerHeroes[currentTeamHeroIDs[i]];

				//Frame
				string frameName = currentHero.GetFrameName();
				Texture2D frameTexture = (Texture2D)Resources.Load("Images/Battle/Hero_frames/" + frameName);
				Sprite frame = Sprite.Create(frameTexture, new Rect(0, 0, frameTexture.width, frameTexture.height), new Vector2(0.5f, 0.5f));
				heroTransform.Find("frame").GetComponent<Image>().sprite = frame;

				//Stars
				Texture2D starsTexture = (Texture2D)Resources.Load("Images/Hero/rarity stars/" + currentHero.GetStars() + "_stars");
				Sprite stars = Sprite.Create(starsTexture, new Rect(0, 0, starsTexture.width, starsTexture.height), new Vector2(0.5f, 0.5f));
				heroTransform.Find("stars").GetComponent<Image>().enabled = true;
				heroTransform.Find("stars").GetComponent<Image>().sprite = stars;

				//Portait
				string portaitPath = currentHero.GetPortraitPath();
				Texture2D portaitTexture = ClientCache.GetTexture(portaitPath);
				if (portaitTexture != null) {
					Sprite newPortait = Sprite.Create(portaitTexture, new Rect(0, 0, portaitTexture.width, portaitTexture.height), new Vector2(0.5f, 0.5f));
					heroTransform.Find("Image").GetComponent<Image>().enabled = true;
					heroTransform.Find("Image").GetComponent<Image>().sprite = newPortait;
				}

				string heroID = currentTeamHeroIDs[i];
				heroTransform.GetComponent<Button>().onClick.AddListener(delegate {
					OnHeroClick(heroID);
				});
			} else {
				heroTransform.Find("Image").GetComponent<Image>().enabled = false;
				heroTransform.Find("stars").GetComponent<Image>().enabled = false;


			}
		}
	}

	//Setup the heroes in the hero list
	void SetupHeroList(){

		//clear out any existing heroes
		//Todo: add these to the objectCachingPool 
		GameObject scrollContent = heroList.transform.Find("Scroll View/Viewport/Content").gameObject;

		for (int i = 0; i < scrollContent.transform.childCount; i++) {
			Destroy(scrollContent.transform.GetChild(i).gameObject);
		}
			

		//go through the player heroes and add anyone not in our team to the lower area
		bool unequippedHeroes = false;
		foreach (KeyValuePair<string, Hero> entry in playerHeroes) {
			string heroID = entry.Value.GetID();

			//Check to make sure we aren't already using this hero
			bool currentlyEquipped = false;
			for (int i = 0; i < currentTeamHeroIDs.Count; i++) {
				if (currentTeamHeroIDs[i] == heroID) {
					currentlyEquipped = true;
					break;
				}
			}

			if (!currentlyEquipped) {
				unequippedHeroes = true;
				GameObject unslottedHero = Instantiate(heroPrefab, scrollContent.transform, false);

				Image heroStars = unslottedHero.transform.Find("stars").GetComponent<Image>();
				Texture2D starsTexture = (Texture2D)Resources.Load("Images/Hero/rarity stars/" + entry.Value.GetStars() + "_stars");
				Sprite stars = Sprite.Create(starsTexture, new Rect(0, 0, starsTexture.width, starsTexture.height), new Vector2(0.5f, 0.5f));
				heroStars.sprite = stars;

				string frameName = entry.Value.GetFrameName();
				Texture2D frameTexture = (Texture2D)Resources.Load("Images/Battle/Hero_frames/" + frameName);
				Sprite frame = Sprite.Create(frameTexture, new Rect(0, 0, frameTexture.width, frameTexture.height), new Vector2(0.5f, 0.5f));
				unslottedHero.transform.Find("frame").GetComponent<Image>().sprite = frame;

				//Check to see if we own this hero yet
				if (entry.Value.GetOwned()) {
					unslottedHero.transform.Find("frame").GetComponent<Image>().color = Color.white;
				} else {
					unslottedHero.transform.Find("frame").GetComponent<Image>().color = Color.gray;

				}

				string portaitPath = entry.Value.GetPortraitPath();
				Texture2D portaitTexture = ClientCache.GetTexture(portaitPath);
				if (portaitTexture != null) {
					Sprite newPortait = Sprite.Create(portaitTexture, new Rect(0, 0, portaitTexture.width, portaitTexture.height), new Vector2(0.5f, 0.5f));
					unslottedHero.transform.Find("Image").GetComponent<Image>().sprite = newPortait;
				}

				unslottedHero.GetComponent<Button>().onClick.AddListener(delegate {
					OnHeroClick(heroID);
				});
			}
		}

		if (!unequippedHeroes) {
			heroList.transform.Find("NoMoreHeroesTxt").gameObject.SetActive(true);
		} else {
			heroList.transform.Find("NoMoreHeroesTxt").gameObject.SetActive(false);

		}

		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience5")){
			NewPlayerExperienceManager.FireEvent("NewPlayerExperience6");
		}
	}

	//Setup the loadout display
	public void SetupLoadout(){

		//get the active/inactive icons when we need them
		if (activeLoadoutIcons == null || inactiveLoadoutIcons == null) {
			activeLoadoutIcons = new List<GameObject>();
			inactiveLoadoutIcons = new List<GameObject>();

			Transform iconParent = heroList.transform.Find("Loadout/LoadoutSwitcher").transform;

			for (int i = 0; i < numberOfLoadouts; i++) {
				activeLoadoutIcons.Add(iconParent.Find("LoadoutIconOn" + i.ToString()).gameObject);
				inactiveLoadoutIcons.Add(iconParent.Find("LoadoutIconOff" + i.ToString()).gameObject);

			}
		}

		for (int i = 0; i < numberOfLoadouts; i++) {
			if (activeLoadoutID == i) {
				activeLoadoutIcons[i].SetActive(true);
				inactiveLoadoutIcons[i].SetActive(false);
			} else {
				activeLoadoutIcons[i].SetActive(false);
				inactiveLoadoutIcons[i].SetActive(true);
			}
		}
	}

	//Setup the display for a hero's current equipment
	public void SetupEquipment(){
		currentHeroEquipment.Clear();

		string heroId = "";

		Dictionary<string, fsData> equipmentDict = Postman.RESPONSE["equipment"].AsDictionary;
		Dictionary<string, fsData> itemsDict = Postman.RESPONSE["items"].AsDictionary;



		foreach (KeyValuePair<string, fsData> entry in itemsDict) {
					Dictionary<string, fsData> itemData = entry.Value.AsDictionary;
			Item newItem = (Item)ScriptableObject.CreateInstance("Item");
			string itemID = itemData["itemId"].AsInt64.ToString();
			newItem.Init(itemID, itemData);
			items[itemID] = newItem;
		}


		if (equipmentDict.ContainsKey("heroId")) {
			heroId = equipmentDict["heroId"].AsInt64.ToString();
		}	

		//Load hero image
		Hero currentHero = playerHeroes[heroId];
		Image heroImage = heroEquipment.transform.Find("Hero").GetComponent<Image>();
		Texture2D heroTexture = ClientCache.GetTexture(currentHero.GetImagePath());

		if (heroTexture != null) {
			Sprite newPortait = Sprite.Create(heroTexture, new Rect(0, 0, heroTexture.width, heroTexture.height), new Vector2(0.5f, 0.5f));
			heroImage.sprite = newPortait;
		}


		//Load the hero name and xp
		heroEquipment.transform.Find("HeroNamePlate/Text").GetComponent<Text>().text = currentHero.GetName();
		heroEquipment.transform.Find("HeroLevelPlate/EnergyBar").GetComponent<Image>().fillAmount = currentHero.GetPercentIntoCurrentLevel();
		heroEquipment.transform.Find("HeroLevelPlate/Text").GetComponent<Text>().text = "Level " + currentHero.GetXPToNextLevel();


		if (equipmentDict.ContainsKey("equipment")) {
			foreach (KeyValuePair<string, fsData> entry in equipmentDict["equipment"].AsDictionary) {
				Equipment newEquipment = (Equipment)ScriptableObject.CreateInstance("Equipment");

				
				newEquipment.Init(heroId, entry.Key, entry.Value.AsDictionary);
			//	Debug.Log("Added equipment with key = " + entry.Key + " and value = " + entry.Value);
				currentHeroEquipment[entry.Key] = newEquipment;
			}
		} 
			
		for (int i = 0; i < currentHeroEquipment.Count; i++) {
			Transform equipmentSlot = heroEquipment.transform.Find("EquipmentSlots/EquipmentSlot" + i.ToString());
			if (equipmentSlot) {
				Equipment equipment = currentHeroEquipment[i.ToString()];

				string name = equipment.GetName();
				equipmentSlot.Find("EquipmentName").GetComponent<Text>().text = name;

				int currentLevel = int.Parse(equipment.GetLevel());
				string bonusTxt = equipment.GetDescForLevel(currentLevel.ToString());
				equipmentSlot.Find("Stats").GetComponent<Text>().text = bonusTxt;

				Button item = equipmentSlot.GetComponent<Button>();
				item.onClick.AddListener(delegate {
					OnItemClick(equipment);
				});
			}
		}


		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience6")) {
			NewPlayerExperienceManager.FireEvent("NewPlayerExperience7");
		}

	}

	//Setup the crafting page for upgrading a heroes equipment
	public void SetupCrafting(Equipment equipment){

		//Get the data for result item f+ ingredients
		string currentLevel = equipment.GetLevel();
		List<Dictionary<string, string>> ingredientsData = equipment.GetIngredientsForLevel((int.Parse(currentLevel)+1).ToString());
		currentEquipmentID = equipment.GetID();

		//Setup initial item
		equipmentCrafting.transform.Find("InitialItem/EquipmentName").GetComponent<Text>().text = equipment.GetName();
		//string bonusText = equipment.GetBonusTextForLevel(currentLevel);
		string bonusText = equipment.GetDescForLevel(currentLevel);
		equipmentCrafting.transform.Find("InitialItem/Stats").GetComponent<Text>().text = bonusText;



		Transform ingredientParent = equipmentCrafting.transform.Find("Ingredients");
		int i;
		for (i = 0; i < ingredientsData.Count; i++) {
			Transform ingredientGameObject = ingredientParent.Find("Ingredient" + i.ToString());

			Dictionary<string, string> ingredient = ingredientsData[i];
			//foreach (KeyValuePair<string, string> entry in ingredient) {
				//Debug.Log(entry.Key);
				//Debug.Log(entry.Value);
			//}

			string itemID = ingredient["id"];
			Item ingredientItem = items[itemID];
			ingredientGameObject.Find("IngredientName").GetComponent<Text>().text = ingredientItem.GetName();

			string ammountNeeded = ingredient["amount"];
			string amountOwned = ingredientItem.GetOwned();
			ingredientGameObject.Find("amountTxt").GetComponent<Text>().text = amountOwned +"/" + ammountNeeded;

	    	string image = ingredientItem.GetImage();
			//Texture2D ingredientTexture = (Texture2D)Resources.Load("Images/PlaceHolders/box");
			Texture2D ingredientTexture = ClientCache.GetTexture(image);

			Image ingredientImage = ingredientGameObject.Find("image").GetComponent<Image>();

			if (ingredientTexture != null) {
				Sprite newImage = Sprite.Create(ingredientTexture, new Rect(0, 0, ingredientTexture.width, ingredientTexture.height), new Vector2(0.5f, 0.5f));
				ingredientImage.sprite = newImage;
			}


		}
		//hide any remaining ingredients
		for (int j = i; j < ingredientParent.childCount; j++) {
			ingredientParent.Find("Ingredient" + j.ToString()).gameObject.SetActive(false);

		}


		string bonusTxt = equipment.GetDescForLevel((int.Parse(currentLevel)+1).ToString());
		equipmentCrafting.transform.Find("resultItem/Stats").GetComponent<Text>().text = bonusTxt;

		string name = equipment.GetName();
		equipmentCrafting.transform.Find("resultItem/EquipmentName").GetComponent<Text>().text = name + " lvl" +currentLevel;

		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience7")) {
			NewPlayerExperienceManager.FireEvent("NewPlayerExperience8");
		}

	}

	public void SetupStory(){
		//Get the 3 story segments

		List<GameObject> storySegments = new List<GameObject>();
		for (int i = 0; i < 3; i++){
			storySegments.Add(heroStory.transform.Find("Story" + i.ToString()).gameObject);
		}

		//get hero stars to see how many story segements we should display
		int storyToDisplay = 1; 

		if (playerHeroes[currentHeroID].GetStars() >= 5) {
			storyToDisplay = 3;
		} else if (playerHeroes[currentHeroID].GetStars() >= 3) {
			storyToDisplay = 2;
		}


		for (int i = 0; i < storyToDisplay; i++) {
			storySegments[i].GetComponent<Animator>().SetTrigger("Show");
		}
	}

	//Sets up the hero details page
	public void SetupHeroDetails(string heroID){

		//Save the hero id for later
		currentHeroID = heroID;

		//Get name for hero id
		Hero currentHero = playerHeroes[currentHeroID];
		string name = currentHero.GetName();
		heroDetails.transform.Find("Details/HeroNamePlate/Text").GetComponent<Text>().text = name;

		//clear out anything on the equip btn
		Button heroInteractionBtn = heroDetails.transform.Find("Details/EquipBtn").GetComponent<Button>();
		heroInteractionBtn.onClick.RemoveAllListeners();

		//Set stars for hero
		Image heroStars = heroDetails.transform.Find("Details/stars").GetComponent<Image>();
		Texture2D starsTexture = (Texture2D)Resources.Load("Images/Hero/rarity stars/" + currentHero.GetStars() + "_stars");
		Sprite stars = Sprite.Create(starsTexture, new Rect(0, 0, starsTexture.width, starsTexture.height), new Vector2(0.5f, 0.5f));
		heroStars.sprite = stars;

		//Set level for hero
		Image heroLevel = heroDetails.transform.Find("Details/HeroLevelPlate/EnergyBar").GetComponent<Image>();
		heroLevel.fillAmount = currentHero.GetPercentIntoCurrentLevel();
		heroDetails.transform.Find("Details/HeroLevelPlate/Text").GetComponent<Text>().text = "Level 1";

		//set Image for hero
		Image heroImage = heroDetails.transform.Find("Details/HeroArt").GetComponent<Image>();

		Texture2D heroTexture = ClientCache.GetTexture(currentHero.GetImagePath());

		if (heroTexture != null) {
			Sprite newPortait = Sprite.Create(heroTexture, new Rect(0, 0, heroTexture.width, heroTexture.height), new Vector2(0.5f, 0.5f));

			heroImage.sprite = newPortait;
		}



		//Get abilities
		List<Ability> abilities = currentHero.GetAbilities();
		Transform abilityTransform = heroDetails.transform.Find("Details/Abilties");
		for (int i = 0; i < abilityTransform.childCount; i++) {
			Transform abilityPlank = abilityTransform.Find("HeroAbility" + i.ToString());

			if (i >= abilities.Count) {
				abilityPlank.gameObject.SetActive(false);
			} else {
				abilityPlank.gameObject.SetActive(true);
				abilityPlank.Find("nameTxt").GetComponent<Text>().text = abilities[i].GetName();
				abilityPlank.Find("descriptionTxt").GetComponent<Text>().text = abilities[i].GetDescription();

			}
		}

		//Set essence
		heroDetails.transform.Find("Details/Promote/Text").GetComponent<Text>().text = currentHero.GetEssence();

		//Set stat details
		heroDetails.transform.Find("Details/StatPanel/ATK/Value").GetComponent<Text>().text = currentHero.GetAttack();
		heroDetails.transform.Find("Details/StatPanel/DEF/Value").GetComponent<Text>().text = currentHero.GetDefense();
		heroDetails.transform.Find("Details/StatPanel/HTH/Value").GetComponent<Text>().text = currentHero.GetHealthMax();
		heroDetails.transform.Find("Details/StatPanel/CRT/Value").GetComponent<Text>().text = currentHero.GetCrit();
		heroDetails.transform.Find("Details/StatPanel/AGI/Value").GetComponent<Text>().text = currentHero.GetAgility();
		heroDetails.transform.Find("Details/StatPanel/RES/Value").GetComponent<Text>().text = currentHero.GetResist();


		//Checks to see if we have an empty slot in our team
		int emptySlot = -1;
		bool foundHeroInLoadout = false;
		for (int i = 0; i < currentTeamHeroIDs.Count; i++) {
			if (currentTeamHeroIDs[i] == heroID) {
				foundHeroInLoadout = true;
			} else if (currentTeamHeroIDs[i] == "-1") {
				emptySlot = i;
			}
		} 

		if (foundHeroInLoadout) {
			heroInteractionBtn.gameObject.transform.Find("Text").GetComponent<Text>().text = "Unequip";
			heroInteractionBtn.onClick.AddListener(delegate {
				OnUnequipHeroClick(heroID);
			});
		} else if (emptySlot > -1 && currentHero.GetOwned()) {
			heroInteractionBtn.gameObject.transform.Find("Text").GetComponent<Text>().text = "Equip";

			heroInteractionBtn.onClick.AddListener(delegate {
				OnEquipHeroClick(heroID);
			});
		}else{
			heroInteractionBtn.gameObject.transform.Find("Text").GetComponent<Text>().text = "Back";
			heroInteractionBtn.onClick.AddListener(delegate {
				BackToHeroList();
			});
		}
			
		Button heroEquipmentBtn = heroDetails.transform.Find("Details/EquipmentBtn").GetComponent<Button>();
		heroEquipmentBtn.onClick.RemoveAllListeners();
		heroEquipmentBtn.interactable = true;
		heroEquipmentBtn.onClick.AddListener(delegate {
			OnEquipmentBtnClick(heroID);
		});


		//Disable the equipment page for unowned heroes
		if (!currentHero.GetOwned() ){
			heroEquipmentBtn.interactable = false;
		}

		Button storyBtn = heroDetails.transform.Find("Details/StoryBtn").GetComponent<Button>();
		storyBtn.onClick.RemoveAllListeners();
		storyBtn.onClick.AddListener(delegate {
			OnStoryBtnClick(heroID);
		});

		Button promoteBtn = heroDetails.transform.Find("Details/PromoteBtn").GetComponent<Button>();
		promoteBtn.onClick.RemoveAllListeners();

		if (currentHero.GetOwned()) {
			promoteBtn.onClick.AddListener(delegate {
				OnPromoteBtnClick(heroID);
			});
		} else {
			promoteBtn.onClick.AddListener(delegate {
				OnCreateBtnClick(heroID);
			});

		}

		//Disable all the other buttons except for upgrade when in this stage of the new player experience
		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience6") &&
		    !NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience7")) {
			storyBtn.interactable = false;
			heroInteractionBtn.interactable = false;
			promoteBtn.interactable = false;
		} else {
			//Got to renable them for when you come back
			storyBtn.interactable = true;
			heroInteractionBtn.interactable = true;
			promoteBtn.interactable = true;

		}

	}
		
	#endregion

	#region Button Handlers

	//handler for clicking on heroes in list or the top
	public void OnHeroClick(string heroID){

		AnimateToSubPage(heroDetails);
		SetupTopTeam();
		SetupHeroDetails(heroID);

	}

	//handler for removing a hero from the team
	public void OnUnequipHeroClick(string heroID){

		//Debug.Log("Got Unequip hero click");

		for (int i = 0; i < currentTeamHeroIDs.Count; i++) {
			if (currentTeamHeroIDs[i] == heroID){
				currentTeamGameObjects[i].transform.Find("Image").GetComponent<Image>().enabled = false;
				currentTeamGameObjects[i].transform.Find("stars").GetComponent<Image>().enabled = false;

				currentTeamHeroIDs[i] = "-1";

				GameManager.currentLoadouts.ChangeHero((int)activeLoadoutID, i, "-1");

			}
		}

		BackToHeroList();
	}


	//For adding a hero to the team
	//adds the hero in the first avaiable slot
	public void OnEquipHeroClick(string heroID){

		for (int i = 0; i < currentTeamHeroIDs.Count; i++) {
			if (currentTeamHeroIDs[i] == "-1") {
				currentTeamHeroIDs[i] = heroID;


				//Update heroes in game manager
				GameManager.currentLoadouts.ChangeHero((int)activeLoadoutID, i, heroID);

				break;
			}
		}

		BackToHeroList();

		//reset the top team		
		SetupTopTeam();
	
		//resetup the new player area
		SetupHeroList();


	}

	//cylces between teams of heroes
	public void OnChangeLoadout(bool iteratingForward){

		//currentTeamHeroIDs.Clear();
		if (iteratingForward) {
			activeLoadoutID = ++activeLoadoutID % numberOfLoadouts;
		} else {
			activeLoadoutID--;
			if (activeLoadoutID < 0) {
				activeLoadoutID = numberOfLoadouts - 1;
			}
		}

		currentTeamHeroIDs = teamHeroIDs[(int)activeLoadoutID];
		SetupLoadout();
		SetupTopTeam();
		SetupHeroList();
		GameManager.currentLoadouts.SetActiveLoadout(activeLoadoutID.ToString());

	}

	public void OnSaveBtnClick(){

		string flatHeroIDs = "";

		foreach (List<string> loadout in teamHeroIDs) {
			foreach (string heroID in loadout) {
				flatHeroIDs += heroID + ":";
			}
		}

		//remove the final colon
		if(flatHeroIDs.Length > 0){
			flatHeroIDs = flatHeroIDs.Remove(flatHeroIDs.Length-1);
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["teams"] = "0";
		body["active"] = activeLoadoutID.ToString();

		StartCoroutine(Postman.SaveTeam(body, null));

	}

	public void OnEquipmentBtnClick(string heroID){

		StartCoroutine(Postman.GetEquipmentForHero(heroID,  SetupEquipment));

		AnimateToSubPage(heroEquipment);
	}

	public void OnHeroEquipmentBackBtnClick(){

		AnimateToSubPage(heroDetails);
	}

	public void OnItemClick(Equipment itemRecipe){

		AnimateToSubPage(equipmentCrafting);
		SetupCrafting(itemRecipe);
	}

	public void OnCraftingBackBtnClick(){

		AnimateToSubPage(heroEquipment);
		if (NewPlayerExperienceManager.HasEventBeenTriggered("NewPlayerExperience8")) {
			NewPlayerExperienceManager.FireEvent("NewPlayerExperience9");
		}
	}
		
	public void BackToHeroList(){

		AnimateToSubPage(heroList);
	}

	public void OnStoryBtnClick(string heroID){

		GameObject popup = PopupManager.OpenPopup("DialogPopup");
		PopupManager.SetTextForSimplePopup(popup, "Feature disabled for Alpha");

		/*
		AnimateToSubPage(heroStory);
		SetupStory(); */
	}

	public void OnPromoteBtnClick(string heroID){
		//Do promote call here

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["hero"] = heroID;

		StartCoroutine(Postman.PromoteHunter(body, BackToHeroList));

		/*GameObject popup = PopupManager.OpenPopup("DialogPopup");
		PopupManager.SetTextForSimplePopup(popup, "Feature disabled for Alpha"); */
	}

	public void OnCreateBtnClick(string heroID){
		Dictionary<string, string> body = new Dictionary<string, string>();
		body["hero"] = heroID;

		StartCoroutine(Postman.CreateHunter(body, BackToHeroList));

	}


	public void OnCraftBtnClick(){

		Dictionary<string, string> body = new Dictionary<string, string>();

		body["HeroID"] = currentHeroID;
		body["EquipmentID"] = currentEquipmentID;

		StartCoroutine(Postman.CraftEquipment(body, OnCraftResponse));
	}

	//For testing
	public void OnResetBtnClick(){

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["hero_id"] = currentHeroID;
		StartCoroutine(Postman.ResetEquipment(body, OnResetResponse));
	}


	public void OnGiveItemsBtnClick(){
		string URL = GameConfig.SERVER_URL+"/testers/test2/";
		Postman.SendGet(URL);
	}

	#endregion


	#region Animation Functions


	public void AnimateToSubPage(GameObject newSubPage){

		if (activePage != newSubPage) {
			activePage.GetComponent<Animator>().SetTrigger("exitRight");
			newSubPage.GetComponent<Animator>().SetTrigger("enterLeft");

			activePage = newSubPage;
		}
	}

	#endregion

	#region Server Responses
	//Todo: refactor this so it doesn't copy so much code
	public void OnCraftResponse(){
		Debug.Log("Got craft response");

		SetupEquipment();

		AnimateToSubPage(heroEquipment);


		foreach (KeyValuePair<string, fsData> entry in Postman.RESPONSE) {
			Debug.Log(entry.Key);
			Debug.Log(entry.Value);
		}

	}

	public void OnResetResponse(){
		Debug.Log("Got reset response");
	}


	#endregion
		

	void SaveCurrentTeam(){

		Debug.Log("Saving current team");


		/*string flatHeroIDs = "";

		foreach (List<string> loadout in teamHeroIDs) {
			foreach (string heroID in loadout) {
				flatHeroIDs += heroID + ":";
			}
		}

		//remove the final colon
		if(flatHeroIDs.Length > 0){
			flatHeroIDs = flatHeroIDs.Remove(flatHeroIDs.Length-1);
		}

		Dictionary<string, string> body = new Dictionary<string, string>();
		body["teams"] = "0";
		body["active"] = activeLoadoutID.ToString();

		StartCoroutine(Postman.SaveTeam(body, null)); */


		//StartCoroutine(Postman.SaveTeam(body, null));

	//	SceneLoader.LoadHome();
	}
}
