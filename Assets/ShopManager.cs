﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ShopManager : MonoBehaviour {

	public Canvas Canvas;
	public ScrollingList ScrollingList;
	public GameObject ShopBtnPrefab;


	// Use this for initialization
	void Start () {

		if (Application.isEditor && !GameObject.Find("Authentication")) {
			SceneManager.LoadScene("LoadGame");
		}


		Canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		ScrollingList = GameObject.FindGameObjectWithTag("ScrollingList").GetComponent<ScrollingList>();

		if (!ShopBtnPrefab) {
			ShopBtnPrefab = (GameObject)Resources.Load("ShopPagePrefabs/ShopBtn");
		}


		//Load data from a config here

		GameObject newButton = (GameObject)Instantiate(ShopBtnPrefab);
		newButton.GetComponent<Button>().onClick.AddListener(delegate {
			BuyBtnClick();	
		});


		List<GameObject> newBtns = new List<GameObject>();
		newBtns.Add(newButton);

		ScrollingList.Populate(newBtns);

	}

	public void testbuyBtnClick(){
		Debug.Log("This is for testing the buy btn");
	}

	public void BuyBtnClick(){
		Debug.Log("Shop Btn Click");

	}

}
